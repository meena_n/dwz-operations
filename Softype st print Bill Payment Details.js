/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

/***************************************************************************************
	** Copyright (c) 1998-2018 Softype, Inc.
	** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
	** All Rights Reserved.
	**
	** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
	** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
	** the license agreement you entered into with Softype.
	**
	** @Author     :  Meena
	** @Dated       :  24 Apr 2020
	** @Version     :  2.0
	** @Description :  Suitelet to print the data from Bill Payment record after the userevent redirects here.
	
***************************************************************************************/

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
define(['N/format', 'N/runtime', 'N/record', 'N/xml', 'N/render', 'N/http', 'N/format', 'N/search'],
    function(format, runtime, record, xml, render, http, format, search) {
        function onRequest(context) {
            /* var ITEM = "INPUT:UNDEF-PH"; */
            var ITEM = "Withholding Tax Payable - Expanded (WE_PH)";

            if (context.request.method == 'GET') {
                // Getting the parameters from Userevent.
                var action = context.request.parameters.action;
                var recordid = context.request.parameters.recordid;

                var loadrec = record.load({
                    type: 'vendorpayment',
                    id: recordid
                });

                log.debug('loadrec', loadrec);

                var subsidiary = loadrec.getValue({
                    fieldId: 'subsidiary'
                });

                var subsidiaryLoad = record.load({
                    type: 'subsidiary',
                    id: subsidiary
                });


                log.debug('subsidiaryLoad', subsidiaryLoad);

                var legalName = subsidiaryLoad.getText({
                    fieldId: 'legalname'
                });

                log.debug('legalName', legalName);

                var mainaddress = subsidiaryLoad.getValue({
                    fieldId: 'mainaddress_text'
                });

                log.debug('mainaddress', mainaddress);

                var country = subsidiaryLoad.getText({
                    fieldId: 'country'
                });

                log.debug('country', country);


                var TotalAmount = loadrec.getText({
                    fieldId: 'total'
                });

                var entityId = loadrec.getValue({
                    fieldId: 'entity'
                });

                log.debug('entityId', entityId);


                var vendorFields = search.lookupFields({
                    type: 'vendor',
                    id: entityId,
                    columns: ['companyname']
                });

                log.debug('vendorFields', vendorFields.companyname);
				
				var currencyInBillPayment = loadrec.getValue({
					fieldId: 'currency'
				});
				
				log.debug('currencyInBillPayment',currencyInBillPayment);
				
				var loadCurrency = record.load({
					type: 'currency',
					id: currencyInBillPayment
				});
				
				log.audit('loadCurrency',loadCurrency);
				
				var currencySymbol = loadCurrency.getText({
					fieldId: 'symbol'
				});
				
				log.audit('currencySymbol',currencySymbol);


                var address = loadrec.getText({
                    fieldId: 'address'
                });
                log.debug('address', address);



                var tranId = loadrec.getText({
                    fieldId: 'tranid'
                });
                log.debug('tranId', tranId);

                var tranDate = loadrec.getText({
                    fieldId: 'trandate'
                });

                log.debug('tranDate', tranDate);

                var url = "https://3952709.app.netsuite.com/core/media/media.nl?id=291&c=3952709&h=a3cf0d1b2dad5d612544"
                var xmlEscapedDocument = xml.escape({
                    xmlText: url
                });



                // Creating the htmlvar file to displayed in the suitelet.
                htmlvar = '';
                htmlvar += '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n';
                htmlvar += '<pdf>\n'
                htmlvar += '<head>';




                htmlvar += '<macrolist>';

                htmlvar += '<macro id="nlheader" height ="auto">';
                htmlvar += '<table width="100%">';
                htmlvar += '<tr>';

                htmlvar += '<td width="50%" rowspan="2"><img style= "width:60%; height:50%" src= "' + xmlEscapedDocument + '" /></td>';
                htmlvar += '<td colspan="2" width="50%" padding-right="135px" align="right" style="font-size:22px"><b>Payment Voucher</b></td>';
                htmlvar += '</tr>';
                htmlvar += '<tr>';
                htmlvar += '<td width="49%" padding-right="32" align="right" style="font-size:12px"><b>Date</b></td>';
                htmlvar += '<td width="49%" padding-left="15px" align="left" style="font-size:12px">' + tranDate + '</td>';
                htmlvar += '</tr>';


                htmlvar += '</table>';

                htmlvar += '<table margin-top="10px" width="1000px">';
                htmlvar += '<tr>';
                htmlvar += '<td width="170px" style="font-size:12px"><p align="left">' + mainaddress + '</p></td>';
                htmlvar += '<td width="310px" padding-right="9" align="right" style="font-size:12px"><b>Cheque #</b><br /><br/><b>Pay To</b></td>';
				if(vendorFields.companyname.length > 42){
					vendorFields.companyname = vendorFields.companyname.concat("...");
				}
                htmlvar += '<td width="520px" padding-left="20px" style="font-size:13px">' + tranId + '<br /><br />' + xml.escape(vendorFields.companyname == '' ? '' : vendorFields.companyname.substring(0,42)) + '</td>';
                htmlvar += '</tr>';



                htmlvar += '</table>';

                htmlvar += '<table margin-top="15px" width="25%">';
                htmlvar += '<tr>';
                htmlvar += '<td style="font-size:12px"><b>Paid To</b></td></tr>';
                htmlvar += '<tr>';
                htmlvar += '<td style="font-size:11px"><p align="left">' + address + '</p></td>';

                htmlvar += '</tr>';

                htmlvar += '</table>';



                htmlvar += '</macro>';

                htmlvar += '<macro id="nlfooter">';

                htmlvar += '</macro>';
                htmlvar += '</macrolist>';

                htmlvar += '</head>';

                htmlvar += '<body header="nlheader"  header-height="26%"   footer="nlfooter" footer-height="" padding="0.5in 0.3in 0.5in 0.3in" width="21.50cm" height="14.00cm" size="Letter">';

                htmlvar += '<table bordercolor="red" width="100%">';
                htmlvar += '<tr>';
                htmlvar += '<th style="padding: 5px 6px 3px; color: #ffffff; background-color: #778899; font-size:12px;" width="12%" align="center"><p align="left"><b>Date</b></p></th>';
                htmlvar += '<th width="12%" align="center" style="color: #ffffff; padding: 5px 6px 3px; font-size:12px; background-color: #778899;"><b>Description</b></th>';
                htmlvar += '<th width="12%" align="center" style="color: #ffffff; padding: 5px 6px 3px; font-size:12px; background-color: #778899;"><b>Orig. Amount</b></th>';
                htmlvar += '<th align="center" width="12%" style="color: #ffffff; padding: 5px 6px 3px; font-size:12px; background-color: #778899;"><b>Tax</b></th>';
                htmlvar += '<th align="center" width="12%" style="color: #ffffff; padding: 5px 6px 3px; font-size:12px; background-color: #778899;"><b>WHT Tax Amt</b></th>';
                htmlvar += '<th width="12%" align="center" style="color: #ffffff; padding: 5px 6px 3px; font-size:12px; background-color: #778899;"><b>Amount Due</b></th>';
                htmlvar += '<th width="12%" align="center" style="color: #ffffff; padding: 5px 6px 3px; font-size:12px; background-color: #778899;"><b>Discount</b></th>';
                htmlvar += '<th align="center" width="12%" style="color: #ffffff; padding: 5px 6px 3px; font-size:12px; background-color: #778899;"><b>Applied Amount</b></th>';

                htmlvar += '</tr>';



                var custcol_4601_witaxapplies = true;
                var internalIdArr = [];

                var numLines = loadrec.getLineCount({
                    sublistId: 'apply'
                });

                log.debug('numLines:', numLines);



                for (var i = 0; i < numLines; i++) {

                    var apply = loadrec.getSublistValue({
                        sublistId: 'apply',
                        fieldId: 'apply',
                        line: i
                    });

                    if (apply == true) {
                        var applyDate = loadrec.getSublistText({
                            sublistId: 'apply',
                            fieldId: 'applydate',
                            line: i
                        });

                        log.debug('applyDate:', applyDate);

                        var applyType = loadrec.getSublistText({
                            sublistId: 'apply',
                            fieldId: 'type',
                            line: i
                        });

                        log.debug('applyType:', applyType);

                        var Total = loadrec.getSublistText({
                            sublistId: 'apply',
                            fieldId: 'total',
                            line: i
                        });

                        log.debug('Total:', Total);

                        var Due = loadrec.getSublistText({
                            sublistId: 'apply',
                            fieldId: 'due',
                            line: i
                        });

                        log.debug('Due:', Due);

                        var Disc = loadrec.getSublistText({
                            sublistId: 'apply',
                            fieldId: 'disc',
                            line: i
                        });

                        log.debug('Disc:', Disc);

                        var Amount = loadrec.getSublistText({
                            sublistId: 'apply',
                            fieldId: 'amount',
                            line: i
                        });

                        log.debug('Amount:', Amount);

                        var internalId = loadrec.getSublistValue({
                            sublistId: 'apply',
                            fieldId: 'internalid',
                            line: i
                        });

                        log.debug('internalId:', internalId);

                        // internalIdArr.push(internalId);

                        log.debug('internalIdArr', internalIdArr);

                        //for(var j=0; j<internalId.length;j++){
                        //log.debug('internalId@@@',internalId[j]);	

                        var loadBillRecord = record.load({
                            type: 'vendorbill',
                            id: internalId
                        });
                        //}



                        log.debug('loadBillRecord', loadBillRecord);

                        var taxTotal = loadBillRecord.getText({
                            fieldId: 'taxtotal'
                        });

                        log.debug('taxTotal', taxTotal);

                        taxTotal = taxTotal.replace("-", "");



                        var whTaxFinalAmount = 0;

                        var lineItemCount = loadBillRecord.getLineCount({
                            sublistId: 'item'
                        });

                        log.debug('lineItemCount', lineItemCount);


                        for (var j = 0; j < lineItemCount; j++) {

                            var wiTaxApplies = loadBillRecord.getSublistValue({
                                sublistId: 'item',
                                fieldId: 'custcol_4601_witaxapplies',
                                line: j
                            });

                            log.debug('wiTaxApplies', wiTaxApplies);

                            if (wiTaxApplies == true) {
                                var whTaxAmount = Number(loadBillRecord.getSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'custcol_4601_witaxamount',
                                    line: j
                                }));
                                log.debug('whTaxAmount', whTaxAmount);

                                whTaxFinalAmount += Math.abs(whTaxAmount || 0);

                                
								
								
								
                                //}
                            }
                        }
						log.debug('type',typeof(whTaxFinalAmount));


                        var lineItemCountExpense = loadBillRecord.getLineCount({
                            sublistId: 'expense'
                        });
						
						log.audit('lineItemCountExpense',lineItemCountExpense);
                        for (var k = 0; k < lineItemCountExpense; k++) {
							var wiTaxApplies = loadBillRecord.getSublistValue({
                                sublistId: 'expense',
                                fieldId: 'custcol_4601_witaxapplies',
                                line: k
                            });

                            log.debug('wiTaxApplies', wiTaxApplies);

                           if(wiTaxApplies == true){
							var whTaxAmount = Number(loadBillRecord.getSublistValue({
                                sublistId: 'expense',
                                fieldId: 'custcol_4601_witaxamt_exp',
                                line: k
                            }));
						   
			
                            whTaxFinalAmount += Math.abs(whTaxAmount || 0);


                            log.debug("Expense Withholding Tax Amount - ", whTaxFinalAmount);

						   }
						}	
						
						whTaxFinalAmount = whTaxFinalAmount.toString();
						

						

                        htmlvar += '<tr>';
                        htmlvar += '<td border-left="1" border-right="1" align="center" style="font-size:12px">' + applyDate + '</td>';
                        htmlvar += '<td border-right="1" style="font-size:12px"><p align="center">' + xml.escape(applyType == '' ? '' : applyType) + '</p></td>';
                        htmlvar += '<td border-right="1" style="font-size:12px"><p align="right">' + xml.escape(Total).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</p></td>';
                        htmlvar += '<td border-right="1" align="right" style="font-size:12px">' + taxTotal.replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</td>';
                        htmlvar += '<td border-right="1" style="font-size:12px" align="right">' + xml.escape(whTaxFinalAmount).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</td>';
                        htmlvar += '<td border-right="1" style="font-size:12px"><p align="right">' + xml.escape(Due).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</p></td>';
                        htmlvar += '<td border-right="1" style="font-size:12px"><p align="right">' + xml.escape(Disc).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</p></td>';
                        htmlvar += '<td border-right="1" style="font-size:12px" align="right">' + xml.escape(Amount).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</td>';

                        htmlvar += '</tr>';

                    }


                }

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';


                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';


                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

              

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';

                htmlvar += '<tr>';
                htmlvar += '<td border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '<td border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';



                htmlvar += '<tr>';
                htmlvar += '<td border-bottom="1" border-right="1" border-left="1">&nbsp;</td>';
                htmlvar += '<td border-bottom="1" border-right="1">&nbsp;</td>';
                htmlvar += '<td border-bottom="1" border-right="1">&nbsp;</td>';
                htmlvar += '<td border-bottom="1" border-right="1">&nbsp;</td>';
                htmlvar += '<td border-bottom="1" border-right="1">&nbsp;</td>';
                htmlvar += '<td border-bottom="1" border-right="1">&nbsp;</td>';
                htmlvar += '<td border-bottom="1" border-right="1">&nbsp;</td>';
                htmlvar += '<td border-bottom="1" border-right="1">&nbsp;</td>';
                htmlvar += '</tr>';




                htmlvar += '</table>';

                htmlvar += '<table margin-top="20px" width="250px" align="right">';
                htmlvar += '<tr padding="3px">';
                htmlvar += '<th width="20px" align="right" style="font-size:12px"><b>Amount</b></th>';
                htmlvar += '<th padding-right="8px" width="20px" align="center" style="font-size:12px"><b>' +xml.escape(currencySymbol)+' '+ xml.escape(TotalAmount).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + '</b></th>';
                htmlvar += '</tr>';
                htmlvar += '</table>';


                htmlvar += '</body>';
                htmlvar += '</pdf>';

                var file = render.xmlToPdf({
                    xmlString: htmlvar
                });

                context.response.writeFile(file, true);
                return;
            }
        }


        return {
            onRequest: onRequest
        };
    });