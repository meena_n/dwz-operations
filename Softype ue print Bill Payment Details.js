/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */
 
 define(['N/search','N/record','N/ui/serverWidget','N/url'],
	function(search,record,serverWidget,url){
		function beforeLoad(scriptContext){
			var form = scriptContext.form;
			
		var currentRecord = scriptContext.newRecord; 
			log.debug('currentRecord:',currentRecord);
			if(scriptContext.type == 'view') {
			
			var recordType = currentRecord.type;
			var recordid = currentRecord.id;
			log.debug('recordType:',recordType);
			 
			
			
			var outputUrl = url.resolveScript
		({
			scriptId: 'customscript_softype_st_print_billpyment',
			deploymentId: 'customdeploy_softype_st_print_billpyment',
			returnExternalUrl: false
		});
		// Adding parameters to pass in the suitelet.
		outputUrl += '&action=GET';
		outputUrl += '&recordid=' + recordid;

		// Creating function to redirect to the suitelet.
		var stringScript = "window.open('"+outputUrl+"','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";
			
			 
			var printLabel = form.addButton({
				id: 'custpage_printId',
				label: 'Print',
				functionName: stringScript
			});
			
			
		
			}
		
		
		}
		return{
		beforeLoad: beforeLoad
		};
		});