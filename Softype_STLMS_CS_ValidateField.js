/* ************************************************************************************  
 ** Copyright (coffee) 1998-2012 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of          
 ** Softype, Inc.("Confidential Information"). You shall not disclose              
 ** such Confidential Information and shall use it only in accordance         
 ** with the terms of the license agreement you entered into with Softype.
 ** @version: Revised version               
 **                       
 ** @author: Rutika More    
 ** @dated: 
 ** @Description:
 ************************************************************************************ */
var leaveType;
var check = 'F';

//Check the field is being updated.
function fieldChange_passCheckVariable(type, name) {

	if(type != 'create' && type != 'delete'){

		if (name != 'custentity_stlms_leavetypes')
		{
			//DO NOTHING
			return true;
		}

		//Field change will trigger only on change of leave type.
		if(name == 'custentity_stlms_leavetypes') {

//			alert('type : '+type);

			check = 'T';

		}
		else {

			check = 'F';

		}
	}
}

//Create leave balance master line for the leaves newly added.
function saveRecord_confirmValidate() {

	var recId = nlapiGetRecordId();

	if(recId != '' && recId != null) {

//		alert('check : '+check);

		leaveType = nlapiGetFieldValues('custentity_stlms_leavetypes');
//		alert('leaveType : '+leaveType);

		if(leaveType != '' && leaveType != null) {
			
			//Suitelet call.
			var URL = nlapiResolveURL('SUITELET', 'customscript_stlms_leaveapplicationretur', 'customdeploy_stlms_leaveapplicationretur', true);
			var searchResult = nlapiRequestURL(URL,{action:'createLeaveBalanceLine',recId:recId, leaveType:leaveType});

			//Created leave balance master line record.
			var a = JSON.parse(searchResult.getBody());
			
		}
	}

	return true;
}