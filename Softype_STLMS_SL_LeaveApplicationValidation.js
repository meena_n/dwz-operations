/* ************************************************************************************  
 ** Copyright (coffee) 1998-2012 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of          
 ** Softype, Inc.("Confidential Information"). You shall not disclose              
 ** such Confidential Information and shall use it only in accordance         
 ** with the terms of the license agreement you entered into with Softype.
 ** @version: Revised version               
 ** 
 ** @author: Rutika More    
 ** @dated: 
 ** @Description: This suitelet is used to calculate the leave period as the client side has limitations to access the work calendar.
 ************************************************************************************ */
var weekoff = [];
var deductweekoff = Number(0);
var deductHoliday = Number(0);
var returnValues = [];
var escalationDay;

//Function to calculate the leave period considering the selected work-calendar on employee.
function suitelet_searchWorkCalendar(request, response) {

	var action = request.getParameter('action');
	nlapiLogExecution('Debug', 'action', action);

	var annualLeaveId = 8;
	
	//This action is for Run Periodic Leave Accrual search on setup.
	if(action == 'searchLeaveMngmntSetup') {

		var method = request.getParameter('method');
		var empType = request.getParameter('empType');
		//var empType = nlapiLookupField('employee', emp, 'employeetype');
		var frequency, calendarStart, calendarStartDate, calendarEndDate;

		var sFilt = [];
		var sColm = [];

		sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', null, 'is', empType));

		sColm.push(new nlobjSearchColumn('custrecord_stlms_su_frequencyofaccrual'));
		sColm.push(new nlobjSearchColumn('custrecord_stlms_su_leavecalendarstart'));
		sColm.push(new nlobjSearchColumn('custrecord_stlms_su_calendarstartdate'));
		sColm.push(new nlobjSearchColumn('custrecord_stlms_su_calendarenddate'));

		var srchSetup = nlapiSearchRecord('customrecord_stlms_setups', null, sFilt, sColm);
		var getSearch = 'F';
		if(srchSetup) {

			getSearch = 'T';
			frequency = srchSetup[0].getValue('custrecord_stlms_su_frequencyofaccrual');
			calendarStart = srchSetup[0].getValue('custrecord_stlms_su_leavecalendarstart');
			calendarStartDate = srchSetup[0].getValue('custrecord_stlms_su_calendarstartdate');
			calendarEndDate = srchSetup[0].getValue('custrecord_stlms_su_calendarenddate');

		}

		var searchReturn = {getSearch:getSearch, frequency:frequency, calendarStart:calendarStart, calendarStartDate:calendarStartDate, calendarEndDate:calendarEndDate};
		searchReturn = JSON.stringify(searchReturn);
		nlapiLogExecution('Debug', 'searchReturn', searchReturn);
		response.write(searchReturn);

	}

	if(action == 'searchRunAccrual') {

		var empType = request.getParameter('empType');
		var fromDate, toDate, tempid;

		//Search latest Run Periodic Accrual record.
		var filterRunAccrual = new Array();
		filterRunAccrual.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filterRunAccrual.push(new nlobjSearchFilter('custrecord_stlms_rpla_employeetype', null, 'is', empType));

		var columnRunAccrual = new Array();
		columnRunAccrual.push(new nlobjSearchColumn('custrecord_stlms_rpla_fromdate',null,'max'));
		columnRunAccrual.push(new nlobjSearchColumn('custrecord_stlms_rpla_todate',null,'max'));
		columnRunAccrual.push(new nlobjSearchColumn('internalid',null,'max'));

		var lastRunPeriodicLeaveAccurl = nlapiSearchRecord('customrecord_stlms_runaccrual', null, filterRunAccrual, columnRunAccrual);
		var getSearch = 'F';

		if(lastRunPeriodicLeaveAccurl && lastRunPeriodicLeaveAccurl[0].getValue('internalid',null,'max') != null && lastRunPeriodicLeaveAccurl[0].getValue('internalid',null,'max') != '')
		{

			getSearch = 'T';
			fromDate = lastRunPeriodicLeaveAccurl[0].getValue('custrecord_stlms_rpla_fromdate',null,'max');
			toDate = lastRunPeriodicLeaveAccurl[0].getValue('custrecord_stlms_rpla_todate',null,'max');
			tempid = lastRunPeriodicLeaveAccurl[0].getId();

		}

		var searchReturn = {getSearch:getSearch, fromDate:fromDate, toDate:toDate, tempid:tempid};
		searchReturn = JSON.stringify(searchReturn);
		nlapiLogExecution('Debug', 'searchReturn', searchReturn);
		response.write(searchReturn);

	}

	//This action is from Employee Client script record save.
	if(action == 'createLeaveBalanceLine') {

		var recId = request.getParameter('recId');
		var leaveType = request.getParameter('leaveType');
		var submitLine;

		var filterLeaveBalanceLine = new Array();
		filterLeaveBalanceLine.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filterLeaveBalanceLine.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', recId));

		var columnLeaveBalanceLine = new Array();
		columnLeaveBalanceLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavetype'));
		columnLeaveBalanceLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_parent'));

		var searchLeaveBalanceLine = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filterLeaveBalanceLine, columnLeaveBalanceLine);

		if(searchLeaveBalanceLine) {

			for(var lt = 0; lt < leaveType.length; lt++) {

				var count = 0;
				var parent = searchLeaveBalanceLine[0].getValue('custrecord_stlms_lbline_parent');
				for(var l = 0; l < searchLeaveBalanceLine.length; l++) {

					var leaveTypeOnLine = searchLeaveBalanceLine[l].getValue('custrecord_stlms_lbline_leavetype');

					if(leaveType[lt] == leaveTypeOnLine) {

						count = count + Number(1);

					}
				}

				//If zero means there is no leave balance master line record for current processing leave type.
				if(count == 0) {

					//Create Leave Balance Master Line Record.
					var createRecord = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');
					createRecord.setFieldValue('custrecord_stlms_lbline_leavetype', leaveType[lt]);
					createRecord.setFieldValue('custrecord_stlms_lbline_leavebalance', 0);
					createRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', 0);
					createRecord.setFieldValue('custrecord_stlms_lbline_leaveutilized', 0);
					if(recId) {
						createRecord.setFieldValue('custrecord_stlms_lbline_parent', parent);
					}
					submitLine = nlapiSubmitRecord(createRecord);

				}
				lt++;
			}
		}
		else {

			var filterLeaveBalanceMaster = new Array();
			filterLeaveBalanceMaster.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filterLeaveBalanceMaster.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', null, 'is', recId));

			var searchLeaveBalanceMaster = nlapiSearchRecord('customrecord_stlms_leavebalancemaster', null, filterLeaveBalanceMaster, null);

			if(searchLeaveBalanceMaster) {

				var parent = searchLeaveBalanceMaster[0].getId();

				for(var lt = 0; lt < leaveType.length; lt++) {

					//Create Leave Balance Master Line Record.
					var createRecord = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');
					createRecord.setFieldValue('custrecord_stlms_lbline_leavetype', leaveType[lt]);
					createRecord.setFieldValue('custrecord_stlms_lbline_leavebalance', 0);
					createRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', 0);
					createRecord.setFieldValue('custrecord_stlms_lbline_leaveutilized', 0);
					if(recId) {
						createRecord.setFieldValue('custrecord_stlms_lbline_parent', parent);
					}
					submitLine = nlapiSubmitRecord(createRecord);

					lt++;
				}
			}
		}

		var searchReturn = {submitLine:submitLine};
		searchReturn = JSON.stringify(searchReturn);
		response.write(searchReturn);
	}

	//This action is from Leave Application Client script field change.
	if(action == 'checkBalanceLine') {

		var emp = request.getParameter('emp');
		var leaveType = request.getParameter('leaveType');
		var leaveTypeConsider = request.getParameter('leaveTypeConsider');

		var leaveBalance = 0;
		var carryForward = 0;
		var totalLeaves = 0;
		var giveAlert = 'F';
		var getSearch = 'F';
		var leaveBalanceTocalculate = 0;
		var carryForwardTocalculate = 0;

		if(leaveType == annualLeaveId) {
			
			calculateTotalLeaves(leaveType,leaveTypeConsider,getSearch,giveAlert,emp,totalLeaves);      
			
		}
		else {
			
			//Search Leave Balance Line to get Leave Balance and Carry Forward Leave Balance for the emp and selected leave type
			var filter = new Array();
			filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filter.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', emp));

			if(leaveType == leaveTypeConsider) {
				filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveType));
			}
			else if(leaveType != leaveTypeConsider) {
				filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveTypeConsider));
			}

			var column = new Array();
			column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
			column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));

			var search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filter, column);

			//If search set the received values
			if(search) {

				getSearch = 'T';
				leaveBalance = search[0].getValue('custrecord_stlms_lbline_leavebalance');
				carryForward = search[0].getValue('custrecord_stlms_lbline_carriedforward');
				totalLeaves = Number(leaveBalance) + Number(carryForward);

			}

			//If no results found set leave balance to zero.
			else {

				getSearch = 'F';

				if(leaveType == leaveTypeConsider) {
					leaveBalance = 0;
					carryForward = 0;

				}
				else if(leaveType != leaveTypeConsider) {

					giveAlert = 'T';

				}
			}

			var searchReturn = {leaveBalance:leaveBalance, carryForward:carryForward, totalLeaves:totalLeaves, giveAlert:giveAlert, getSearch:getSearch};
			searchReturn = JSON.stringify(searchReturn);
			response.write(searchReturn);

		}

	}

	//This action is from Leave Application Client script record save.
	if(action == 'calculateLeavePeriod') {

		//Parameters received from the Client Side.
		var empType = request.getParameter('empType');
		var leavePeriod = request.getParameter('leavePeriod');
		var startYear = request.getParameter('startYear');
		var startMonth = request.getParameter('startMonth');
		var startDate = request.getParameter('startDate');
		var getDay = request.getParameter('getDay');
		var leaveType = request.getParameter('getLeaveType');
		nlapiLogExecution('DEBUG','Leave Type in Parameter',leaveType);
		var emp = request.getParameter('getEmpId');
		var empType = request.getParameter('empType');//nlapiLookupField('employee',emp,'employeetype');
//		var weekOffHolidays = request.getParameter('weekOffHolidays');
//		var publicHolidays = request.getParameter('publicHolidays');

		nlapiLogExecution('Debug', 'Get All Values', 'Employee Type : '+empType+' Leave Period : '+leavePeriod+' Start Year : '+startYear+' Start Month : '+startMonth+' Start Date : '+startDate+' getDay : '+getDay);

		var filter = new Array();
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', null, 'is', empType));

		var column = new Array();
		column.push(new nlobjSearchColumn('custrecord_stlms_su_publicholidayasleave'));
		column.push(new nlobjSearchColumn('custrecord_stlms_su_weeklyoffasleave'));
		column.push(new nlobjSearchColumn('custrecord_stlms_su_daysforescalation'));

		var search = nlapiSearchRecord('customrecord_stlms_setups', null, filter, column);

		if(search) {

			var publicHolidays = search[0].getValue('custrecord_stlms_su_publicholidayasleave');
			var weekOffHolidays = search[0].getValue('custrecord_stlms_su_weeklyoffasleave');
			escalationDay = search[0].getValue('custrecord_stlms_su_daysforescalation');

			//Search on the selected work-calendar on employee
			var filterCalendar = new Array();
			filterCalendar.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filterCalendar.push(new nlobjSearchFilter('custrecord_stlms_wc_emptype', null, 'is', empType));

			var columnCalendar = new Array();
			columnCalendar.push(new nlobjSearchColumn('custrecord_stlms_wc_sunday'));
			columnCalendar.push(new nlobjSearchColumn('custrecord_stlms_wc_monday'));
			columnCalendar.push(new nlobjSearchColumn('custrecord_stlms_wc_tuesday'));
			columnCalendar.push(new nlobjSearchColumn('custrecord_stlms_wc_wednesday'));
			columnCalendar.push(new nlobjSearchColumn('custrecord_stlms_wc_thursday'));
			columnCalendar.push(new nlobjSearchColumn('custrecord_stlms_wc_friday'));
			columnCalendar.push(new nlobjSearchColumn('custrecord_stlms_wc_saturday'));

			var searchCalendar = nlapiSearchRecord('customrecord_stlms_workcalendar', null, filterCalendar, columnCalendar);

			if(searchCalendar) {

				nlapiLogExecution('Debug', 'Inside search calendar');

				//Results pushed in array to get the week off days. The check-box which is false is considered as week off.
				if(searchCalendar[0].getValue('custrecord_stlms_wc_monday') == 'F') {
					nlapiLogExecution('Debug', 'Inside search monday');
					weekoff.push(1);
				}

				if(searchCalendar[0].getValue('custrecord_stlms_wc_tuesday') == 'F') {
					nlapiLogExecution('Debug', 'Inside search tuesday');
					weekoff.push(2);
				}

				if(searchCalendar[0].getValue('custrecord_stlms_wc_wednesday') == 'F') {
					nlapiLogExecution('Debug', 'Inside search wednesday');
					weekoff.push(3);
				}

				if(searchCalendar[0].getValue('custrecord_stlms_wc_thursday') == 'F') {
					nlapiLogExecution('Debug', 'Inside search thursday');
					weekoff.push(4);
				}

				if(searchCalendar[0].getValue('custrecord_stlms_wc_friday') == 'F') {
					nlapiLogExecution('Debug', 'Inside search friday');
					weekoff.push(5);
				}

				if(searchCalendar[0].getValue('custrecord_stlms_wc_saturday') == 'F') {
					nlapiLogExecution('Debug', 'Inside search saturday');
					weekoff.push(6);
				}

				if(searchCalendar[0].getValue('custrecord_stlms_wc_sunday') == 'F') {
					nlapiLogExecution('Debug', 'Inside search sunday');
					weekoff.push(0);
				}
				nlapiLogExecution('Debug', 'weekoff length', weekoff.length);
				if(leavePeriod > 0) {
					nlapiLogExecution('Debug', 'Inside leave period > 0');

					//Record is loaded to get line items to get the values of public holidays.
					var loadRecord = nlapiLoadRecord('customrecord_stlms_workcalendar',searchCalendar[0].getId());
					var lineCount = loadRecord.getLineItemCount('recmachcustrecord_stlms_wcnon_parent');

					//For loop to process all the leave dates within the selected range.
					for(var lp = 0; lp < leavePeriod; lp++) {

						//To keep a track of dates which are public holidays as well as week-offs.
						var compareDates;// = [];

						//If any days are considered as week offs.
						if(weekoff.length > 0){
							nlapiLogExecution('Debug', 'weekoff.length', weekoff.length);

							//For loop of the day of current date being processed within the complete length of week-off.
							for(var wo = 0; wo < weekoff.length; wo++) {

								if(getDay == weekoff[wo]) {

									nlapiLogExecution('Debug', 'Inside getDay == weekOff', wo);
									compareDates = startDate;
									deductweekoff = Number(deductweekoff) + Number(1);
									nlapiLogExecution('Debug', 'week off to be deducted', Number(deductweekoff));
								}
							}
						}

						//If any days are considered as Public Holidays
						if(lineCount > 0){
							nlapiLogExecution('Debug', 'line Count', lineCount);

							//For loop to check current date with all the mentioned dates in public holidays.
							for(var lc = 1; lc <= lineCount; lc++) {

								var getHolidays = loadRecord.getLineItemValue('recmachcustrecord_stlms_wcnon_parent', 'custrecord_stlms_wcnon_date', lc);
								getHolidays = nlapiStringToDate(getHolidays);
//								nlapiLogExecution('Debug', 'Holiday Date', getHolidays);
								var holidayDay = getHolidays.getDate();
								var holidayYear = getHolidays.getFullYear();
								var holidayMonth = getHolidays.getMonth() + Number(1);
								var holidayDate = getHolidays.getDate(getHolidays);

								if(startYear == holidayYear && startMonth == holidayMonth && startDate == holidayDate) {

//									for(var com = 0;  com < compareDates.length; com++) {

									if(compareDates == startDate) {

										//Do Nothing
										continue;

									}

									else
									{
										nlapiLogExecution('Debug', 'Dates match', lc);
										deductHoliday = Number(deductHoliday) + Number(1);
										nlapiLogExecution('Debug', 'wekk off to be deducted', Number(deductHoliday));
									}
//									}
								}
							}
						}

						//Is increased by one to get the next date and day within the selected range of leave period.
						//This will compare each date value with the calendar.
						//Also can use nlapiAddMonths and nlapiAddDays to increment the date value by 1 but it might require a work around so the normal calculation is used.

						if(startDate == Number(30)) {

							var month = getMonth(startMonth, startYear);
							nlapiLogExecution('Debug', 'Month 30', month);
							if(month == startDate) {
								nlapiLogExecution('Debug', 'inside 30 if');
								startMonth++;
								startDate = Number(1);
							}
							else {
								startDate++;
							}
						}

						else if(startDate == Number(31)) {

							var month = getMonth(startMonth, startYear);
							if(month == startDate) {
								nlapiLogExecution('Debug', 'inside 31 if');
								startMonth++;
								startDate = Number(1);
							}
						}

						else if(startDate == Number(28)) {

							var month = getMonth(startMonth, startYear);
							if(month == startDate) {
								nlapiLogExecution('Debug', 'inside 28 if');
								startMonth++;
								startDate = Number(1);
							}
							else {
								nlapiLogExecution('Debug', 'inside 28 else');
								startDate++;
							}
						}

						else if(startDate == Number(29)) {
							nlapiLogExecution('Debug', 'inside 29 else');
							startDate++;
						}

						else if(startDate < 28){
							nlapiLogExecution('Debug', 'inside main else');
							startDate++;
						}

						getDay = Number(getDay) + Number(1);
						nlapiLogExecution('Debug', 'getDay', getDay);
						if(getDay > Number(6)) {
							nlapiLogExecution('Debug', 'inside getDay set to zero', getDay);
							getDay = Number(0);
						}
						nlapiLogExecution('Debug', 'getDay outside', getDay);
						nlapiLogExecution('Debug', 'Start Date', startDate);

					}

					//Total of all the weekoffs and holidays to be deducted from the leave period.
					var totalHolidaysDeducted = Number(deductweekoff) + Number(deductHoliday);

					//Various conditions based on which any of the above calculated holidays should be deducted.
					//The check-box which is false needs to be deducted from the leave period i.e. it should not be deducted from the leave balance.

					if(publicHolidays == 'T' && weekOffHolidays == 'F') {

						leavePeriod = Number(leavePeriod) - Number(deductweekoff);
					}

					else if(publicHolidays == 'F' && weekOffHolidays == 'T') {

						leavePeriod = Number(leavePeriod) - Number(deductHoliday);
					}

					else if(publicHolidays == 'T' && weekOffHolidays == 'T') {

						leavePeriod = Number(leavePeriod);
					}

					else if(publicHolidays == 'F' && weekOffHolidays == 'F') {

						leavePeriod = Number(leavePeriod) - Number(totalHolidaysDeducted);
					}
				}
			}
		}
		
		//Total calculated leave is returned.
		var calculatedLeave = Number(leavePeriod);
		
		if(leaveType == annualLeaveId) {
			totalLeaves = searchTotalLeaves(leaveType,emp);
			nlapiLogExecution('Debug', 'Calculated Leave', calculatedLeave);
			nlapiLogExecution('Debug', 'Total Leaves in st', totalLeaves);
			var returnJSON = {escalationDay:escalationDay, calculatedLeave:calculatedLeave,totalLeaves:totalLeaves};
			returnJSON = JSON.stringify(returnJSON);
			response.write(returnJSON);
			
		}
		else{
			var returnJSON = {escalationDay:escalationDay, calculatedLeave:calculatedLeave,totalLeaves:0};
			returnJSON = JSON.stringify(returnJSON);
			response.write(returnJSON);
			
			
		}
		
	}
}

function getMonth(startMonth, startYear) {

	var month = 0;

	switch(startMonth) {

	case 1:
		month = Number(31);
		break;

	case 2:
		if((startYear%4==0) || (startYear%100==0) || (startYear%400==0))
		{
			month = Number(29);
			break;
		}
		else
		{
			month = Number(28);
			break;
		}

	case 3:
		month = Number(31);
		break;

	case 4:
		month = Number(30);
		break;

	case 5:
		month = Number(31);
		break;

	case 6:
		month = Number(30);
		break;

	case 7:
		month = Number(31);
		break;

	case 8:
		month = Number(31);
		break;

	case 9:
		month = Number(30);
		break;

	case 10:
		month = Number(31);
		break;

	case 11:
		month = Number(30);
		break;

	case 12:
		month = Number(31);
		break;

	case '1' :
		month = Number(31);
		break;

	case '2' :
		if((startYear%4==0) || (startYear%100==0) || (startYear%400==0))
		{
			month = Number(29);
			break;
		}
		else
		{
			month = Number(28);
			break;
		}

	case '3' :
		month = Number(31);
		break;

	case '4' :
		month = Number(30);
		break;

	case '5' :
		month = Number(31);
		break;

	case '6' :
		month = Number(30);
		break;

	case '7' :
		month = Number(31);
		break;

	case '8' :
		month = Number(31);
		break;

	case '9' :
		month = Number(30);
		break;

	case '10' :
		month = Number(31);
		break;

	case '11' :
		month = Number(30);
		break;

	case '12' :
		month = Number(31);
		break;

	}

	nlapiLogExecution('Debug', 'Return', month);
	return month;

}

// This function will give me totalLeaves available and send in response in Json Format i.e totalLeaves = Annual_Leave + Absence_of_leave 
function calculateTotalLeaves(leaveType,leaveTypeConsider,getSearch,giveAlert,emp,totalLeaves) 
{
	nlapiLogExecution('DEBUG','Calculate Total Leaves function call');
	var leaveOfAbsence = 7;
	var leaveBalanceTocalculate = 0;
	var carryForwardTocalculate = 0;
	var filter = new Array();
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', emp));

	if(leaveType == leaveTypeConsider) {
		filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', [leaveType,leaveOfAbsence]));
	}
	else if(leaveType != leaveTypeConsider) {
		filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', [leaveTypeConsider,leaveOfAbsence]));
	}

	var column = new Array();
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavetype'));
	

	var search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filter, column);
	//If search set the received values
	if(search) {
		
		getSearch = 'T';
		
		for(var s = 0; s < search.length; s++) {
			nlapiLogExecution('DEBUG','Search Length',search[s].getValue('custrecord_stlms_lbline_leavetype'));
			if(search[s].getValue('custrecord_stlms_lbline_leavetype') == leaveType) {
				
				leaveBalance = search[s].getValue('custrecord_stlms_lbline_leavebalance');   
				carryForward = search[s].getValue('custrecord_stlms_lbline_carriedforward');
				totalLeaves = Number(leaveBalance) + Number(carryForward);
				nlapiLogExecution('DEBUG','Total Leaves',totalLeaves);
			}
			if(search[s].getValue('custrecord_stlms_lbline_leavetype') == leaveOfAbsence) {
				leaveBalanceTocalculate = search[s].getValue('custrecord_stlms_lbline_leavebalance');
				carryForwardTocalculate = search[s].getValue('custrecord_stlms_lbline_carriedforward');
			}
			
		}
		totalLeaves = Number(totalLeaves) + Number(leaveBalanceTocalculate) + Number(carryForwardTocalculate);
		
		nlapiLogExecution('DEBUG','Final Total Leaves',totalLeaves);
		nlapiLogExecution('DEBUG','LOA',Number(leaveBalanceTocalculate));
		nlapiLogExecution('DEBUG','LOA C',Number(carryForwardTocalculate));
		
		var searchReturn = {leaveBalance:leaveBalance, carryForward:carryForward, totalLeaves:totalLeaves, giveAlert:giveAlert, getSearch:getSearch};
		nlapiLogExecution('DEBUG','SEACRCH RETURN search found',JSON.stringify(searchReturn));
		searchReturn = JSON.stringify(searchReturn);
		response.write(searchReturn);

	}
	// If no results found set leave balance to zero.
	else {

		getSearch = 'F';

		if(leaveType == leaveTypeConsider) {
			leaveBalance = 0;
			carryForward = 0;

		}
		else if(leaveType != leaveTypeConsider) {

			giveAlert = 'T';

		}
		
		var searchReturn = {leaveBalance:leaveBalance, carryForward:carryForward, totalLeaves:totalLeaves, giveAlert:giveAlert, getSearch:getSearch};
		searchReturn = JSON.stringify(searchReturn);
		nlapiLogExecution('DEBUG','SEACRCH RETURN search not found',searchReturn);
		response.write(searchReturn);
	}
}


function searchTotalLeaves(leaveType,emp)
{
	
		var leaveOfAbsence = 1;
		var filter = new Array();
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filter.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', emp));
		filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', [leaveType,leaveOfAbsence]));
		
//		else if(leaveType != leaveTypeConsider) {
//			filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', [leaveTypeConsider,leaveOfAbsence]));
//		}

		var column = new Array();
		column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
		column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
		column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavetype'));
		

		var search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filter, column);
		//If search set the received values
		if(search) {
			
//			getSearch = 'T';
			
			for(var s = 0; s < search.length; s++) {
				nlapiLogExecution('DEBUG','Search Length',search[s].getValue('custrecord_stlms_lbline_leavetype'));
				if(search[s].getValue('custrecord_stlms_lbline_leavetype') == leaveType) {
					
					leaveBalance = search[s].getValue('custrecord_stlms_lbline_leavebalance');   
					carryForward = search[s].getValue('custrecord_stlms_lbline_carriedforward');
					totalLeaves = Number(leaveBalance) + Number(carryForward);
					nlapiLogExecution('DEBUG','Total Leaves',totalLeaves);
				}
				if(search[s].getValue('custrecord_stlms_lbline_leavetype') == leaveOfAbsence) {
					leaveBalanceTocalculate = search[s].getValue('custrecord_stlms_lbline_leavebalance');
					carryForwardTocalculate = search[s].getValue('custrecord_stlms_lbline_carriedforward');
				}
				
			}
		//	totalLeaves = Number(totalLeaves) + Number(leaveBalanceTocalculate) + Number(carryForwardTocalculate);
			return totalLeaves;
			
//			nlapiLogExecution('DEBUG','Final Total Leaves',totalLeaves);
//			nlapiLogExecution('DEBUG','LOA',Number(leaveBalanceTocalculate));
//			nlapiLogExecution('DEBUG','LOA C',Number(carryForwardTocalculate));
//			
//			var searchReturn = {leaveBalance:leaveBalance, carryForward:carryForward, totalLeaves:totalLeaves, giveAlert:giveAlert, getSearch:getSearch};
//			searchReturn = JSON.stringify(searchReturn);
//			response.write(searchReturn);

		}
}