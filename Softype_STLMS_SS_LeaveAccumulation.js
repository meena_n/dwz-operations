/* ************************************************************************************  
 ** Copyright (coffee) 1998-2012 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.
 ** 
 ** This software is the confidential and proprietary information of          
 ** Softype, Inc.("Confidential Information"). You shall not disclose              
 ** such Confidential Information and shall use it only in accordance         
 ** with the terms of the license agreement you entered into with Softype.
 ** @version: Revised version
 ** 
 ** @author: Rutika More
 ** @dated: 
 ** @Description: 
 ************************************************************************************ */

var arraySetup = [];//new Array();
var s = 0;
var search = [];
var countOfSearch = 0;
var count, counts = 0;
var emailDetails = [];
var carryForward;//search[s].getValue('custrecord_stlms_ltsetup_carryforward');
var maxLeaves = 0;//search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
var toBeAddedAnnually;
var annualCompleteLeave = 0;

function scheduled_leaveAccumulation(type) {

	//Run Periodic Leave Accrual status
	var completedStatus = 2;

	//Script Parameters(Values sent from an workflow action script)
	var resetEmpRecord = nlapiGetContext().getSetting('SCRIPT', 'custscript_reset_employee_record');//'F';//
	nlapiLogExecution('Debug', 'reset employee record', resetEmpRecord);

	var runPeriodicAccrl = nlapiGetContext().getSetting('SCRIPT', 'custscript_run_period_accrual_eave_recid');//73;//
	nlapiLogExecution('Debug', 'run periodic accural leave record', runPeriodicAccrl);

	var matchingCalndrMnth = nlapiGetContext().getSetting('SCRIPT', 'custscript_matching_calendar_month');//'T';//
	nlapiLogExecution('Debug', 'matching calendar month', matchingCalndrMnth);

	var fromDate = nlapiGetContext().getSetting('SCRIPT', 'custscript_run_accrual_start_date');//'1/1/2016';//
	nlapiLogExecution('Debug', 'run periodic accrual from date', fromDate);

	var toDate = nlapiGetContext().getSetting('SCRIPT', 'custscript_run_accrual_end_date');//'31/12/2016';//
	nlapiLogExecution('Debug', 'run periodic accrual to date', toDate);

	var empType = nlapiGetContext().getSetting('SCRIPT', 'custscript_run_accrual_emp_type');//3;//
	nlapiLogExecution('Debug', 'run periodic accrual emp type', empType);

	var emailDetail = nlapiGetContext().getSetting('SCRIPT', 'custscript_email_details');
	if(emailDetail)
		emailDetail = JSON.parse(emailDetail);

	nlapiLogExecution('Debug', 'Email Details Not Processed', emailDetail);

	var scriptPara = nlapiGetContext().getSetting('SCRIPT', 'custscript_lastidprocessed');
	nlapiLogExecution('Debug', 'Last processed id', scriptPara);

	//Used to send email
	var author = nlapiGetContext().getSetting('SCRIPT', 'custscript_author');
	nlapiLogExecution('Debug', 'author', author);
	
	var emailId = nlapiGetContext().getSetting('SCRIPT', 'custscript_hr_email_id');
	nlapiLogExecution('Debug', 'emailId', emailId);

	//If the script is not scheduled and is called from the workflow action script with the parameters not empty.
	if(resetEmpRecord != '' && runPeriodicAccrl != '' && matchingCalndrMnth != '' && fromDate != '' && toDate != '' && empType != '' && resetEmpRecord != null && runPeriodicAccrl != null && matchingCalndrMnth != null && fromDate != null && toDate != null && empType != null)// && emailDetail != ''
	{

		if(resetEmpRecord == 'T') {
			nlapiLogExecution('Debug', 'Inside True condition', resetEmpRecord);

			var filterEmp = new Array();
			filterEmp.push(new nlobjSearchFilter('custentity_stlms_periodicaccrualran', null, 'is', 'T'));
			filterEmp.push(new nlobjSearchFilter('employeetype', null, 'is', empType));
			if(scriptPara != '' && scriptPara != null){
				filterEmp.push(new nlobjSearchFilter('internalidnumber',null,'greaterthan',scriptPara));
			}
			var columnEmp = new Array();//nlobjSearchColumn('internalid').setSort();
			columnEmp.push(new nlobjSearchColumn('internalid').setSort());
			columnEmp.push(new nlobjSearchColumn('entityid'));

			var completeSearchEmp = nlapiSearchRecord(null, 'customsearch_stlms_employeesearch', filterEmp, columnEmp);

			var searchEmp = completeSearchEmp;

			//RESCHEDULE SEARCH AFTER THE USAGE LIMIT IS OVER
			while(completeSearchEmp.length == 1000)
			{
				//Get All Search Results
				var lastId = completeSearchEmp[999].getId();
				filterEmp.push( new nlobjSearchFilter('internalid',null,'greaterthan',lastId));
				completeSearchEmp = nlapiSearchRecord(null,'customsearch_stlms_employeesearch',filterEmp,columnEmp);
				searchEmp = searchEmp.concat(completeSearchEmp);
			}

			if(searchEmp) {

				nlapiLogExecution('Debug', 'Inside Search Employee True Condition', searchEmp.length);
				for(var e = 0; e < searchEmp.length; e++) {

					//nlapiLogExecution('Debug', 'Inside Search Employee True Condition Employee Id', searchEmp[e].getId());
					nlapiSubmitField('employee', searchEmp[e].getId(), 'custentity_stlms_periodicaccrualran', 'F');

					//Reschedule after usage limit code
					if (nlapiGetContext().getRemainingUsage() <= 1000)
					{

						var params = new Array();
						params['custscript_lastidprocessed'] = searchEmp[e].getId();
						params['custscript_run_period_accrual_eave_recid'] = runPeriodicAccrl;
						params['custscript_reset_employee_record'] = resetEmpRecord;
						params['custscript_matching_calendar_month'] = matchingCalndrMnth;
						params['custscript_run_accrual_start_date'] = fromDate;
						params['custscript_run_accrual_end_date'] = toDate;
						params['custscript_run_accrual_emp_type'] = empType;
						params['custscript_hr_email_id'] = emailId;
						params['custscript_author'] = author;
						params['custscript_email_details'] = JSON.stringify(emailDetails);

						var status = nlapiScheduleScript('customscript_stlms_leave_accumulation', 'customdeploy_stlms_leave_accumulation', params);

						if (status == 'QUEUED') {
							nlapiLogExecution('Debug', 'End', 'Finished Scheduled Script Due To Usage Limit');
							nlapiLogExecution('Debug', '===================== END =====================');
							return;
						}
					}
				}

				nlapiLogExecution('Debug', 'emailDetails.length', emailDetail.length);
				if(emailDetail.length > 0) {

					var footer = '---This is a system-generated email so please do not reply. If you have any further inquiries, please contact the department concerned directly.---';

					var htmlTag = '';
					htmlTag+='<table style = "border:thin; border-color:#A9A9A9;">';
					htmlTag+='<tr style = "border:thin; border-color:#A9A9A9;">';
					htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic;">'+"Employee"+'</td>';
					htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
					htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic;">'+"Leave Type"+'</td>';
					htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
					htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic;">'+"Reason"+'</td>';
					htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
					htmlTag+='</tr>';

					for(var ed = 0; ed < emailDetail.length; ed++) {

						var entityId = emailDetail[ed].entityId;//
						var leaveTypeName = emailDetail[ed].leaveTypeName;//
						var reason = emailDetail[ed].reason;//

						htmlTag+='<tr style = "border:thin; border-color:#A9A9A9;">';
						htmlTag+='<td style = "border:thin; border-color:#A9A9A9;">'+entityId+'</td>';//
						htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
						htmlTag+='<td style = "border:thin; border-color:#A9A9A9;" align = "left">'+leaveTypeName+'</td>';//
						htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
						htmlTag+='<td style = "border:thin; border-color:#A9A9A9;" align = "left">'+reason+'</td>';//
						htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
						htmlTag+='</tr>';

					}

					htmlTag+='</table>';
					nlapiSendEmail(author, emailId, 'Employees not processed during Periodic Run Accrual.', 'Dear Sir/Madam,<br />FYI: Employees not processed during Periodic Run Accrual.<br /><br />'+" "+'Attached are Employees who are not processed for leave accumulation.<br /><br />'+htmlTag+'<br /><br />Regards,<br />Netsuite<br/><br/>'+footer+'', null, null, null, null);

				}
			}
		}

		else if(resetEmpRecord == 'F') {
			nlapiLogExecution('Debug', 'Inside False condition If', resetEmpRecord);

			var filterEmp = new Array();
			filterEmp.push(new nlobjSearchFilter('custentity_stlms_periodicaccrualran', null, 'is', 'F'));
			filterEmp.push(new nlobjSearchFilter('employeetype', null, 'is', empType));
			if(scriptPara != '' && scriptPara != null){
				filterEmp.push(new nlobjSearchFilter('internalidnumber',null,'greaterthan',scriptPara));
			}
			var columnEmp = new Array();
			columnEmp.push(new nlobjSearchColumn('internalid').setSort());
			columnEmp.push(new nlobjSearchColumn('entityid'));

			var completeSearchEmp = nlapiSearchRecord(null, 'customsearch_stlms_employeesearch', filterEmp, columnEmp);

			var searchEmp = completeSearchEmp;
//			nlapiLogExecution('Debug', 'searchEmp', searchEmp.length);

			//RESCHEDULE SEARCH AFTER THE USAGE LIMIT IS OVER
			while(completeSearchEmp.length == 1000)
			{
				//Get All Search Results
				var lastId = completeSearchEmp[999].getId();
				filterEmp.push( new nlobjSearchFilter('internalid',null,'greaterthan',lastId));
				completeSearchEmp = nlapiSearchRecord(null, 'customsearch_stlms_employeesearch',filterEmp,columnEmp);
				searchEmp = searchEmp.concat(completeSearchEmp);
			}

			if(searchEmp) {

				nlapiLogExecution('Debug', 'Inside Search Employee False Condition', searchEmp.length);
				for(var e = 0; e < searchEmp.length; e++) {

					var leaveType = searchEmp[e].getValue('custentity_stlms_leavetypes');
					var empType = searchEmp[e].getValue('employeetype');
					var entityId = searchEmp[e].getValue('entityid');
					var jobTitle = searchEmp[e].getValue('custentity_stlms_jobtitle');
					var servicePeriod = searchEmp[e].getValue('custentity_stlms_serviceperiod');

					nlapiLogExecution('Debug', 'Leave type', leaveType+'@@'+'Emp jobTitle = '+jobTitle+'@@'+'servicePeriod = '+servicePeriod);
					if(leaveType != '' && leaveType != null) {

						nlapiLogExecution('Debug', 'Inside condition leave type not blank 1', leaveType.length);
						for(var lt = 0; lt < leaveType.length; lt++) {

							var leaveCalculatedBalance = 0;

							nlapiLogExecution('Debug', 'Inside condition leave type not blank', lt);
							if(leaveType[lt] != '' && leaveType[lt] != null) {

								var leaveTypeName = nlapiLookupField('customlist_stlms_leavetypes',leaveType[lt], 'name');
								var leaveAccumulation = Number(0);

								nlapiLogExecution('Debug', 'Inside condition leave type not blank 2', leaveType[lt]);

								//If Job Title is not blank on the employee record.
								if(jobTitle) {

									//Search Leave Type Setup for details
									var filter = new Array();
									filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
									filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

									var column = new Array();
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

									search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

									//If Job title and Min and Max values are not blank.
									if(search) {

										nlapiLogExecution('Debug', 'If Case 1');
										nlapiLogExecution('Debug', 'If Case 1 leave type search.length = ', search.length);
										for(s = 0; s < search.length; s++) {

											var minexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmin');
											var maxexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmax');

											if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

												var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
												nlapiLogExecution('Debug', 'If Case 1 leave calculation formula', formula);
												var recID = search[s].getId();

												toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
												annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

												leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
												nlapiLogExecution('Debug', 'If Case 1 leaveAccumulation', leaveAccumulation);

												carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
												maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

											}
										}
									}

									//If Job Title is blank on Run Periodic Record and Min and Max values are not blank.
									else {

										var filter = new Array();
										filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
										filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

										var column = new Array();
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

										search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

										if(search) {

											nlapiLogExecution('Debug', 'If Case 2');
											nlapiLogExecution('Debug', 'If Case 2 leave type search.length = ', search.length);
											for(s = 0; s < search.length; s++) {

												var minexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmin');
												var maxexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmax');

												if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

													var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
													nlapiLogExecution('Debug', 'If Case 2 leave calculation formula', formula);
													var recID = search[s].getId();

													toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
													annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

													leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
													nlapiLogExecution('Debug', 'If Case 2 leaveAccumulation', leaveAccumulation);

													carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
													maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

												}

											}

										}

										//If Job Title is not blank on Run Periodic Record and Min and Max values are blank
										else {

											var filter = new Array();
											filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
											filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
											filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
											filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
											filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty',null));
											filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

											var column = new Array();
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
											column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

											search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

											if(search) {

												nlapiLogExecution('Debug', 'If Case 3');
												nlapiLogExecution('Debug', 'If Case 3 leave type search.length = ', search.length);
												for(s = 0; s < search.length; s++) {

													var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
													nlapiLogExecution('Debug', 'If Case 3 leave calculation formula', formula);
													var recID = search[s].getId();

													toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
													annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

													leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
													nlapiLogExecution('Debug', 'If Case 3 leaveAccumulation', leaveAccumulation);

													carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
													maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

												}

											}

											//If Job Title and Min and Max values are blank on Run Periodic Record.
											else {

												var filter = new Array();
												filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
												filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty',null));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

												var column = new Array();
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

												search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

												if(search) {

													nlapiLogExecution('Debug', 'If Case 4');
													nlapiLogExecution('Debug', 'If Case 4 leave type search.length = ', search.length);
													for(s = 0; s < search.length; s++) {

														var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
														nlapiLogExecution('Debug', 'If Case 4 leave calculation formula', formula);
														var recID = search[s].getId();

														toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
														annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

														leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
														nlapiLogExecution('Debug', 'If Case 4 leaveAccumulation', leaveAccumulation);

														carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
														maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

													}

												}

											}

										}

									}

								}

								//If Job Title is blank on Employee record.
								else {

									var filter = new Array();
									filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
									filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
									filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

									var column = new Array();
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
									column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

									search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

									//If Min and Max values are not blank on Run Periodic Record.
									if(search) {

										nlapiLogExecution('Debug', 'Else Case 1');
										nlapiLogExecution('Debug', 'Else Case 1 Inside Job Title blank on Employee search.length', search.length);
										for(s = 0; s < search.length; s++) {

											var minexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmin');
											var maxexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmax');

											if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

												var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
												nlapiLogExecution('Debug', 'Else Case 1 leave calculation formula', formula);
												var recID = search[s].getId();

												toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
												annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

												leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
												nlapiLogExecution('Debug', 'Else Case 1 leaveAccumulation', leaveAccumulation);

												carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
												maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

											}

										}

									}

									//If All are blank calculate with the normal flow with only the employee type and leave type.
									else {

										var filter = new Array();
										filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
										filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty',null));
										filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

										var column = new Array();
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
										column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

										search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

										if(search) {

											nlapiLogExecution('Debug', 'Else Case 2');
											nlapiLogExecution('Debug', 'Else Case 2 leave type search.length = ', search.length);
											for(s = 0; s < search.length; s++) {

												var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
												nlapiLogExecution('Debug', 'Else Case 2 leave calculation formula', formula);
												var recID = search[s].getId();

												toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
												annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

												leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
												nlapiLogExecution('Debug', 'Else Case 2 leaveAccumulation', leaveAccumulation);

												carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
												maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

											}

										}

									}

								}

								if(leaveAccumulation != 0) {

									//Search Leave Master Line for current employee to update leaves
									var filterLeaveBlncLine = new Array();
									filterLeaveBlncLine.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
									filterLeaveBlncLine.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', searchEmp[e].getId()));
									filterLeaveBlncLine.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveType[lt]));

									var columnLeaveBlncLine = new Array();
									columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbm_asondate', 'custrecord_stlms_lbline_parent'));
									columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbm_fromdate', 'custrecord_stlms_lbline_parent'));
									columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbm_todate', 'custrecord_stlms_lbline_parent'));
									columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_parent'));
									columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
									columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
									columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_leaveutilized'));

									var searchLeaveBlncLine = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filterLeaveBlncLine, columnLeaveBlncLine);

									if(searchLeaveBlncLine) {

										nlapiLogExecution('Debug', 'Inside search leave balance line', searchLeaveBlncLine.length);

//										for(var l = 0; l < searchLeaveBlncLine.length; l++) {

										var todaysDate = new Date();
										todaysDate = nlapiDateToString(todaysDate);//nlapiStringToDate(todaysDate);

										var parent = searchLeaveBlncLine[0].getValue('custrecord_stlms_lbline_parent');
										nlapiLogExecution('Debug', 'parent', parent);

										var loadRecord = nlapiLoadRecord('customrecord_stlms_leavebalancemasterlin',searchLeaveBlncLine[0].getId());

										var getCarriedForward = searchLeaveBlncLine[0].getValue('custrecord_stlms_lbline_carriedforward');
										var getLeaveBalance = searchLeaveBlncLine[0].getValue('custrecord_stlms_lbline_leavebalance');
										var previousYearBalance = Number(getLeaveBalance) + Number(getCarriedForward);
										var temp = 0;

										//If Start Month matches Run Accural Month
										if(matchingCalndrMnth == 'T') {
											
											if(toBeAddedAnnually == 'T')
												leaveAccumulation = Number(annualCompleteLeave);

											//Hidden Field to be set as on the end date of the previous calendar year.
											previousYearBalance = parseFloat((previousYearBalance).toFixed(2));
											loadRecord.setFieldValue('custrecord_stmm_lbline_leavebalprevious', previousYearBalance);

											//Compare minimum value between current leaves and max to be carry forward
											if(Number(maxLeaves) > Number(getLeaveBalance)) {
												temp = Number(getLeaveBalance);
											}
											else {
												temp = Number(maxLeaves);
											}

											//If carry forward is true set field as zero and add previous value to new leave
											if(carryForward == 'T') {

												getCarriedForward = Number(temp);
												loadRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', 0);
											}
											else {
												loadRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', Number(temp));
												temp = Number(0);

											}
										}
										//For other months
										else {
											
											if(toBeAddedAnnually == 'T') {
												
												temp = Number(0);
												leaveAccumulation = Number(getLeaveBalance);
											}
											
											else
												temp = Number(getLeaveBalance);
										}

										//Leave Balance Calculation = Calculated Leaves from setup + Carry forward if any
										getLeaveBalance = Number(leaveAccumulation) + Number(temp);
										getLeaveBalance = parseFloat((getLeaveBalance).toFixed(2));
										nlapiLogExecution('Debug', 'Leave Balance to be set', getLeaveBalance);

										loadRecord.setFieldValue('custrecord_stlms_lbline_leaveutilized', 0);
										loadRecord.setFieldValue('custrecord_stlms_lbline_leavebalance', getLeaveBalance);
										nlapiSubmitRecord(loadRecord);
//										nlapiLogExecution('Debug', 'loadRecord', loadRecord);

										if(matchingCalndrMnth == 'T') {
											nlapiSubmitField('customrecord_stlms_leavebalancemaster', parent, ['custrecord_stlms_lbm_asondate', 'custrecord_stlms_lbm_fromdate', 'custrecord_stlms_lbm_todate'], [todaysDate, fromDate, toDate]);
										}

										else {
											nlapiSubmitField('customrecord_stlms_leavebalancemaster', parent, ['custrecord_stlms_lbm_asondate', 'custrecord_stlms_lbm_todate'], [todaysDate, toDate]);
										}
										nlapiLogExecution('Debug', 'after parent submit');

//										}
									}

									else {

										var scriptId = nlapiGetContext().getScriptId();
										var empName = nlapiLookupField('employee', searchEmp[e].getId(),'entityid');
										var errorMsg = 'No Leave Balance Line Record for employee '+empName;

										var createRecord = nlapiCreateRecord('customrecord_stlms_errorcatcher');
										createRecord.setFieldValue('custrecord_stlms_ecr_employeename',searchEmp[e].getId());
										createRecord.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
										createRecord.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

										var createdRecord = nlapiSubmitRecord(createRecord,true,true);
										nlapiLogExecution('DEBUG', 'createdRecord Error catcher',createdRecord);

										counts = counts + Number(1);

										emailDetails.push({

											count: counts,
											entityId: entityId,
											leaveTypeName: leaveTypeName,
											reason: 'No Leave Balance Line Record Found.',

										});

									}
								}
//								}
//								}
								else {

									var scriptId = nlapiGetContext().getScriptId();
									var leaveTypes= leaveType[lt];
									var errorMsg = 'No Leave Type Setup Found for '+leaveTypes;

									var createRecord = nlapiCreateRecord('customrecord_stlms_errorcatcher');
									createRecord.setFieldValue('custrecord_stlms_ecr_employeename',searchEmp[e].getId());
									createRecord.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
									createRecord.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

									var createdRecord = nlapiSubmitRecord(createRecord,true,true);
									nlapiLogExecution('DEBUG', 'createdRecord Error catcher',createdRecord);

									count = count + Number(1);

									emailDetails.push({

										count:count,
										entityId:entityId,
										leaveTypeName:leaveTypeName,
										reason: 'No Leave Type Setup Found.',

									});

								}

							}
							//To get the odd iteration which is the actual internal id's
							lt++;
						}
						//Mark Employee being processed
						nlapiSubmitField('employee', searchEmp[e].getId(),'custentity_stlms_periodicaccrualran', 'T');
					}

					//Reschedule after usage limit code
					if (nlapiGetContext().getRemainingUsage() <= 1000)
					{

						var params = new Array();
						params['custscript_lastidprocessed'] = searchEmp[e].getId();
						params['custscript_run_period_accrual_eave_recid'] = runPeriodicAccrl;
						params['custscript_reset_employee_record'] = resetEmpRecord;
						params['custscript_matching_calendar_month'] = matchingCalndrMnth;
						params['custscript_run_accrual_start_date'] = fromDate;
						params['custscript_run_accrual_end_date'] = toDate;
						params['custscript_run_accrual_emp_type'] = empType;
						params['custscript_hr_email_id'] = emailId;
						params['custscript_author'] = author;
						params['custscript_email_details'] = JSON.stringify(emailDetails);

						var status = nlapiScheduleScript('customscript_stlms_leave_accumulation', 'customdeploy_stlms_leave_accumulation', params);

						if (status == 'QUEUED') {
							nlapiLogExecution('Debug', 'End', 'Finished Scheduled Script Due To Usage Limit');
							nlapiLogExecution('Debug', '===================== END =====================');
							return;
						}
					}

					countOfSearch = Number(countOfSearch) + Number(1);
				}
			}

			//Submit Run Accrual Record Status = Completed
			var submitRecord = nlapiSubmitField('customrecord_stlms_runaccrual', runPeriodicAccrl, 'custrecord_stlms_rpla_status', completedStatus);

			//After all records are processed re-set the checkbox to false for next accrual
			if(countOfSearch == searchEmp.length) {

				var params = [];
//				params['custscript_lastidprocessed'] = searchEmp[e].getId();
				params['custscript_run_period_accrual_eave_recid'] = runPeriodicAccrl;
//				params['custscript_reset_employee_record'] = resetEmpRecord;
				params['custscript_matching_calendar_month'] = matchingCalndrMnth;
				params['custscript_run_accrual_start_date'] = fromDate;
				params['custscript_run_accrual_end_date'] = toDate;
				params['custscript_reset_employee_record'] = 'T';
				params['custscript_run_accrual_emp_type'] = empType;
				params['custscript_hr_email_id'] = emailId;
				params['custscript_author'] = author;
				params['custscript_email_details'] = JSON.stringify(emailDetails);

				var status = nlapiScheduleScript('customscript_stlms_leave_accumulation', 'customdeploy_stlms_leave_accumulation', params);
				nlapiLogExecution('Debug', 'Scheduled Status', status);

			}
		}

		return;
	}

	//If the script is a scheduled script it has to perform all the roles of the complete process from creation of the Run Periodic Leave Accrual Record upto upto the leaves of Employee.
	else {

		var getSearchResult = getSetup();

		if(getSearchResult != '') {

			nlapiLogExecution('Debug', 'Get setup length', getSearchResult.length);

			for(var gs = 0; gs < getSearchResult.length; gs++) {

				if(getSearchResult[gs].getValue('custrecord_stlms_su_periodicaccrualauto') == 'T') {

					//Run Periodic Leave Accrual status
					var completedStatus = 2;

					//Script Parameters(Values sent from an workflow action script)
					var empType = getSearchResult[gs].getValue('custrecord_stlms_su_employeetype');
					nlapiLogExecution('Debug', 'run periodic accrual emp type', empType);

					var frequency = getSearchResult[gs].getValue('custrecord_stlms_su_frequencyofaccrual');
					nlapiLogExecution('Debug', 'run periodic accrual frequency', frequency);

					var calendarStartDate = getSearchResult[gs].getValue('custrecord_stlms_su_calendarstartdate');
					nlapiLogExecution('Debug', 'run periodic accrual calendarStartDate', calendarStartDate);

					var calendarEndDate = getSearchResult[gs].getValue('custrecord_stlms_su_calendarenddate');
					nlapiLogExecution('Debug', 'run periodic accrual calendarEndDate', calendarEndDate);

					//Create Run Periodic Leave Accrual Record based on the Employee Type being processed.
					var runAccrualRecord = createRunPeriodicAccrualRecord(empType, frequency, calendarStartDate, calendarEndDate);
					runAccrualRecord = JSON.parse(runAccrualRecord);
					nlapiLogExecution('Debug', 'Run Accrual Record Creation', JSON.stringify(runAccrualRecord));

					var resetEmpRecord = nlapiGetContext().getSetting('SCRIPT', 'custscript_reset_employee_record');//'F';//
					nlapiLogExecution('Debug', 'reset employee record', resetEmpRecord);

					if(resetEmpRecord == '' || resetEmpRecord == null)
						resetEmpRecord = 'F';

					else
						resetEmpRecord = resetEmpRecord;

					var matchingCalndrMnth = nlapiGetContext().getSetting('SCRIPT', 'custscript_matching_calendar_month');//'T';//
					nlapiLogExecution('Debug', 'matching calendar month', matchingCalndrMnth);

					var leaveCalendrStMonth = getSearchResult[gs].getValue('custrecord_stlms_su_leavecalendarstart');

					if(matchingCalndrMnth == '' || matchingCalndrMnth == null) {

						var periodLeaveStMonth = runAccrualRecord.fromDate;
						if(periodLeaveStMonth)
						{
							periodLeaveStMonth = nlapiStringToDate(periodLeaveStMonth);
							periodLeaveStMonth = periodLeaveStMonth.getMonth() + Number(1);
						}

						if(leaveCalendrStMonth == periodLeaveStMonth)
							matchingCalndrMnth = 'T';
						else
							matchingCalndrMnth = 'F';

					}
					else
						matchingCalndrMnth = matchingCalndrMnth;

					var emailDetail = nlapiGetContext().getSetting('SCRIPT', 'custscript_email_details');
					if(emailDetail)
						emailDetail = JSON.parse(emailDetail);

					nlapiLogExecution('Debug', 'Email Details Not Processed', emailDetail);

					var scriptPara = nlapiGetContext().getSetting('SCRIPT', 'custscript_lastidprocessed');
					nlapiLogExecution('Debug', 'Last processed id', scriptPara);

					var runPeriodicAccrl = runAccrualRecord.runAccrual;//nlapiGetContext().getSetting('SCRIPT', 'custscript_run_period_accrual_eave_recid');//73;//
					nlapiLogExecution('Debug', 'run periodic accural leave record', runPeriodicAccrl);

					var fromDate = runAccrualRecord.fromDate;//nlapiGetContext().getSetting('SCRIPT', 'custscript_run_accrual_start_date');//'1/1/2016';//
					nlapiLogExecution('Debug', 'run periodic accrual from date', fromDate);

					var toDate = runAccrualRecord.toDate;//nlapiGetContext().getSetting('SCRIPT', 'custscript_run_accrual_end_date');//'31/12/2016';//
					nlapiLogExecution('Debug', 'run periodic accrual to date', toDate);

					var role = getSearchResult[gs].getValue('custrecord_stlms_su_hrapproverrole');
					
					//Used to send email
//					var author = getEmployee(role);//nlapiGetUser();//'10';
//					nlapiLogExecution('Debug', 'author', author);
					
					var emailId = getSearchResult[gs].getValue('custrecord_stlms_su_hremailid');//nlapiLookupField('employee', author, 'email');//'rutikam@softype.com';
					nlapiLogExecution('Debug', 'emailId', emailId);
					
					var author = getSender(emailId);
					nlapiLogExecution('Debug', 'author', author);

					if(resetEmpRecord == 'T') {
						nlapiLogExecution('Debug', 'Inside True condition', resetEmpRecord);

						var filterEmp = new Array();
						filterEmp.push(new nlobjSearchFilter('custentity_stlms_periodicaccrualran', null, 'is', 'T'));
						filterEmp.push(new nlobjSearchFilter('employeetype', null, 'is', empType));
						if(scriptPara != '' && scriptPara != null){
							filterEmp.push(new nlobjSearchFilter('internalidnumber',null,'greaterthan',scriptPara));
						}
						var columnEmp = new Array();//nlobjSearchColumn('internalid').setSort();
						columnEmp.push(new nlobjSearchColumn('internalid').setSort());
						columnEmp.push(new nlobjSearchColumn('entityid'));

						var completeSearchEmp = nlapiSearchRecord(null, 'customsearch_stlms_employeesearch', filterEmp, columnEmp);

						var searchEmp = completeSearchEmp;

						//RESCHEDULE SEARCH AFTER THE USAGE LIMIT IS OVER
						while(completeSearchEmp.length == 1000)
						{
							//Get All Search Results
							var lastId = completeSearchEmp[999].getId();
							filterEmp.push( new nlobjSearchFilter('internalid',null,'greaterthan',lastId));
							completeSearchEmp = nlapiSearchRecord(null,'customsearch_stlms_employeesearch',filterEmp,columnEmp);
							searchEmp = searchEmp.concat(completeSearchEmp);
						}

						if(searchEmp) {

							nlapiLogExecution('Debug', 'Inside Search Employee True Condition', searchEmp.length);
							for(var e = 0; e < searchEmp.length; e++) {

								//nlapiLogExecution('Debug', 'Inside Search Employee True Condition Employee Id', searchEmp[e].getId());
								nlapiSubmitField('employee', searchEmp[e].getId(), 'custentity_stlms_periodicaccrualran', 'F');

								//Reschedule after usage limit code
								if (nlapiGetContext().getRemainingUsage() <= 1000)
								{

									var params = new Array();
									params['custscript_lastidprocessed'] = searchEmp[e].getId();
									params['custscript_run_period_accrual_eave_recid'] = runPeriodicAccrl;
									params['custscript_reset_employee_record'] = resetEmpRecord;
									params['custscript_matching_calendar_month'] = matchingCalndrMnth;
									params['custscript_run_accrual_start_date'] = fromDate;
									params['custscript_run_accrual_end_date'] = toDate;
									params['custscript_run_accrual_emp_type'] = empType;
									params['custscript_hr_email_id'] = emailId;
									params['custscript_author'] = author;
									params['custscript_email_details'] = JSON.stringify(emailDetails);

									var status = nlapiScheduleScript('customscript_stlms_leave_accumulation', 'customdeploy_stlms_leave_accumulation', params);

									if (status == 'QUEUED') {
										nlapiLogExecution('Debug', 'End', 'Finished Scheduled Script Due To Usage Limit');
										nlapiLogExecution('Debug', '===================== END =====================');
										return;
									}
								}
							}

							nlapiLogExecution('Debug', 'emailDetails.length', emailDetail.length);
							if(emailDetail.length > 0) {

								var footer = '---This is a system-generated email so please do not reply. If you have any further inquiries, please contact the department concerned directly.---';

								var htmlTag = '';
								htmlTag+='<table style = "border:thin; border-color:#A9A9A9;">';
								htmlTag+='<tr style = "border:thin; border-color:#A9A9A9;">';
								htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic;">'+"Employee"+'</td>';
								htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
								htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic;">'+"Leave Type"+'</td>';
								htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
								htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic;">'+"Reason"+'</td>';
								htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
								htmlTag+='</tr>';

								for(var ed = 0; ed < emailDetail.length; ed++) {

									var entityId = emailDetail[ed].entityId;//
									var leaveTypeName = emailDetail[ed].leaveTypeName;//
									var reason = emailDetail[ed].reason;//

									htmlTag+='<tr style = "border:thin; border-color:#A9A9A9;">';
									htmlTag+='<td style = "border:thin; border-color:#A9A9A9;">'+entityId+'</td>';//
									htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
									htmlTag+='<td style = "border:thin; border-color:#A9A9A9;" align = "left">'+leaveTypeName+'</td>';//
									htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
									htmlTag+='<td style = "border:thin; border-color:#A9A9A9;" align = "left">'+reason+'</td>';//
									htmlTag+='<td style = "border:thin; border-color:#A9A9A9; font-style:italic; width:50px">'+""+'</td>';
									htmlTag+='</tr>';

								}

								htmlTag+='</table>';
								nlapiSendEmail(author, emailId, 'Employees not processed during Periodic Run Accrual.', 'Dear Sir/Madam,<br />FYI: Employees not processed during Periodic Run Accrual.<br /><br />'+" "+'Attached are Employees who are not processed for leave accumulation.<br /><br />'+htmlTag+'<br /><br />Regards,<br />Netsuite<br/><br/>'+footer+'', null, null, null, null);

							}
						}
					}

					else if(resetEmpRecord == 'F') {
						nlapiLogExecution('Debug', 'Inside False condition', resetEmpRecord);

						var filterEmp = new Array();
						filterEmp.push(new nlobjSearchFilter('custentity_stlms_periodicaccrualran', null, 'is', 'F'));
						filterEmp.push(new nlobjSearchFilter('employeetype', null, 'is', empType));
						if(scriptPara != '' && scriptPara != null){
							filterEmp.push(new nlobjSearchFilter('internalidnumber',null,'greaterthan',scriptPara));
						}
						var columnEmp = new Array();
						columnEmp.push(new nlobjSearchColumn('internalid').setSort());
						columnEmp.push(new nlobjSearchColumn('entityid'));

						var completeSearchEmp = nlapiSearchRecord(null, 'customsearch_stlms_employeesearch', filterEmp, columnEmp);

						var searchEmp = completeSearchEmp;
						nlapiLogExecution('Debug', 'searchEmp', searchEmp.length);

						//RESCHEDULE SEARCH AFTER THE USAGE LIMIT IS OVER
						while(completeSearchEmp.length == 1000)
						{
							//Get All Search Results
							var lastId = completeSearchEmp[999].getId();
							filterEmp.push( new nlobjSearchFilter('internalid',null,'greaterthan',lastId));
							completeSearchEmp = nlapiSearchRecord(null, 'customsearch_stlms_employeesearch',filterEmp,columnEmp);
							searchEmp = searchEmp.concat(completeSearchEmp);
						}

						if(searchEmp) {

							nlapiLogExecution('Debug', 'Inside Search Employee False Condition', searchEmp.length);
							for(var e = 0; e < searchEmp.length; e++) {

								var leaveType = searchEmp[e].getValue('custentity_stlms_leavetypes');
								var empType = searchEmp[e].getValue('employeetype');
								var entityId = searchEmp[e].getValue('entityid');
								var jobTitle = searchEmp[e].getValue('custentity_stlms_jobtitle');
								var servicePeriod = searchEmp[e].getValue('custentity_stlms_serviceperiod');

								nlapiLogExecution('Debug', 'Leave type', leaveType+'@@'+'Emp jobTitle = '+jobTitle+'@@'+'servicePeriod = '+servicePeriod);
								if(leaveType != '' && leaveType != null) {

									nlapiLogExecution('Debug', 'Inside condition leave type not blank 1', leaveType.length);
									for(var lt = 0; lt < leaveType.length; lt++) {

										var leaveCalculatedBalance = 0;

										nlapiLogExecution('Debug', 'Inside condition leave type not blank', lt);
										if(leaveType[lt] != '' && leaveType[lt] != null) {

											var leaveTypeName = nlapiLookupField('customlist_stlms_leavetypes',leaveType[lt], 'name');
											var leaveAccumulation = Number(0);

											nlapiLogExecution('Debug', 'Inside condition leave type not blank 2', leaveType[lt]);

											//If Job Title is not blank on the employee record.
											if(jobTitle) {

												//Search Leave Type Setup for details
												var filter = new Array();
												filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
												filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

												var column = new Array();
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

												search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

												//If Job title and Min and Max values are not blank.
												if(search) {

													nlapiLogExecution('Debug', 'If Case 1');
													nlapiLogExecution('Debug', 'If Case 1 leave type search.length = ', search.length);
													for(s = 0; s < search.length; s++) {

														var minexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmin');
														var maxexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmax');

														if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

															var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
															nlapiLogExecution('Debug', 'If Case 1 leave calculation formula', formula);
															var recID = search[s].getId();

															toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
															annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

															leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
															nlapiLogExecution('Debug', 'If Case 1 leaveAccumulation', leaveAccumulation);

															carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
															maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

														}
													}
												}

												//If Job Title is blank on Run Periodic Record and Min and Max values are not blank.
												else {

													var filter = new Array();
													filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
													filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

													var column = new Array();
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

													search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

													if(search) {

														nlapiLogExecution('Debug', 'If Case 2');
														nlapiLogExecution('Debug', 'If Case 2 leave type search.length = ', search.length);
														for(s = 0; s < search.length; s++) {

															var minexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmin');
															var maxexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmax');

															if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

																var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
																nlapiLogExecution('Debug', 'If Case 2 leave calculation formula', formula);
																var recID = search[s].getId();

																toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
																annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

																leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
																nlapiLogExecution('Debug', 'If Case 2 leaveAccumulation', leaveAccumulation);

																carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
																maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

															}

														}

													}

													//If Job Title is not blank on Run Periodic Record and Min and Max values are blank
													else {

														var filter = new Array();
														filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
														filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
														filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
														filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
														filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty',null));
														filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

														var column = new Array();
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
														column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

														search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

														if(search) {

															nlapiLogExecution('Debug', 'If Case 3');
															nlapiLogExecution('Debug', 'If Case 3 leave type search.length = ', search.length);
															for(s = 0; s < search.length; s++) {

																var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
																nlapiLogExecution('Debug', 'If Case 3 leave calculation formula', formula);
																var recID = search[s].getId();

																toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
																annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

																leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
																nlapiLogExecution('Debug', 'If Case 3 leaveAccumulation', leaveAccumulation);

																carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
																maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

															}

														}

														//If Job Title and Min and Max values are blank on Run Periodic Record.
														else {

															var filter = new Array();
															filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
															filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
															filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
															filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
															filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty',null));
															filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

															var column = new Array();
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
															column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

															search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

															if(search) {

																nlapiLogExecution('Debug', 'If Case 4');
																nlapiLogExecution('Debug', 'If Case 4 leave type search.length = ', search.length);
																for(s = 0; s < search.length; s++) {

																	var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
																	nlapiLogExecution('Debug', 'If Case 4 leave calculation formula', formula);
																	var recID = search[s].getId();

																	toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
																	annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

																	leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
																	nlapiLogExecution('Debug', 'If Case 4 leaveAccumulation', leaveAccumulation);

																	carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
																	maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

																}

															}

														}

													}

												}

											}

											//If Job Title is blank on Employee record.
											else {

												var filter = new Array();
												filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
												filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
												filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

												var column = new Array();
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
												column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

												search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

												//If Min and Max values are not blank on Run Periodic Record.
												if(search) {

													nlapiLogExecution('Debug', 'Else Case 1');
													nlapiLogExecution('Debug', 'Else Case 1 Inside Job Title blank on Employee search.length', search.length);
													for(s = 0; s < search.length; s++) {

														var minexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmin');
														var maxexprnc = search[s].getValue('custrecord_stlms_ltsetup_expmax');

														if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

															var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
															nlapiLogExecution('Debug', 'Else Case 1 leave calculation formula', formula);
															var recID = search[s].getId();

															toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
															annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

															leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
															nlapiLogExecution('Debug', 'Else Case 1 leaveAccumulation', leaveAccumulation);

															carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
															maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

														}

													}

												}

												//If All are blank calculate with the normal flow with only the employee type and leave type.
												else {

													var filter = new Array();
													filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
													filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', empType));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType[lt]));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty',null));
													filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

													var column = new Array();
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_jobtitle'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
													column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

													search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

													if(search) {

														nlapiLogExecution('Debug', 'Else Case 2');
														nlapiLogExecution('Debug', 'Else Case 2 leave type search.length = ', search.length);
														for(s = 0; s < search.length; s++) {

															var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
															nlapiLogExecution('Debug', 'Else Case 2 leave calculation formula', formula);
															var recID = search[s].getId();

															toBeAddedAnnually = search[s].getValue('custrecord_stlms_ltsetup_appliedannually');
															annualCompleteLeave = search[s].getValue('custrecord_stlms_ltsetup_accrualfactor');

															leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
															nlapiLogExecution('Debug', 'Else Case 2 leaveAccumulation', leaveAccumulation);

															carryForward = search[s].getValue('custrecord_stlms_ltsetup_carryforward');
															maxLeaves = search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo');

														}

													}

												}

											}

											if(leaveAccumulation != 0) {

												//Search Leave Master Line for current employee to update leaves
												var filterLeaveBlncLine = new Array();
												filterLeaveBlncLine.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
												filterLeaveBlncLine.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', searchEmp[e].getId()));
												filterLeaveBlncLine.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveType[lt]));

												var columnLeaveBlncLine = new Array();
												columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbm_asondate', 'custrecord_stlms_lbline_parent'));
												columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbm_fromdate', 'custrecord_stlms_lbline_parent'));
												columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbm_todate', 'custrecord_stlms_lbline_parent'));
												columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_parent'));
												columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
												columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
												columnLeaveBlncLine.push(new nlobjSearchColumn('custrecord_stlms_lbline_leaveutilized'));

												var searchLeaveBlncLine = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filterLeaveBlncLine, columnLeaveBlncLine);

												if(searchLeaveBlncLine) {

													nlapiLogExecution('Debug', 'Inside search leave balance line', searchLeaveBlncLine.length);

//													for(var l = 0; l < searchLeaveBlncLine.length; l++) {

													var todaysDate = new Date();
													todaysDate = nlapiDateToString(todaysDate);//nlapiStringToDate(todaysDate);

													var parent = searchLeaveBlncLine[0].getValue('custrecord_stlms_lbline_parent');
													nlapiLogExecution('Debug', 'parent', parent);

													var loadRecord = nlapiLoadRecord('customrecord_stlms_leavebalancemasterlin',searchLeaveBlncLine[0].getId());

													var getCarriedForward = searchLeaveBlncLine[0].getValue('custrecord_stlms_lbline_carriedforward');
													var getLeaveBalance = searchLeaveBlncLine[0].getValue('custrecord_stlms_lbline_leavebalance');
													var previousYearBalance = Number(getLeaveBalance) + Number(getCarriedForward);
													var temp = 0;

													//If Start Month matches Run Accural Month
													if(matchingCalndrMnth == 'T') {
														
														if(toBeAddedAnnually == 'T')
															leaveAccumulation = Number(annualCompleteLeave);
														
														//Hidden Field to be set as on the end date of the previous calendar year.
														previousYearBalance = parseFloat((previousYearBalance).toFixed(2));
														loadRecord.setFieldValue('custrecord_stmm_lbline_leavebalprevious', previousYearBalance);
//														loadRecord.setFieldValue('custrecord_stlms_lbline_leavebalance', getLeaveBalance);

														//Compare minimum value between current leaves and max to be carry forward
														if(Number(maxLeaves) > Number(getLeaveBalance)) {
															temp = Number(getLeaveBalance);
														}
														else {
															temp = Number(maxLeaves);
														}

														//If carry forward is true set field as zero and add previous value to new leave
														if(carryForward == 'T') {

															getCarriedForward = Number(temp);
															loadRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', 0);
														}
														else {
															loadRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', Number(temp));
															temp = Number(0);

														}
													}
													//For other months
													else {
														
														if(toBeAddedAnnually == 'T') {
															
															temp = Number(0);
															leaveAccumulation = Number(getLeaveBalance);
														}
														
														else
															temp = Number(getLeaveBalance);
													
													}

													//Leave Balance Calculation = Calculated Leaves from setup + Carry forward if any
													getLeaveBalance = Number(leaveAccumulation) + Number(temp);
													getLeaveBalance = parseFloat((getLeaveBalance).toFixed(2));
													nlapiLogExecution('Debug', 'Leave Balance to be set', getLeaveBalance);

													loadRecord.setFieldValue('custrecord_stlms_lbline_leaveutilized', 0);
													loadRecord.setFieldValue('custrecord_stlms_lbline_leavebalance', getLeaveBalance);
													nlapiSubmitRecord(loadRecord);
//													nlapiLogExecution('Debug', 'loadRecord', loadRecord);

													if(matchingCalndrMnth == 'T') {
														nlapiSubmitField('customrecord_stlms_leavebalancemaster', parent, ['custrecord_stlms_lbm_asondate', 'custrecord_stlms_lbm_fromdate', 'custrecord_stlms_lbm_todate'], [todaysDate, fromDate, toDate]);
													}

													else {
														nlapiSubmitField('customrecord_stlms_leavebalancemaster', parent, ['custrecord_stlms_lbm_asondate', 'custrecord_stlms_lbm_todate'], [todaysDate, toDate]);
													}
													nlapiLogExecution('Debug', 'after parent submit');

//													}
												}

												else {

													var scriptId = nlapiGetContext().getScriptId();
													var empName = nlapiLookupField('employee', searchEmp[e].getId(),'entityid');
													var errorMsg = 'No Leave Balance Line Record for employee '+empName;

													var createRecord = nlapiCreateRecord('customrecord_stlms_errorcatcher');
													createRecord.setFieldValue('custrecord_stlms_ecr_employeename',searchEmp[e].getId());
													createRecord.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
													createRecord.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

													var createdRecord = nlapiSubmitRecord(createRecord,true,true);
													nlapiLogExecution('DEBUG', 'createdRecord Error catcher',createdRecord);

													counts = counts + Number(1);

													emailDetails.push({

														count: counts,
														entityId: entityId,
														leaveTypeName: leaveTypeName,
														reason: 'No Leave Balance Line Record Found.',

													});

												}
											}
//											}
//											}
											else {

												var scriptId = nlapiGetContext().getScriptId();
												var leaveTypes= leaveType[lt];
												var errorMsg = 'No Leave Type Setup Found for '+leaveTypes;

												var createRecord = nlapiCreateRecord('customrecord_stlms_errorcatcher');
												createRecord.setFieldValue('custrecord_stlms_ecr_employeename',searchEmp[e].getId());
												createRecord.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
												createRecord.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

												var createdRecord = nlapiSubmitRecord(createRecord,true,true);
												nlapiLogExecution('DEBUG', 'createdRecord Error catcher',createdRecord);

												count = count + Number(1);

												emailDetails.push({

													count:count,
													entityId:entityId,
													leaveTypeName:leaveTypeName,
													reason: 'No Leave Type Setup Found.',

												});

											}

										}
										//To get the odd iteration which is the actual internal id's
										lt++;
									}
									//Mark Employee being processed
									nlapiSubmitField('employee', searchEmp[e].getId(),'custentity_stlms_periodicaccrualran', 'T');
								}

								//Reschedule after usage limit code
								if (nlapiGetContext().getRemainingUsage() <= 1000)
								{

									var params = new Array();
									params['custscript_lastidprocessed'] = searchEmp[e].getId();
									params['custscript_run_period_accrual_eave_recid'] = runPeriodicAccrl;
									params['custscript_reset_employee_record'] = resetEmpRecord;
									params['custscript_matching_calendar_month'] = matchingCalndrMnth;
									params['custscript_run_accrual_start_date'] = fromDate;
									params['custscript_run_accrual_end_date'] = toDate;
									params['custscript_run_accrual_emp_type'] = empType;
									params['custscript_hr_email_id'] = emailId;
									params['custscript_author'] = author;
									params['custscript_email_details'] = JSON.stringify(emailDetails);

									var status = nlapiScheduleScript('customscript_stlms_leave_accumulation', 'customdeploy_stlms_leave_accumulation', params);

									if (status == 'QUEUED') {
										nlapiLogExecution('Debug', 'End', 'Finished Scheduled Script Due To Usage Limit');
										nlapiLogExecution('Debug', '===================== END =====================');
										return;
									}
								}

								countOfSearch = Number(countOfSearch) + Number(1);
							}
						}

						//Submit Run Accrual Record Status = Completed
						var submitRecord = nlapiSubmitField('customrecord_stlms_runaccrual', runPeriodicAccrl, 'custrecord_stlms_rpla_status', completedStatus);

						//After all records are processed re-set the checkbox to false for next accrual
						if(countOfSearch == searchEmp.length) {

							var params = [];
							params['custscript_run_period_accrual_eave_recid'] = runPeriodicAccrl;
//							params['custscript_reset_employee_record'] = resetEmpRecord;
							params['custscript_matching_calendar_month'] = matchingCalndrMnth;
							params['custscript_run_accrual_start_date'] = fromDate;
							params['custscript_run_accrual_end_date'] = toDate;
//							params['custscript_run_accrual_emp_type'] = empType;
							params['custscript_reset_employee_record'] = 'T';
							params['custscript_run_accrual_emp_type'] = empType;
							params['custscript_hr_email_id'] = emailId;
							params['custscript_author'] = author;
							params['custscript_email_details'] = JSON.stringify(emailDetails);

							var status = nlapiScheduleScript('customscript_stlms_leave_accumulation', 'customdeploy_stlms_leave_accumulation', params);
							nlapiLogExecution('Debug', 'Scheduled Status', status);

						}
					}

					return;
				}
			}
		}
	}
}

function getSetup() {

	var filterSetup = new Array();
	filterSetup.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columnSetup = new Array();
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_employeetype'));
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_frequencyofaccrual'));
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_calendarstartdate'));
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_calendarenddate'));
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_periodicaccrualauto'));
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_leavecalendarstart'));
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_hremailid'));
	columnSetup.push(new nlobjSearchColumn('custrecord_stlms_su_hrapproverrole'));

	var searchSetup = nlapiSearchRecord('customrecord_stlms_setups', null, filterSetup, columnSetup);

	return searchSetup;

}

//Funtion to get employee to send email.
function getEmployee(role) {
	
	var Approver_emp;
	
	var filt1 = new Array();
	filt1.push(new nlobjSearchFilter('role',null,'is',role));
	filt1.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var search1 = nlapiSearchRecord('employee',null,filt1,null);

	if(search1)
	{
		Approver_emp=search1[0].getId();
		nlapiLogExecution('Debug', 'Approver_emp', Approver_emp);
	}

	return Approver_emp;
	
}

function getSender(emailId) {
	
	var filter = new Array();
	filter.push(new nlobjSearchFilter('email', null, 'is', emailId));
	
	var searchEmp = nlapiSearchRecord('employee', null, filter, null);
	var empId;
	
	if(searchEmp != '' && searchEmp != null)
		empId = searchEmp[0].getId();
	
	nlapiLogExecution('Debug', 'empId', empId);
	return empId;
	
}

function getCalculatedValue(formula,runPeriodicAccrl,search) {

	nlapiLogExecution('Debug', 'Formula', formula);
	var words = [];//new Array();
	var customRecord, customField, getValue;
	var firstValue;

	formula = formula.replace(/{(.+?)}/g, function($0, $1) {

		nlapiLogExecution('Debug', '$0', $0);
		nlapiLogExecution('Debug', '$1', $1);
		firstValue = $1.toString();
		firstValue = firstValue.split('.');
		nlapiLogExecution('Debug', 'First Value', firstValue);

		if(firstValue.length > 1) {
			nlapiLogExecution('Debug', 'Inside First Value length > 1 : ', 'customrecord : ' + firstValue[0] + ' customfield : '+firstValue[1]);

			customRecord = firstValue[0];
			customField = firstValue[1];

			getValue = nlapiLookupField(customRecord.toString(), runPeriodicAccrl , customField.toString());
			nlapiLogExecution('Debug', 'Inside First Value length > 1 : ', 'getValue : '+getValue);
		}
		else {
			nlapiLogExecution('Debug', 'Inside First Value length = 1 : ', 'customfield : '+firstValue[1]);

			customField = firstValue[0];
			getValue = search.getValue(customField.toString());
			nlapiLogExecution('Debug', 'Inside First Value length = 1 : ', 'getValue : '+getValue);
		}

		var temp1 = words.toString();

		words = getValue;//words.replace(temp1,getValue);
		var a = words;
		return words;

		nlapiLogExecution('Debug', 'Words Array Values', words);
	});

	nlapiLogExecution('Debug', 'Formula after replace function', formula);

	var result = math.eval(formula);
	nlapiLogExecution('Debug', 'Calculated Result', result);

	return result;
}

function createRunPeriodicAccrualRecord(empType, frequency, calendarStartDate, calendarEndDate) {

	var empType = empType;
	var frequency = frequency;
	var calendarStartDate = calendarStartDate;
	var calendarEndDate = calendarEndDate;

	try{

		if(empType != '' && empType != null) {

			//Search Leave Management Setup to get frequency and start date and month for the selected employee type.
			{
				/**Value of frequency to set the to date on the record.**/
				var monthToAdd = getMonthFreq(frequency);

				//Suitelet call with required parameters to search on Run Periodic Leave Accrual record to get the latest.
//				var URL = nlapiResolveURL('SUITELET', 'customscript_stlms_leaveapplicationretur', 'customdeploy_stlms_leaveapplicationretur', true);
//				var searchResult = nlapiRequestURL(URL,{action:'searchRunAccrual',empType:empType, method:'runaccrual'});

				//Search on Run Periodic Leave Accrual record to get the latest.
				var runAccrual = getLatestRunAccrual(empType);//JSON.parse(searchResult.getBody());
				runAccrual = JSON.parse(runAccrual);

				var submitRunAccrualRecord;
				var sendData = [];
				var createRunAccrualRecord = nlapiCreateRecord('customrecord_stlms_runaccrual');
				createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_employeetype', empType);

				if(runAccrual.getSearch == 'T')
				{

					var fromDate1 = runAccrual.fromDate;
					var toDate1 = runAccrual.toDate;
					var tempid = runAccrual.tempid;

					if(monthToAdd <= 12)
					{

						toDate1 = nlapiStringToDate(toDate1)
						var startDate1 = nlapiAddDays(toDate1,1);
						var getCurrentMonth = startDate1.getMonth() + Number(1);
						var getCurrentYear = startDate1.getFullYear();
						newFromDate = startDate1;
						newFromDate = nlapiDateToString(newFromDate);

						var newYearStartDate = toDate1;
						newYearStartDate = ((nlapiAddMonths(newYearStartDate,Number(monthToAdd))));
						var year = newYearStartDate.getFullYear();
						var month = newYearStartDate.getMonth()+Number(1);
						var day = getDay(month,year);
						newYearStartDate.setDate(day);
						var newToDate1 = nlapiDateToString(newYearStartDate);

						var monthInWords = getMonthInWords(getCurrentMonth);
						var name = monthInWords+' '+getCurrentYear;

						createRunAccrualRecord.setFieldValue('name', name);
						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_fromdate',newFromDate);
						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_todate',newToDate1);
						submitRunAccrualRecord = nlapiSubmitRecord(createRunAccrualRecord);

						sendData = {
							fromDate: newFromDate,
							toDate: newToDate1,
							runAccrual: submitRunAccrualRecord
						};
					}
					else
					{
						var days = 0;
						var month = ''
							var year = '';
						var finalToDate = '';
						toDate2 = toDate1;

						toDate1 = nlapiStringToDate(toDate1)
						var startDate1 = nlapiAddDays(toDate1,1);
						newFromDate = startDate1;
						newFromDate = nlapiDateToString(newFromDate);
						var newYearStartDate = toDate1;
						newYearStartDate = ((nlapiAddMonths(newYearStartDate,Number(monthToAdd))));
						var newToDate1 = nlapiDateToString(newYearStartDate);
						if(toDate2)
						{
							toDate2 = toDate2.split('/');
							toDate2 = new Date(toDate2[1]+'/'+toDate2[0]+'/'+toDate2[2]);
						}
						var startDate1 = nlapiDateToString(nlapiAddDays(toDate2,1));
						newFromDate = startDate1;

						var toDateToChnage = nlapiDateToString(toDate1);
						var finalToDate='';
						if(toDateToChnage && newFromDate)
						{

							var fromDateNew = newFromDate;
							fromDateNew = fromDateNew.split('/');

							toDateToChnage = toDateToChnage.split('/');
							days = toDateToChnage[0];
							if(days!=15)
							{

								finalToDate = '15'+'/'+fromDateNew[1]+'/'+fromDateNew[2];
							}
							else
							{
								newFromDate = nlapiAddDays(toDate1,1);
								var tempDate = newFromDate;
								newFromDate = nlapiDateToString(newFromDate);

								var year = tempDate.getFullYear();
								var month = tempDate.getMonth()+1;
								var day = getDay(month,year);

								toDate1 = tempDate;
								toDate1.setDate(day);
								finalToDate = nlapiDateToString(toDate1);

							}
						}

						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_fromdate',newFromDate);
						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_todate',finalToDate);
						submitRunAccrualRecord = nlapiSubmitRecord(createRunAccrualRecord);

						sendData = {
							fromDate: newFromDate,
							toDate: newToDate1,
							runAccrual: submitRunAccrualRecord
						};

					}
				}
				//If no records found.
				else
				{

					fromDate = calendarStartDate;
					newFromDate = fromDate;
					var toDate1 = newFromDate;
					var finalToDates = '';

					if(monthToAdd <= 11)
					{

						fromDate = nlapiStringToDate(fromDate);
						toDate1 = fromDate;
						var year = fromDate.getFullYear();
						var month = fromDate.getMonth()+1;
						var day = getDay(month,year);
						toDate1.setDate(day);

						toDate1 = nlapiAddMonths(toDate1, Number(monthToAdd - 1));
						toDate1 = nlapiDateToString(toDate1);

						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_fromdate',newFromDate);
						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_todate',toDate1);
						submitRunAccrualRecord = nlapiSubmitRecord(createRunAccrualRecord);

						sendData = {
							fromDate: newFromDate,
							toDate: newToDate1,
							runAccrual: submitRunAccrualRecord
						};

					}
					else if(monthToAdd == 12)
					{
						var fromDate = calendarStartDate;
						var finalToDate = calendarEndDate;

						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_fromdate',fromDate);
						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_todate',finalToDate);
						submitRunAccrualRecord = nlapiSubmitRecord(createRunAccrualRecord);

						sendData = {
							fromDate: newFromDate,
							toDate: newToDate1,
							runAccrual: submitRunAccrualRecord
						};

					}
					else
					{
						var days = 0;
						var toDateToChnage = toDate;
						console.log('Nihalxx  toDateToChnage  '+ toDateToChnage);
						if(toDateToChnage)
						{
							toDateToChnage = toDateToChnage.split('/');
							days = toDateToChnage[0];
						}

						if(days!=15)
						{
							console.log('Nihalxx  days not 154 ');
							var newFromDate1 = nlapiStringToDate(newFromDate);
							var tempDate = newFromDate1;
							var year = newFromDate1.getFullYear();
							var month = newFromDate1.getMonth()+1;
							console.log('Nihalxx  month '+month);
							var day = getDay(month);
							console.log('Nihalxx  toDate1 '+toDate1);
							var toDate1 = tempDate;

							console.log('Nihalxx  toDate1 '+toDate1);
							toDate1.setDate(monthToAdd+1);
							var finalToDate = nlapiDateToString(toDate1);
							console.log('Nihalxx  finalToDate '+finalToDate);
							newToDate = finalToDate;

						}

						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_fromdate',newFromDate);
						createRunAccrualRecord.setFieldValue('custrecord_stlms_rpla_todate',newToDate);
						submitRunAccrualRecord = nlapiSubmitRecord(createRunAccrualRecord);

						sendData = {
							fromDate: newFromDate,
							toDate: newToDate1,
							runAccrual: submitRunAccrualRecord
						};

					}
				}
			}
			sendData = JSON.stringify(sendData);
			return sendData;
		}
	}
	catch(error)
	{
		if (error.getDetails != undefined)
		{
			nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
			throw error;
		}
		else
		{
			nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function getMonthFreq(frequency)
{
	var results = 0;
	switch (frequency) {

	//Monthly
	case 1:
		results = 1;
		break;

		//Quarterly
	case 2:
		results = 3;
		break;

		//Yearly
	case 3:
		results = 12;
		break;

		//Fortnightly
	case 4:
		results = 14;
		break;

		//Half Yearly
	case 5:
		results = 6;
		break;

		//Monthly
	case '1':
		results = 1;
		break;

		//Quarterly
	case '2':
		results = 3;
		break;

		//Yearly
	case '3':
		results = 12;
		break;

		//Fortnightly
	case '4':
		results = 14;
		break;

		//Half Yearly
	case '5':
		results = 6;
		break;


	}

	return results;
}

function getDay(leaveCalndrStMonth,toYears)
{
	var toYear = parseInt(toYears);
	var results = 0;
	switch (leaveCalndrStMonth) {
	case 1:
		results = 31;
		break;
	case 2:
		if((toYears%4==0) || (toYears%100==0) || (toYears%400==0))
		{
			results = 29;
			break;
		}
		else
		{
			results = 28;
			break;
		}
	case 3:
		results = 31;
		break;
	case 4:
		results = 30;
		break;
	case 5:
		results = 31;
		break;
	case 6:
		results = 30;
		break;
	case 7:
		results = 31;
		break;
	case 8:
		results = 31;
		break;
	case 9:
		results = 30;
		break;
	case 10:
		results = 31;
		break;
	case 11:
		results = 30;
		break;	
	case 12:
		results = 31;
		break;	
	}

	return results;
}

//Function returns latest Run Periodic Leave Accrual record found or not
function getLatestRunAccrual(empType) {

	var empType = empType;
	var fromDate, toDate, tempid;

	//Search latest Run Periodic Accrual record.
	var filterRunAccrual = new Array();
	filterRunAccrual.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filterRunAccrual.push(new nlobjSearchFilter('custrecord_stlms_rpla_employeetype', null, 'is', empType));

	var columnRunAccrual = new Array();
	columnRunAccrual.push(new nlobjSearchColumn('custrecord_stlms_rpla_fromdate',null,'max'));
	columnRunAccrual.push(new nlobjSearchColumn('custrecord_stlms_rpla_todate',null,'max'));
	columnRunAccrual.push(new nlobjSearchColumn('internalid',null,'max'));

	var lastRunPeriodicLeaveAccurl = nlapiSearchRecord('customrecord_stlms_runaccrual', null, filterRunAccrual, columnRunAccrual);
	var getSearch = 'F';

	if(lastRunPeriodicLeaveAccurl && lastRunPeriodicLeaveAccurl[0].getValue('internalid',null,'max') != null && lastRunPeriodicLeaveAccurl[0].getValue('internalid',null,'max') != '')
	{

		getSearch = 'T';
		fromDate = lastRunPeriodicLeaveAccurl[0].getValue('custrecord_stlms_rpla_fromdate',null,'max');
		toDate = lastRunPeriodicLeaveAccurl[0].getValue('custrecord_stlms_rpla_todate',null,'max');
		tempid = lastRunPeriodicLeaveAccurl[0].getId();

	}

	var searchReturn = {getSearch:getSearch, fromDate:fromDate, toDate:toDate, tempid:tempid};
	searchReturn = JSON.stringify(searchReturn);
	nlapiLogExecution('Debug', 'searchReturn', searchReturn);

	return searchReturn;

}

function getMonthInWords(getCurrentMonth) {

	var results = '';
	switch (getCurrentMonth) {
	case 1:
		results = 'January';
		break;
	case 2:
		results = 'February';
		break;
	case 3:
		results = 'March';
		break;
	case 4:
		results = 'April';
		break;
	case 5:
		results = 'May';
		break;
	case 6:
		results = 'June';
		break;
	case 7:
		results = 'July';
		break;
	case 8:
		results = 'August';
		break;
	case 9:
		results = 'September';
		break;
	case 10:
		results = 'October';
		break;
	case 11:
		results = 'November';
		break;	
	case 12:
		results = 'December';
		break;	
	}

	return results;

}

/*
function getLeaveTypeSetup(leaveType, runPeriodicAccrl) {

	var filter = new Array();
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType));

	var column = new Array();
	column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
	column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leaveaccformula'));
	column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
	column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
	column.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));

	search = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, column);

	if(search) {

		for(s = 0; s < search.length; s++) {

			var leaveAccumulation = Number(0);
			var formula = search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula');
			nlapiLogExecution('Debug', 'leave calculation formula', formula);
			var recID = search[s].getId();

			leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
			nlapiLogExecution('Debug', 'Calculated Leave after function', leaveAccumulation);

			arraySetup.push({
				leaveType:search[s].getValue('custrecord_stlms_ltsetup_leavetype')
				,	formula:search[s].getValue('custrecord_stlms_ltsetup_leaveaccformula')
				,	carryForwardCheckbox:search[s].getValue('custrecord_stlms_ltsetup_carryforward')
				,	carryForwardNumber:search[s].getValue('custrecord_stlms_ltsetup_leavescarriedfo')
				,	accuralFactor:search[s].getValue('custrecord_stlms_ltsetup_accrualfactor')
				,	leaveAccumulation:leaveAccumulation
			});
		}
	}
	return arraySetup;
}



formula = formula.replace(/{(.+?)}/g, function($0, $1) {

//	words.push($1);
alert($0);
alert($1);
	var firstValue = $1.toString();
	firstValue = firstValue.split('.');

	var customRecord, customField, getValue;

	if(firstValue.length > 1) {

		customRecord = firstValue[0];
		customField = firstValue[1];

		getValue = nlapiLookupField(customRecord.toString(), runPeriodicAccrl , customField.toString());

	}
	else {

		customField = firstValue[0];
		getValue = nlapiGetFieldValue(customField.toString());

	}

	if(getValue == null || getValue == undefined || getValue == ''){
		getValue = 0;

	}
	else {
		getValue = getValue;
	}
	var temp1 = words.toString();

	words=getValue;//words.replace(temp1,getValue);
	var a = words;
alert(a);
return words;
});
alert('formula '+formula);
var result = math.eval(formula);
alert(result);

 */




//To be deleted
//var length = words.length;

//for(var w = 0; w < length; w++) {

//var firstValue = words[w];
//firstValue = firstValue.split('.');

//var customRecord, customField, getValue;

//if(firstValue.length > 1) {

//customRecord = firstValue[0];
//customField = firstValue[1];

//getValue = nlapiLookupField(customRecord.toString(), runPeriodicAccrl , customField.toString());

//}
//else {

//customField = firstValue[0];
//getValue = nlapiGetFieldValue(customField.toString());
//}

//if(getValue == null || getValue == undefined || getValue == ''){
//getValue = 0;
////firstValue = getValue;
//}
//else {
//getValue = getValue;
//}

//var temp = words[w].toString();
//words[w] = words[w].replace(temp1,getValue);
//var a = words;
////words[w].replace(/words[w].toString()/g,firstValue);

//}



//formula.replace(/{(.+?)}/g, function($0, $1) {

//words.push($1);

//var firstValue = $1;
//firstValue = firstValue.split('.');
//var customRecord, customField, getValue;

//if(firstValue.length > 1) {
//customRecord = firstValue[0];
//customField = firstValue[1];
//getValue = nlapiLookupField(customRecord.toString(), runPeriodicAccrl , customField.toString());

//}
//else {
//customField = firstValue[0];
//getValue = nlapiGetFieldValue(customField.toString());
//}

//words.replace($1,getValue);

//});
//nlapiLogExecutiom('Debug', 'Words array', words);


//var leaveTypeJobTitle = search[s].getValue('custrecord_stlms_ltsetup_jobtitle');
//var leaveTypeMinExprn = search[s].getValue('custrecord_stlms_ltsetup_expmin');
//var leaveTypeMaxExprn = search[s].getValue('custrecord_stlms_ltsetup_expmax');

//If JobTitle, Min and Max are not blank - Consider Job Title and service period of employee for accrual.
//if(leaveTypeJobTitle != '' && leaveTypeJobTitle != null && leaveTypeMinExprn != '' && leaveTypeMinExprn != null && leaveTypeMaxExprn != '' && leaveTypeMaxExprn != null)
//{
//nlapiLogExecution('Debug', 'If JobTitle, Min and Max are not blank', 'leaveTypeJobTitle = '+leaveTypeJobTitle+'@@'+'leaveTypeMinExprn = '+leaveTypeMinExprn+'@@'+'leaveTypeMaxExprn = '+leaveTypeMaxExprn);

//if(jobTitle == leaveTypeJobTitle && leaveTypeMinExprn < servicePeriod && leaveTypeMaxExprn > servicePeriod)
//{

//if(formula) {
//leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
//nlapiLogExecution('Debug', 'case1 Consider job title and service period both ## Calculated Leave after function', leaveAccumulation);
//}
//else {
//leaveAccumulation = Number(0);
//}

//}
//else {
//leaveAccumulation = Number(0);
//}

//}

////If Job Title blank and min and max not blank - Consider only service period of employee for accrual.
//if(leaveTypeJobTitle == '' || leaveTypeJobTitle == null && leaveTypeMinExprn != '' && leaveTypeMinExprn != null && leaveTypeMaxExprn != '' && leaveTypeMaxExprn != null)
//{

//if(leaveTypeMinExprn < servicePeriod && leaveTypeMaxExprn > servicePeriod)
//{

//if(formula) {
//leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
//nlapiLogExecution('Debug', 'Case 2 Conside only service period ## Calculated Leave after function', leaveAccumulation);
//}
//else {
//leaveAccumulation = Number(0);
//}

//}
//else {
//leaveAccumulation = Number(0);
//}

//}

////If Job Title is not blank and min and max is blank - Consider only Job Title of employee for accrual.
//if(leaveTypeJobTitle != '' && leaveTypeJobTitle != null && leaveTypeMinExprn == '' || leaveTypeMinExprn == null && leaveTypeMaxExprn == '' || leaveTypeMaxExprn == null)
//{

//if(jobTitle == leaveTypeJobTitle)
//{

//if(formula) {
//leaveAccumulation = getCalculatedValue(formula,runPeriodicAccrl,search[s]);
//nlapiLogExecution('Debug', 'Case 3 Conside only job title ## Calculated Leave after function', leaveAccumulation);
//}
//else {
//leaveAccumulation = Number(0);
//}

//}
//else {
//leaveAccumulation = Number(0);
//}

//}