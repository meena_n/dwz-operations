/*************************************************************************
 * Copyright (c) 1998-2012 Softype, Inc.                                 
 * Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 * All Rights Reserved.                                                    
 *                                                                        
 * This software is the confidential and proprietary information of          
 * Softype, Inc. ("Confidential Information"). You shall not               
 * disclose such Confidential Information and shall use it only in          
 * accordance with the terms of the license agreement you entered into    
 * with Softype.
 *
 * @author:  SHYAM SHARMA
 * @date:    14 Mar 2016
 * @version: Revised version
 * 
 * @recent update on = 22 Mar
 * Description:	This script creates final settlement record when employee resign.
 *************************************************************************/
var noofMonthNotWorked=0,leaveBalTobeElapsed=0,accuralFactor=0,leavesToBePaid=0;
var noofMonthsNotWorkedTxt = '',employeeName='';
var currContext = nlapiGetContext();
var noofMonthsNotWorked=0,noOfMonthsToBeLapsed=0,setupLeavType='',setupLeavTypeNew='',totalLeaveBalance=0;
var calendrStartDate = '';

function CalcLeaveBalance(type)
{
	try
	{
		if(type=='delete')
			return;

		var recId  =  nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		var empRecObj = nlapiLoadRecord(recType, recId);
		employeeName = empRecObj.getFieldValue('entityid');
		var empLeaveType = empRecObj.getFieldValues('custentity_stlms_leavetypes');
		var employeeType = empRecObj.getFieldValue('employeetype');
		var empLastDay = empRecObj.getFieldValue('custentity_stlms_lastdayofemployment');
		var empStatus = empRecObj.getFieldValue('employeestatus');

		/** Get final settlement leave types and leave calender start date from leave management setup record.**/
		var setupSearchs = getLeavMangtSetup(employeeType);
		if(setupSearchs)
		{	
			setupLeavType = setupSearchs[0].getValue('custrecord_stlms_su_leavesfinalsettlmnt');
			setupLeavType = setupLeavType.toString();
			setupLeavTypeNew = setupLeavType;
			calendrStartDate = setupSearchs[0].getValue('custrecord_stlms_su_calendarstartdate');
		}

		var isCalendrYearChange = '';
		var finalSettleToDate = '';
		var fromDate = empLastDay;
		var empLastDateYear = getYear(fromDate);

		var ToDate  = leaveBalMasterToDate(recId);
		var leaveBalMastrToDate = ToDate;
		var ToDate1 = ToDate;
		var leaveBalMastrToDateYear = getYear(ToDate1);

		/** Compare year from Last Date of Employment and leave balance master ToDate. 
		If year has changed then set Final Settlement ToDate as last day of previous year other wise set leave balance master ToDate**/
		if(Number(empLastDateYear) != Number(leaveBalMastrToDateYear))
		{
			var newYearStartDate = calendrStartDate;
			finalSettleToDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));
			isCalendrYearChange = 'T';
		}
		else
		{
			finalSettleToDate = leaveBalMastrToDate;
			isCalendrYearChange = 'F';
		}

		//nlapiLogExecution('DEBUG', '74 isCalendrYearChange = ',isCalendrYearChange+'@@'+'finalSettleToDate = '+finalSettleToDate);
		/** Get all leave types from leave balance line master. Compare these leave types with employee leave type 
		and Calculate total leave balance to set in final settlement leave balance field.**/
		var searchResults = leaveBalLeaveType(recId);
		if(searchResults)
		{
			/*
			var totalLeave1 = 0;
			var LeaveBalTemp = 0;

			if(setupLeavType)
			{
				setupLeavType = setupLeavType.split(',');
				for(var elt=0;elt<setupLeavType.length;elt++)
				{
					var setupLeavetype = setupLeavType[elt];
					for(var iii=0;iii<searchResults.length;iii++)
					{
						var leaveBal1 = searchResults[iii].getValue('custrecord_stlms_lbline_leavebalance');
						var leaveBal2 = searchResults[iii].getValue('custrecord_stlms_lbline_carriedforward');
						var leaveTypes = searchResults[iii].getValue('custrecord_stlms_lbline_leavetype');
						var leaveBalPrevYear = searchResults[iii].getValue('custrecord_stmm_lbline_leavebalprevious');
						var leabBalrecId = searchResults[iii].getId();
						nlapiLogExecution('Debug', 'setupLeavetype = ', setupLeavetype+'##'+'leaveTypes = '+leaveTypes+'##'+'Leave Bal = '+leabBalrecId);
						if(setupLeavetype==leaveTypes)
						{
						//*** If calender year has changed then calculate total leave balance from leave balance line master 
							previous year leave balance field. Else total leave balance from leave balance and carry forward field.*
							if(isCalendrYearChange=='T')
							{
								LeaveBalTemp = Number(LeaveBalTemp) + Number(leaveBalPrevYear);
								nlapiLogExecution('Debug', 'if true total leave balance', LeaveBalTemp);
							}	
							else
							{
								totalLeave1 = Number(leaveBal1) + Number(leaveBal2);
								nlapiLogExecution('Debug', 'total leave balance', totalLeave1);
								if(Number(totalLeave1)!=0)
									LeaveBalTemp = Number(LeaveBalTemp) + Number(totalLeave1);

							}
						}	
					}
				}	
			}
			 */			
			//totalLeaveBalance = LeaveBalTemp;
			//nlapiLogExecution('Debug', '118 totalLeaveBalance = ', totalLeaveBalance);

		}

		//Employee status 2 is resigned
		if(empLastDay && empStatus == 2)
		{
			/** This part is for Resigned Employee. Create final settlement record. **/

			var sFilt = [];
			var sColm = [];

			sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_fslc_employeename', null, 'is', recId));

			var searchResultsFinallSettle = nlapiSearchRecord('customrecord_stlms_finalleavecalculation', null, sFilt, sColm);
			if(!searchResultsFinallSettle)
			{
				var finalSettleLeaveCalc = nlapiCreateRecord('customrecord_stlms_finalleavecalculation');
				finalSettleLeaveCalc.setFieldValue('custrecord_stlms_fslc_lastdateofemp',empLastDay);
				finalSettleLeaveCalc.setFieldValue('custrecord_stlms_fslc_employeename',recId);
				//finalSettleLeaveCalc.setFieldValue('custrecord_stlms_fslc_leavebalance',totalLeaveBalance);
				//finalSettleLeaveCalc.setFieldValue('custrecord_stlms_fslc_noofmonths',parseFloat(noofMonthsNotWorkedTxt).toFixed(2));

				/**Compare leave types from final settlement leave type field in setup with employee leave type, to set leave type field 
				in final settlement record.**/
				if(setupLeavTypeNew)
				{
					var leaveTypeArr = [];
					setupLeavTypeNew = setupLeavTypeNew.split(',');
					for(var slt=0;slt<setupLeavTypeNew.length;slt++)
					{
						var leaveType=setupLeavTypeNew[slt];
						for(var elt=0;elt<empLeaveType.length;elt++)
						{
							if(leaveType==empLeaveType[elt])
							{
								//finalSettleLeaveCalc.setFieldValue('custrecord_stlms_fslc_leavetype',leaveType);
								//break;
								leaveTypeArr.push(leaveType);
							}	
						}	
					}
					finalSettleLeaveCalc.setFieldValues('custrecord_stlms_fslc_leavetype',leaveTypeArr);
					/** Get all leave types from leave balance line master. Compare these leave types with employee leave type 
					and Calculate total leave balance to set in final settlement leave balance field.**/
					if(leaveTypeArr.length>0)
					{	
						var totalLeave1 = 0;
						var LeaveBalTemp = 0;
						for(var tt=0;tt<leaveTypeArr.length;tt++)
						{	
							var setupLeavetype = leaveTypeArr[tt];
							for(var iii=0;iii<searchResults.length;iii++)
							{
								var leaveBal1 = searchResults[iii].getValue('custrecord_stlms_lbline_leavebalance');
								var leaveBal2 = searchResults[iii].getValue('custrecord_stlms_lbline_carriedforward');
								var leaveTypes = searchResults[iii].getValue('custrecord_stlms_lbline_leavetype');
								var leaveBalPrevYear = searchResults[iii].getValue('custrecord_stmm_lbline_leavebalprevious');

								if(setupLeavetype==leaveTypes)
								{
									/** If calender year has changed then calculate total leave balance from leave balance line master 
									previous year leave balance field. Else total leave balance from leave balance and carry forward field.**/
									if(isCalendrYearChange=='T')
									{
										LeaveBalTemp = Number(LeaveBalTemp) + Number(leaveBalPrevYear);
										//nlapiLogExecution('Debug', 'if true total leave balance', LeaveBalTemp);
									}	
									else
									{
										totalLeave1 = Number(leaveBal1) + Number(leaveBal2);
										//nlapiLogExecution('Debug', 'total leave balance', totalLeave1);
										if(Number(totalLeave1)!=0)
											LeaveBalTemp = Number(LeaveBalTemp) + Number(totalLeave1);

									}
								}	
							}
						}
						totalLeaveBalance = LeaveBalTemp;
						nlapiLogExecution('Debug', '118 totalLeaveBalance = ', totalLeaveBalance);
					}
				}	

				finalSettleLeaveCalc.setFieldValue('custrecord_stlms_fslc_leavebalance',totalLeaveBalance);
				var leavBalToDate = leaveBalMasterToDate(recId);
				finalSettleLeaveCalc.setFieldValue('custrecord_stlms_fslc_leavebalancetodate',leavBalToDate);
				finalSettleLeaveCalc.setFieldValue('custrecord_stmm_fslc_fstodate',finalSettleToDate);

				var finalSettleLeaveCalcSubmitId = nlapiSubmitRecord(finalSettleLeaveCalc,true,true);
				if(finalSettleLeaveCalcSubmitId)
				{
					nlapiLogExecution('DEBUG', '70 finalSettleLeaveCalcSubmitId = ',finalSettleLeaveCalcSubmitId);
					nlapiSetRedirectURL('RECORD', 'customrecord_stlms_finalleavecalculation', finalSettleLeaveCalcSubmitId, false);
				}	

			}
			else
			{
				var recId  = searchResultsFinallSettle[0].getId();
				nlapiSubmitField('customrecord_stlms_finalleavecalculation',recId,'custrecord_stlms_fslc_leavebalance',totalLeaveBalance);
			}	
		}
		else
		{
			if(type=='create') {

				/** This part is to update leave accrual start date field in employee record.**/
				getLeaveBalCalcNewJoinee(empRecObj,recId,employeeName);
				nlapiLogExecution('DEBUG', '70 After updaing Balance Leave Master = ','After updaing Balance Leave Master');

			}
		}	
	}
	catch(error)
	{
		if (error.getDetails != undefined)
		{
			nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
			throw error;
		}
		else
		{
			nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function getYear(d1)
{
	var results = 0;
	if(d1)
	{
		d1 = d1.split('/');
		d1 = new Date(d1[2],d1[1]-1,d1[0]);

		results = d1.getFullYear();
	}

	return results;
}

function leaveBalLeaveType(recId)
{
	var sFilt = [];
	var sColm = [];

	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', recId));

	sColm.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavetype'));
	sColm.push(new nlobjSearchColumn('custrecord_stmm_lbline_leavebalprevious'));

	var searchResults = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, sFilt, sColm);

	return searchResults;
}


function leaveBalMasterToDate(recId)
{
	var sFilt = [];
	var sColm = [];
	var results = '';


	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', null, 'is', recId));

	sColm.push(new nlobjSearchColumn('custrecord_stlms_lbm_todate'));

	var leaveBalmasters = nlapiSearchRecord('customrecord_stlms_leavebalancemaster', null, sFilt, sColm); 
	if(leaveBalmasters)
	{
		for(var i=0;i<leaveBalmasters.length;i++)
		{
			results = leaveBalmasters[i].getValue('custrecord_stlms_lbm_todate');
		}
	}

	return results;
}

function getLeaveBalCalcNewJoinee(empRecObj,recId,employeeName)
{
	var leaveDate = '';
//	var sFilt = [];
//	var sColm = [];
//	var fields = ['custrecord_stlms_lbm_fromdate','custrecord_stlms_lbm_todate'];

	var leaveAccrualStDate = empRecObj.getFieldValue('custentity_stlms_leaveaccrualstartdate');
	if(leaveAccrualStDate)
		leaveDate = leaveAccrualStDate;
	else
		leaveDate = empRecObj.getFieldValue('hiredate');

	var leaveBalFromDate = leaveDate;
	var leaveBalAsOnDate = nlapiDateToString(new Date());
	var toDate = getRunPeriodicLeaveAccrDate();
//	var values = [fromDate,toDate];

	var isEligibleLeaveAccrual = empRecObj.getFieldValue('custentity_stlms_eligibilityforleaveacc');
	var isResumeTimeLeaveAccrual = empRecObj.getFieldValue('custentity_stlms_resumeleaveaccrual');

	//New Joinee create leave balance master and line.
//	if(isEligibleLeaveAccrual=='T' && isResumeTimeLeaveAccrual=='F')//(isEligibleLeaveAccrual=='T' && isResumeTimeLeaveAccrual=='T')
	{

		var leaveBalObj = nlapiCreateRecord('customrecord_stlms_leavebalancemaster');
		leaveBalObj.setFieldValue('custrecord_stlms_lbm_employeename',recId);
		leaveBalObj.setFieldValue('custrecord_stlms_lbm_fromdate',leaveBalFromDate);
		leaveBalObj.setFieldValue('custrecord_stlms_lbm_todate',toDate);
		leaveBalObj.setFieldValue('custrecord_stlms_lbm_asondate',leaveBalAsOnDate);

		var leaveBalSubmitId = nlapiSubmitRecord(leaveBalObj,true,true);
		nlapiLogExecution('DEBUG', '139 leaveBalSubmitId = ',leaveBalSubmitId);

		if(leaveBalSubmitId) {

			var empLeaveType = empRecObj.getFieldValues('custentity_stlms_leavetypes');
			var employeeType = empRecObj.getFieldValue('employeetype');
			var leavTypeArr = [];
			if(empLeaveType)
			{
				for(var pp=0;pp<empLeaveType.length;pp++)
				{
					leavTypeArr.push(empLeaveType[pp]);
				}


				for(var lt = 0; lt < leavTypeArr.length; lt++) {

					var leaveType = leavTypeArr[lt];

//					var filter = new Array();
//					filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//					filter.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType));
//					filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', employeeType));

//					var searchLeaveTypeSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, filter, null);

//					if(searchLeaveTypeSetup) {

					var createRecord = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');
					createRecord.setFieldValue('custrecord_stlms_lbline_leavetype', leaveType);
					createRecord.setFieldValue('custrecord_stlms_lbline_leavebalance', Number(0));
					createRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', Number(0));
					createRecord.setFieldValue('custrecord_stlms_lbline_leaveutilized', Number(0));
					createRecord.setFieldValue('custrecord_stmm_lbline_leavebalprevious', Number(0));
					createRecord.setFieldValue('custrecord_stlms_lbline_parent', leaveBalSubmitId);

					var submitRecord = nlapiSubmitRecord(createRecord);
					nlapiLogExecution('Debug', 'Line Submitted', submitRecord);

//					}
				}
			}
//			nlapiSetRedirectURL('RECORD', 'customrecord_stlms_leavebalancemaster', leaveBalSubmitId, false);
		}
	}
}

function getRunPeriodicLeaveAccrDate()
{
	var today = nlapiDateToString(new Date());
	today = Date.parse(today);

	var sFilt = [];
	var sColm = [];
	var endsDate = '';

	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_rpla_status', null, 'is', 2));

	sColm.push(new nlobjSearchColumn('custrecord_stlms_rpla_fromdate',null,'max'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_rpla_todate',null,'max'));

	var runRediodicResults = nlapiSearchRecord('customrecord_stlms_runaccrual', null, sFilt, sColm); 
	if(runRediodicResults)
	{
		for(var ii=0;ii<runRediodicResults.length;ii++)
		{
			var stDate = runRediodicResults[ii].getValue('custrecord_stlms_rpla_fromdate',null,'max');
//			stDate = Date.parse(stDate);
			var enDate = runRediodicResults[ii].getValue('custrecord_stlms_rpla_todate',null,'max');
//			endsDate = enDate;
//			enDate = Date.parse(enDate);

			//nlapiLogExecution('DEBUG', '200 endsDate = ',endsDate);

//			if(stDate<today && today<enDate)
			return enDate;
		}
	}

	return enDate;
}

function getTotalLeaveBalance(leaveType, recId)
{
	var sFilt = [];
	var sColm = [];
	var totalLeave = 0;

	if(leaveType)
	{
		sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		sFilt.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'anyof', leaveType));
		sFilt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', recId));

		sColm.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
		sColm.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));

		var searchResults = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, sFilt, sColm);
		if(searchResults)
		{
			for(var i=0;i<searchResults.length;i++)
			{
				var leaveBal1 = searchResults[i].getValue('custrecord_stlms_lbline_leavebalance');
				var leaveBal2 = searchResults[i].getValue('custrecord_stlms_lbline_carriedforward');

				var totalLeaveTemp = Number(leaveBal1) + Number(leaveBal2);
				totalLeave = Number(totalLeave) + Number(totalLeaveTemp);
			}	
		}	
	}	

	return totalLeave;
}


function getLeavMangtSetup(employeeType)
{
	nlapiLogExecution('DEBUG', '403 employeeType = ',employeeType);

	var sFilt = [];
	var sColm = [];


	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', null, 'is', employeeType));

//	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_slcnoofmonthsformula'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_slcleavetolapseformu'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_leavesfinalsettlmnt'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_calendarstartdate'));

	var srchSetup = nlapiSearchRecord('customrecord_stlms_setups', null, sFilt, sColm);


	/*
	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', 'custrecord_stlms_ltsetup_parent', 'is', employeeType));

	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_slcnoofmonthsformula','custrecord_stlms_ltsetup_parent'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_calendarstartdate','custrecord_stlms_ltsetup_parent'));

	var srchSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilt, sColm);
	 */

	return srchSetup;

}

/*
function evalFormula(formulaText,employeeId,leaveType)
{
	var formula = formulaText;
	var methodPart = formulaText;
	var words = [];
	var dateArr = [];
	var results = 0;
	var errorCatcherReturn = -1;

	formula.replace(/{(.+?)}/g, function($0, $1) { words.push($1) });

	for(var w = 0; w <words.length; w++)
	{
		var partToreplace = words[w];
		var firstValue = words[w].split('.');
		var customRecord, customField, getValue;

		if(firstValue.length > 1) 
		{
			customRecord = firstValue[0].trim();
			customField = firstValue[1].trim();

			getValue = getFieldVal(customRecord,customField,employeeId,leaveType);
		}
		else
		{
			customField = firstValue[0];

			getValue = nlapiGetFieldValue(customField);
		}

		if(getValue=='' || getValue=='null' || getValue==null)
			getValue=0;

		nlapiLogExecution('DEBUG', '247 getValue = ',getValue);
		if(getValue==-1 || getValue==0)
		{
			var scriptId = currContext.getScriptId();
			var errorMsg = 'No search results for employee '+employeeName;

			var errorCatcherObj = nlapiCreateRecord('customrecord_stlms_errorcatcher');
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_employeename',employeeId);
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

			var errorCatcherSubmitId = nlapiSubmitRecord(errorCatcherObj,true,true);
			nlapiLogExecution('DEBUG', '258 errorCatcherSubmitId = ',errorCatcherSubmitId);
			return errorCatcherReturn;
		}


		if(formula.indexOf(partToreplace))
			formula = formula.replace('{'+partToreplace+'}',getValue);


		if(formula.indexOf('Months_between') && formula.indexOf('/')>=0)//identification for date
			dateArr.push(getValue);

	}

	if(formula.indexOf('Months_between') && formula.indexOf('/')>=0)//identification for date 
	{
		formula = formula.replace('Round(Months_between','round(');
		results = monthDiff(dateArr[0],dateArr[1]);
	}	
	else
	{
		results = math.eval(formula);
		nlapiLogExecution('DEBUG', '134 else date results = ',results);
	}	

	return results;
}

function getFieldVal(recordType,recordFeild,employeeId,leaveType)
{
	var sFilt = [];
	var sColm = [];
	var dates = '';

	/*
	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sColm.push(new nlobjSearchColumn(recordFeild));

	var searchResults = nlapiSearchRecord(recordType, null, sFilt, sColm);
	if(searchResults)
		dates = searchResults[0].getValue(recordFeild);

	return dates;
 */

/*
	var searchRecType = '';
	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var joinId = getJoinOperator(recordType);
	if(joinId)
	{
		sFilt.push(new nlobjSearchFilter('internalid', null, 'is', employeeId));

		sColm.push(new nlobjSearchColumn('internalid',null,'group'));
		sColm.push(new nlobjSearchColumn(recordFeild,joinId,'max'));
		searchRecType = 'employee';
	}	
	else
	{
		sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType));

		sColm.push(new nlobjSearchColumn(recordFeild));
		searchRecType = recordType;

	}	

	var searchResults = nlapiSearchRecord(searchRecType, null, sFilt, sColm);
	if(searchResults)
	{
		if(joinId)
			dates = searchResults[0].getValue(recordFeild,joinId,'max');
		else
			dates = searchResults[0].getValue(recordFeild);

	}
	else
	{
		nlapiLogExecution('DEBUG', '320 No search Results = ','No search Results');
		dates = -1;
	}	

	return dates;
}


function monthDiff(d1, d2) 
{
	var months = '';
	if(d1)
	{
		d1 = d1.split('/');
		d1 = new Date(d1[2],d1[1]-1,d1[0]);
	}
	if(d2)
	{
		d2 = d2.split('/');
		d2 = new Date(d2[2],d2[1]-1,d2[0]);
	}	

	var d1FullYear = d1.getFullYear();
	var d2FullYear = d2.getFullYear();
	var d1Months = d1.getMonth();
	var d2Months = d2.getMonth();


	months =  (d2FullYear-d1FullYear)*12;
	months += Math.abs(d1Months-d2Months);

    return months <= 0 ? 0 : months;
}
 */

function evalFormula(formulaText,employeeId,leaveType)
{
	var formula = formulaText;
	var methodPart = formulaText;
	var words = [];
	var dateArr = [];
	var results = 0;

	formula.replace(/{(.+?)}/g, function($0, $1) { words.push($1) });

	for(var w = 0; w <words.length; w++)
	{
		var partToreplace = words[w];
		var firstValue = words[w].split('.');
		var customRecord, customField, getValue;

		if(firstValue.length > 1) 
		{
			customRecord = firstValue[0].trim();
			customField = firstValue[1].trim();

			getValue = getFieldVal(customRecord,customField,employeeId,leaveType);
		}
		else
		{
			customField = firstValue[0];

			getValue = nlapiGetFieldValue(customField);
		}

		if(getValue=='' || getValue=='null' || getValue==null)
			getValue=0;


		if(formula.indexOf(partToreplace)>=0)
			formula = formula.replace('{'+partToreplace+'}',getValue);


		if(formula.indexOf('Months_between') && formula.indexOf('/')>=0)//identification for date
			dateArr.push(getValue);

	}

	if(formula.indexOf('Months_between') && formula.indexOf('/')>=0)//identification for date 
	{
		formula = formula.replace('Round(Months_between','round(');
		results = monthDiff(dateArr[0],dateArr[1]);
	}	
	else
	{
		//nlapiLogExecution('DEBUG', '144 formula = ',formula);
		results = math.eval(formula);
		//nlapiLogExecution('DEBUG', '146 else date results = ',results);
	}	

	return results;
}

function getFieldVal(recordType,recordFeild,employeeId,leaveType)
{
	var sFilt = [];
	var sColm = [];
	var dates = '';

	var searchRecType = '';
	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var joinId = getJoinOperator(recordType);
	if(joinId)
	{
		sFilt.push(new nlobjSearchFilter('internalid', null, 'is', employeeId));

		sColm.push(new nlobjSearchColumn('internalid',null,'group'));
		sColm.push(new nlobjSearchColumn(recordFeild,joinId,'max'));
		searchRecType = 'employee';
	}	
	else
	{
		sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', leaveType));

		sColm.push(new nlobjSearchColumn(recordFeild));
		searchRecType = recordType;
	}	

	var searchResults = nlapiSearchRecord(searchRecType, null, sFilt, sColm);
	if(searchResults)
	{
		if(joinId)
			dates = searchResults[0].getValue(recordFeild,joinId,'max');
		else
			dates = searchResults[0].getValue(recordFeild);

		//nlapiLogExecution('DEBUG', '184 dates = ',dates);
	}
	else
		nlapiLogExecution('DEBUG', '189 no search resulst type = ',searchRecType);

	//nlapiLogExecution('DEBUG', '191 dates = ',dates);
	return dates;

}

function monthDiff(d1, d2) 
{
	var months = '';
	if(d1)
	{
		d1 = d1.split('/');
		d1 = new Date(d1[2],d1[1]-1,d1[0]);
	}
	if(d2)
	{
		d2 = d2.split('/');
		d2 = new Date(d2[2],d2[1]-1,d2[0]);
	}	


	var one_day = 1000*60*60*24;
	var date1 = d1;//new Date('4/19/2016');//mm/dd/yyyy
	var date2 = d2;//new Date('2/28/2017');//mm/dd/yyyy

	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();

	var difference_ms = Math.abs(date2_ms - date1_ms);

	var results = Math.round(difference_ms/one_day);
	//var months = Math.round(results / 30);
	//nlapiLogExecution('DEBUG', '236 results = ',results);
	var months = ((results+1) / 30);
	//nlapiLogExecution('DEBUG', '236 months = ',months);

	return months;
}


