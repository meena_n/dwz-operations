/* ************************************************************************************  
 ** Copyright (coffee) 1998-2012 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of          
 ** Softype, Inc.("Confidential Information"). You shall not disclose              
 ** such Confidential Information and shall use it only in accordance         
 ** with the terms of the license agreement you entered into with Softype.
 ** @version: Revised version               
 **                       
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Apr 2016     Nihal Mulani
 *
 Description : This Script set the approver in the field.
 ************************************************************************************ */
var Context = nlapiGetContext();

function beforeSubmit_setFieldValues(type) {

	var currentContext = Context.getExecutionContext();   
	nlapiLogExecution('DEBUG','24 currentContext = ',currentContext+'@@'+'type = '+type);

	if(type == 'create' && currentContext == 'csvimport') {

		fieldChange_calculateLeaves();
		saveRecord_confirmValidate();

	}

}

function userEventAfterSubmit(type)
{
	var currentContext = Context.getExecutionContext();   
	nlapiLogExecution('DEBUG','24 currentContext = ',currentContext+'@@'+'type = '+type);

	if(type=='delete')
	{
		return;
	}

	if(type == 'create' && (currentContext == 'userinterface' || currentContext == 'csvimport'))
	{
		var sec_approvername = nlapiGetFieldValue('custrecord_stlms_la_approvername');
		var la_hrapproverrole = nlapiGetFieldValue('custrecord_stlms_la_hrapproverrole');


		var employee= nlapiGetFieldValue('custrecord_stlms_la_employeename');
		var  employeetype = nlapiLookupField('employee', employee , 'employeetype');

		var filt = new Array();
		filt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype',null,'is',employeetype));
		filt.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		var cols= new Array();
		cols[0] = new nlobjSearchColumn('custrecord_stlms_su_hrapproverrole');
		cols[1] = new nlobjSearchColumn('custrecord_stlms_su_approverrole');
		cols[2] = new nlobjSearchColumn('custrecord_stlms_su_hremailid');
		var search = nlapiSearchRecord('customrecord_stlms_setups',null,filt,cols);
		var hrapproverrole,approverrole,hremailid='';
		if(search)
		{
			hrapproverrole= search[0].getValue('custrecord_stlms_su_hrapproverrole');
			approverrole= search[0].getValue('custrecord_stlms_su_approverrole');
			hremailid= search[0].getValue('custrecord_stlms_su_hremailid');
		}
		nlapiLogExecution('debug', 'hrapproverrole', hrapproverrole);
		nlapiLogExecution('debug', 'hremailid', hremailid);
		nlapiLogExecution('debug', 'approverrole', approverrole);
		var Approver_emp='';

		if(approverrole != '' && approverrole != null) {

			var filt1 = new Array();
			filt1.push(new nlobjSearchFilter('role',null,'is',approverrole));
			filt1.push(new nlobjSearchFilter('isinactive',null,'is','F'));
			var search1 = nlapiSearchRecord('employee',null,filt1,null);

			if(search1)
			{
				Approver_emp=search1[0].getId();
//				 Approver_emp = 99;
				nlapiLogExecution('debug', 'Approver_emp', Approver_emp);
			}
		}
		else {

			Approver_emp = '';
			nlapiLogExecution('debug', 'Approver_emp', Approver_emp);
		}


		if(sec_approvername == '' || sec_approvername == null)
		{
			nlapiSubmitField('customrecord_stlms_application', nlapiGetRecordId(), ['custrecord_stlms_la_approvername','custrecord_stlms_la_hremailid'], [Approver_emp,hremailid]);
		}
		else
		{
			nlapiSubmitField('customrecord_stlms_application', nlapiGetRecordId(), ['custrecord_stlms_la_hremailid'], [hremailid]);
		}

		if(la_hrapproverrole == '' || la_hrapproverrole == null)
		{
			nlapiSubmitField('customrecord_stlms_application', nlapiGetRecordId(), ['custrecord_stlms_la_hrapproverrole','custrecord_stlms_la_hremailid'], [hrapproverrole,hremailid]);
		}
		else
		{
			nlapiSubmitField('customrecord_stlms_application', nlapiGetRecordId(), ['custrecord_stlms_la_hremailid'], [hremailid]);
		}
	}
}

var emp;
var leaveType;
var startMonth, endMonth, todayMonth, startYear, endYear, todayYear;
var startDate, endDate;
var getDay;
var totalLeaves = 0;
var leavePeriod, firstHalf, secondHalf;
var completeStartDate, completeEndDate;

function fieldChange_calculateLeaves() {

//	if (name != 'custrecord_stlms_la_leavetype' && name != 'custrecord_stlms_la_startdate' && name != 'custrecord_stlms_la_enddate' && name != 'custrecord_stlms_la_deductfrom' && name != 'custrecord_stlms_la_employeename')
//	{
//		//DO NOTHING
//		return true;
//	}

	//Field change will trigger only on change of leave type, start date and end date
//	if(name == 'custrecord_stlms_la_leavetype' || name == 'custrecord_stlms_la_startdate' || name == 'custrecord_stlms_la_enddate' || name == 'custrecord_stlms_la_deductfrom' || name == 'custrecord_stlms_la_employeename') {

		emp = nlapiGetFieldValue('custrecord_stlms_la_employeename');
		leaveType = nlapiGetFieldValue('custrecord_stlms_la_leavetype');
		var leaveTypeConsider = nlapiGetFieldValue('custrecord_stlms_la_deductfrom');

//		if(name == 'custrecord_stlms_la_employeename' && leaveType != '' && leaveTypeConsider != '') {
//
//			nlapiSetFieldValue('custrecord_stlms_la_leavetype', '');
//			nlapiSetFieldValue('custrecord_stlms_la_deductfrom', '');
//			nlapiSetFieldValue('custrecord_stlms_la_leavebalance', '');
//			nlapiSetFieldValue('custrecord_stlms_la_carriedforwardbalanc', '');
//
//		}

		if(leaveType != '' && leaveType != null && leaveTypeConsider != '' && leaveTypeConsider != null)
		{

			var URL = nlapiResolveURL('SUITELET', 'customscript_stlms_leaveapplicationretur', 'customdeploy_stlms_leaveapplicationretur', true);
			var searchResult = nlapiRequestURL(URL,{action:'checkBalanceLine',emp:emp, leaveType:leaveType, leaveTypeConsider:leaveTypeConsider});

			//Calculated leave period.
			var a = JSON.parse(searchResult.getBody());
			
			
//			//Search Leave Balance Line to get Leave Balance and Carry Forward Leave Balance for the emp and selected leave type
//			var filter = new Array();
//			filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//			filter.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', emp));
//
//			if(leaveType == leaveTypeConsider) {
//				filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveType));
//			}
//			else if(leaveType != leaveTypeConsider) {
//				filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveTypeConsider));
//			}
//
//			var column = new Array();
//			column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
//			column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
//
//			var search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filter, column);

			//If search set the received values
			if(a.getSearch == 'T') {

//				var leaveBalance = search[0].getValue('custrecord_stlms_lbline_leavebalance');
//				var carryForward = search[0].getValue('custrecord_stlms_lbline_carriedforward');
				totalLeaves = Number(a.totalLeaves);

				nlapiSetFieldValue('custrecord_stlms_la_leavebalance', a.leaveBalance);
				nlapiSetFieldValue('custrecord_stlms_la_carriedforwardbalanc', a.carryForward);
			}

			//If no results found set leave balance to zero.
			else if(a.getSearch == 'F') {

				if(leaveType == leaveTypeConsider) {
					nlapiSetFieldValue('custrecord_stlms_la_leavebalance', 0);
					nlapiSetFieldValue('custrecord_stlms_la_carriedforwardbalanc', 0);
				}
				else if(leaveType != leaveTypeConsider) {
//					alert('There is no balance for the selected Leave to be deducted from field. Kindly update the field.');
//					nlapiSetFieldValue('custrecord_stlms_la_deductfrom', '');
					throw nlapiCreateError('There is no balance for the selected Leave to be deducted from field.', 'There is no balance for the selected Leave to be deducted from field.', true);

				}
			}

			//Based on the leave selected search carry forward check-box and show and hide field accordingly.
//			var getLeaveTypeSetupValue = getLeaveTypeSetup(leaveType);
//			if(getLeaveTypeSetupValue) {
//			var carryForwardCheckbox = getLeaveTypeSetupValue[0].getValue('custrecord_stlms_ltsetup_carryforward');

//			if(carryForwardCheckbox == 'T') {

//			var getField = nlapiGetField('custrecord_stlms_la_carriedforwardbalanc');
//			getField.setDisplayType('hidden');
//			}
//			}

			startDate = nlapiGetFieldValue('custrecord_stlms_la_startdate');
			endDate = nlapiGetFieldValue('custrecord_stlms_la_enddate');

			if(startDate != '' && startDate != null || endDate != '' && endDate != null) {

				completeStartDate = startDate;
				startDate = nlapiStringToDate(startDate);
//				startYear = startDate.getFullYear(startDate);
//				getDay = startDate.getDay();
//				startMonth = startDate.getMonth() + Number(1);
//				startDate = startDate.getDate(startDate);
//				alert('Start Month '+startDate);
				var todaysDate = new Date();
				todayYear = todaysDate.getFullYear(todaysDate);
				todayMonth = todaysDate.getMonth() + Number(1);
				todaysDate = todaysDate.getDate(todaysDate);

				if(endDate != '' && endDate != null){

					completeEndDate = endDate;
					endDate = nlapiStringToDate(endDate);
//					endYear = endDate.getFullYear(endDate);
//					endMonth = endDate.getMonth() + Number(1);
//					endDate = endDate.getDate(endDate);
//					alert('End Month '+endDate);// && endMonth <= startMonth && endYear <= startYear
					if(endDate < startDate) {

						throw nlapiCreateError('End date should be on or after the start date.', 'End date should be on or after the start date.', true);
//						alert('End date should be on or after the start date.');
//						nlapiSetFieldValue('custrecord_stlms_la_enddate', '');
//						return true;
					}
				}
			}
		}
//	}
}

var arrayValues = {};
var escalationDay;
var escalationDate;
function saveRecord_confirmValidate() {

//	if(startDate == '' && startDate == null && endDate == '' && endDate == null) {

	startDate = nlapiGetFieldValue('custrecord_stlms_la_startdate');
	endDate = nlapiGetFieldValue('custrecord_stlms_la_enddate');

	emp = nlapiGetFieldValue('custrecord_stlms_la_employeename');
	leaveType = nlapiGetFieldValue('custrecord_stlms_la_deductfrom');

	completeStartDate = startDate;
	startDate = nlapiStringToDate(startDate);
	startYear = startDate.getFullYear(startDate);
	getDay = startDate.getDay();
	startMonth = startDate.getMonth() + Number(1);
	startDate = startDate.getDate(startDate);

	var todaysDate = new Date();
	todayYear = todaysDate.getFullYear(todaysDate);
	todayMonth = todaysDate.getMonth() + Number(1);
	todaysDate = todaysDate.getDate(todaysDate);

	completeEndDate = endDate;
	endDate = nlapiStringToDate(endDate);
	endYear = endDate.getFullYear(endDate);
	endMonth = endDate.getMonth() + Number(1);
	endDate = endDate.getDate(endDate);

//	}

	if(startDate != '' && startDate != null && endDate != '' && endDate != null) {

		if(endMonth != startMonth) {

			//Leave Period for the selected date range without considering the work-calendar.
			leavePeriod = monthDiff(completeStartDate, completeEndDate);
//			alert('Inside month not equal '+leavePeriod);
			leavePeriod = Number(leavePeriod) + Number(1);
//			alert('Inside month not equal '+leavePeriod);
		}
		else if(endMonth == startMonth){

			//Leave Period for the selected date range without considering the work-calendar.
			leavePeriod = monthDiff(completeStartDate, completeEndDate);
//			alert('Inside month equal '+leavePeriod);
			leavePeriod = Number(leavePeriod) + Number(1);
//			alert('Inside month equal '+leavePeriod);
		}
		//To get emp type and work-calendar selected on emp.
		var empType = nlapiLookupField('employee', emp, 'employeetype');

//		//Search Setup to get values required to calculate leave period.
//		var filter = new Array();
//		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//		filter.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', null, 'is', empType));
//
//		var column = new Array();
//		column.push(new nlobjSearchColumn('custrecord_stlms_su_publicholidayasleave'));
//		column.push(new nlobjSearchColumn('custrecord_stlms_su_weeklyoffasleave'));
//		column.push(new nlobjSearchColumn('custrecord_stlms_su_daysforescalation'));
//
//		var search = nlapiSearchRecord('customrecord_stlms_setups', null, filter, column);
//
//		if(search) {
//
//			var publicHolidays = search[0].getValue('custrecord_stlms_su_publicholidayasleave');
//			var weekOffHolidays = search[0].getValue('custrecord_stlms_su_weeklyoffasleave');

			var todaysDate = new Date();
			todaysDate = nlapiDateToString(todaysDate);

//			escalationDay = search[0].getValue('custrecord_stlms_su_daysforescalation');

			//Add the days of escalation to the current date and set on leave application record
//			escalationDate = nlapiAddDays(nlapiStringToDate(todaysDate), escalationDay);
//			escalationDate = nlapiDateToString(escalationDate);

			//Suitelet call with required parameters to calculate leave period of the selected date range
			var URL = nlapiResolveURL('SUITELET', 'customscript_stlms_leaveapplicationretur', 'customdeploy_stlms_leaveapplicationretur', true);
			var searchResult = nlapiRequestURL(URL,{action:'calculateLeavePeriod',empType:empType, leavePeriod:leavePeriod, startYear:startYear, startMonth:startMonth, startDate:startDate, getDay:getDay,getLeaveType:leaveType,getEmpId:emp});

			var a = JSON.parse(searchResult.getBody());
			leavePeriod = a.calculatedLeave;
			escalationDay = a.escalationDay;
			totalLeaves = a.totalLeaves;
			nlapiLogExecution('DEBUG','Total Leaves in UE',totalLeaves);
			//Add the days of escalation to the current date and set on leave application record
			escalationDate = nlapiAddDays(nlapiStringToDate(todaysDate), escalationDay);
			escalationDate = nlapiDateToString(escalationDate);
			
//			//Calculated leave period.
//			leavePeriod = searchResult.getBody();

//		}
	}

	nlapiSetFieldValue('custrecord_stlms_la_dateofescalation', escalationDate);
	nlapiSetFieldValue('custrecord_stlms_la_leaveperiod', leavePeriod);

	var leaveBlncToConsider = nlapiGetFieldValue('custrecord_stlms_la_leavebalance');
	var carryForwardToConsider = nlapiGetFieldValue('custrecord_stlms_la_carriedforwardbalanc');
	totalLeaves = Number(leaveBlncToConsider) + Number(carryForwardToConsider);
//	alert('after totalLeaves '+totalLeaves);
	var totalAnnualLeaves = Number(leaveBlncToConsider) + Number(carryForwardToConsider);
	var totalLeaveOfAbsence = Number(totalLeaves) - Number(totalAnnualLeaves);
	//If the leave balance is less than the leave period.
	var annualLeaveId = 8;
	var leaveOfAbsenceId = 1;
	if(totalLeaves < leavePeriod && leaveType == annualLeaveId) {

//		alert('Insufficient leave balance. Kindly update the Leave period. Annual Leave Balance: '+totalAnnualLeaves+' Leave of Absence Balance: '+totalLeaveOfAbsence+'.');
		throw nlapiCreateError('Insufficient leave balance. Kindly update the Leave period. Annual Leave Balance: '+totalAnnualLeaves+' Leave of Absence Balance: '+totalLeaveOfAbsence+'.','Insufficient leave balance. Kindly update the Leave period. Annual Leave Balance: '+totalAnnualLeaves+' Leave of Absence Balance: '+totalLeaveOfAbsence+'.');
//		nlapiSetFieldValue('custrecord_stlms_la_enddate', '');
//		nlapiSetFieldValue('custrecord_stlms_la_leaveperiod', '');
		return false;
	}
	
	else if(leaveType == annualLeaveId && leavePeriod > totalAnnualLeaves && leavePeriod < totalLeaves) {
		
//		var res = confirm('Insufficient Annual Leave balance. The remaining will be deducted from Leave of Absence Balance: '+totalLeaveOfAbsence+'.');
//
//		if(res == false) {
//
//			nlapiSetFieldValue('custrecord_stlms_la_leaveperiod', '');
//			nlapiSetFieldValue('custrecord_stlms_la_enddate', '');
//			return false;
//		}
	}
	
	else if(leavePeriod == totalLeaveOfAbsence){
		
//		var res = confirm('Insufficient Annual Leave balance. The remaining will be deducted from Leave of Absence Balance: '+totalLeaveOfAbsence+'.');
//
//		if(res == false) {
//
//			nlapiSetFieldValue('custrecord_stlms_la_leaveperiod', '');
//			nlapiSetFieldValue('custrecord_stlms_la_enddate', '');
//			return false;
//		}
		
	}
	
	
	else if(leaveType != annualLeaveId && leavePeriod > totalAnnualLeaves){
		
		if(leaveType == leaveOfAbsenceId) {
			
			throw nlapiCreateError('Insufficient leave balance. Kindly update the Leave period','Insufficient leave balance. Kindly update the Leave period');
		}
		
//		var res = confirm('Leave balance is less than the leave period. Do you want to continue?');
//
//		if(res == false) {
//
//			nlapiSetFieldValue('custrecord_stlms_la_leaveperiod', '');
//			nlapiSetFieldValue('custrecord_stlms_la_enddate', '');
//			return false;
//		}
//		
		
	}
	return true;
}

function monthDiff(d1, d2)
{
	var months = '';

	if(d1)
	{
		d1 = d1.split('/');
		d1 = new Date(d1[2],d1[1]-1,d1[0]);
	}
	if(d2)
	{
		d2 = d2.split('/');
		d2 = new Date(d2[2],d2[1]-1,d2[0]);
	}

	var one_day = 1000*60*60*24;
	var date1 = d1;//new Date('4/19/2016');//mm/dd/yyyy
	var date2 = d2;//new Date('2/28/2017');//mm/dd/yyyy

	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();

	var difference_ms = date2_ms - date1_ms;

	var results = Math.round(difference_ms/one_day);

	return results;
}