/*************************************************************************
 * Copyright (c) 1998-2012 Softype, Inc.                                 
 * Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 * All Rights Reserved.                                                    
 *                                                                        
 * This software is the confidential and proprietary information of          
 * Softype, Inc. ("Confidential Information"). You shall not               
 * disclose such Confidential Information and shall use it only in          
 * accordance with the terms of the license agreement you entered into    
 * with Softype.
 *
 * @author:  SHYAM SHARMA
 * @date:    11 Mar 2016
 * @version: Revised version
 * 
 *************************************************************************/
var isResetEmpRecord = 'F';
var isMatchCalendMonth = '';
var leaveCalendrStMonth = '';
var runAccrualStDate='',runAccrualEnDate='';
var param = [];
var author;
var emailid;

function Call_LeaveAccumn_Schedule() 
{
	try
	{
		
		var recId  =   nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		
		var runPeriodLeavObj = nlapiLoadRecord(recType, recId);
		
		var runAccrualStDate =  runPeriodLeavObj.getFieldValue('custrecord_stlms_rpla_fromdate');
		var runAccrualEnDate =  runPeriodLeavObj.getFieldValue('custrecord_stlms_rpla_todate');
		var empType = runPeriodLeavObj.getFieldValue('custrecord_stlms_rpla_employeetype');
		
		var periodLeaveStMonth =  runPeriodLeavObj.getFieldValue('custrecord_stlms_rpla_fromdate');
		if(periodLeaveStMonth)
		{
			periodLeaveStMonth = periodLeaveStMonth.split('/');
			periodLeaveStMonth = periodLeaveStMonth[1];
		}
		
		var leaveMngtSysSetup = getLeavMangtSetup(empType);
		if(leaveMngtSysSetup.length>0) {
			leaveCalendrStMonth = leaveMngtSysSetup[0].getValue('custrecord_stlms_su_leavecalendarstart');
			emailid = leaveMngtSysSetup[0].getValue('custrecord_stlms_su_hremailid');
			var role = leaveMngtSysSetup[0].getValue('custrecord_stlms_su_hrapproverrole');
//			author = getEmployee(role);
			author = getSender(emailid);
			
		}
			
		
		if(leaveCalendrStMonth == periodLeaveStMonth)
			isMatchCalendMonth = 'T';
		else
			isMatchCalendMonth = 'F';

		param['custscript_run_period_accrual_eave_recid'] = recId;
		param['custscript_reset_employee_record'] = isResetEmpRecord;
		param['custscript_matching_calendar_month'] = isMatchCalendMonth;
		param['custscript_run_accrual_start_date'] = runAccrualStDate;
		param['custscript_run_accrual_end_date'] = runAccrualEnDate;
		param['custscript_run_accrual_emp_type'] = empType;
		param['custscript_hr_email_id'] = emailid;
		param['custscript_author'] = author;

		var returnStatus = nlapiScheduleScript('customscript_stlms_leave_accumulation','customdeploy_stlms_leave_accumulation',param);
		nlapiLogExecution('DEBUG', '54 returnStatus = ',returnStatus);
	}
	catch(error)
	{
		if (error.getDetails != undefined)
		{
			nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
			throw error;
		}
		else
		{
			nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function getEmployee(role) {
	
	var Approver_emp;
	
	var filt1 = new Array();
	filt1.push(new nlobjSearchFilter('role',null,'is',approverrole));
	filt1.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var search1 = nlapiSearchRecord('employee',null,filt1,null);

	if(search1)
	{
		Approver_emp=search1[0].getId();
		nlapiLogExecution('Debug', 'Approver_emp', Approver_emp);
	}

	return Approver_emp;
	
}

function getSender(emailId) {
	
	var filter = new Array();
	filter.push(new nlobjSearchFilter('email', null, 'is', emailId));
	
	var searchEmp = nlapiSearchRecord('employee', null, filter, null);
	var empId;
	
	if(searchEmp != '' && searchEmp != null)
		empId = searchEmp[0].getId();
	
	nlapiLogExecution('Debug', 'empId', empId);
	return empId;
	
}

function getLeavMangtSetup(empType)
{
	var sFilt = [];
	var sColm = [];
	
	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', null, 'is', empType));
	
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_leavecalendarstart'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_hremailid'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_hrapproverrole'));
	
	var srchSetup = nlapiSearchRecord('customrecord_stlms_setups', null, sFilt, sColm);
	
	return srchSetup;
	
}