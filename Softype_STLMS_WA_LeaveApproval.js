/* ************************************************************************************  
 ** Copyright (coffee) 1998-2012 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of          
 ** Softype, Inc.("Confidential Information"). You shall not disclose              
 ** such Confidential Information and shall use it only in accordance         
 ** with the terms of the license agreement you entered into with Softype.
 ** @version: Revised version               
 **                       
 ** @author: Rutika More
 ** @dated: 
 ** @Description: 
 ************************************************************************************ */
var leavePeriod, leaveBalance, carryForward, emp;
var leavesToDeductBalance = 0;
var leavesToDeductCarry = 0;

function workflowAction_deductLeavesOnMaster(type) {

	nlapiLogExecution('Debug', 'Start Script');
	nlapiLogExecution('Debug', 'Record Id', nlapiGetRecordId());
	var status = nlapiGetFieldValue('custrecord_stlms_la_approvalstatus');
	var leaveType = nlapiGetFieldValue('custrecord_stlms_la_deductfrom');
	emp = nlapiGetFieldValue('custrecord_stlms_la_employeename');
	nlapiLogExecution('Debug', 'Record Details', 'status : '+status+' leaveType : '+leaveType+' emp : '+emp);
	var recId = nlapiGetRecordId();
	var totalLeaves = 0;
	var leftLeavePeriod = 0;
	var leavesUsed = 0;
	if(status == 1) {
		// leaveType is Annual Leave
		if(leaveType == 8) {
			
			leavePeriod = nlapiGetFieldValue('custrecord_stlms_la_leaveperiod');
			leaveBalance = nlapiGetFieldValue('custrecord_stlms_la_leavebalance');
			carryForward = nlapiGetFieldValue('custrecord_stlms_la_carriedforwardbalanc');
			totalLeaves = Number(leaveBalance) + Number(carryForward);
			nlapiLogExecution('Debug', 'Record Details', 'leavePeriod : '+leavePeriod+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward+' Total Leaves : '+totalLeaves);

			//Annual Leave = 8, Casual Leave = 3, Maternity Leave = 5, Paternity Leave = 6, 
			//Privilige Leave = 2, Sabbatical/Study Leave = 4, Unpaid leave of absence = 7.
//			if(leaveType != 1) {
			
			if(leavePeriod < totalLeaves) {
				nlapiLogExecution('DEBUG','Leave Period less than Total Leaves');
				if(carryForward > 0) {

					if(carryForward < leavePeriod) {

						leavesToDeductBalance = Number(leavePeriod) - Number(carryForward);
						leavesToDeductCarry = Number(leavePeriod) - Number(leavesToDeductBalance);

						leaveBalance = Number(leaveBalance) - Number(leavesToDeductBalance);
						carryForward = Number(0);

					}

					else if(carryForward > leavePeriod) {

						leavesToDeductBalance = Number(0);
						carryForward = Number(carryForward) - Number(leavePeriod);
						leavesToDeductCarry = Number(leavePeriod) - Number(carryForward);
						leaveBalance = leaveBalance;

					}
					else if(carryForward = leavePeriod) {
						
						leavesToDeductBalance = Number(0);
						leavesToDeductCarry = Number(leavePeriod);
						carryForward = Number(carryForward) - Number(leavePeriod);
						leaveBalance = leaveBalance;
					}

				}

				else if(carryForward == 0) {

					leavesToDeductBalance = Number(leavePeriod);
					leavesToDeductCarry = Number(0);

					leaveBalance = Number(leaveBalance) - Number(leavePeriod);
					leavesToDeductBalance = Number(leavePeriod) - Number(leaveBalance);
					carryForward = Number(0);

//					nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromleavebal', leavePeriod);
//					nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromcarryfwd', 0);

				}
					nlapiLogExecution('Debug', 'Record Details', 'leavesToDeductBalance : '+leavesToDeductBalance+' leavesToDeductCarry : '+leavesToDeductCarry+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward);
					leaveBalance = Number(leaveBalance);
					leaveBalance = parseFloat(Number(leaveBalance).toFixed(2));
					getLeaveBalanceLine(leaveType, emp, leaveBalance, carryForward, leavePeriod);
					var submit = nlapiSubmitField(nlapiGetRecordType(), recId, ['custrecord_stlms_la_deductedfromleavebal', 'custrecord_stlms_la_deductedfromcarryfwd', 'custrecord_stlms_la_approvalstatus'], [leavesToDeductBalance, leavesToDeductCarry, status]);
					nlapiLogExecution('Debug', 'End Script');
					nlapiSetRedirectURL('RECORD', nlapiGetRecordType(), submit, false);
			}
			
			if(leavePeriod > totalLeaves) {
				nlapiLogExecution('DEBUG','Leave Period greater than Total Leaves');
				if(carryForward > 0) {
					
					leavesToDeductBalance = Number(leavePeriod) - Number(carryForward);
					leavesToDeductCarry = Number(leavePeriod) - Number(leavesToDeductBalance);
//					leaveBalance = Number(leaveBalance) - Number(leavesToDeductBalance);
					leftLeavePeriod = Number(leavesToDeductBalance) - Number(leaveBalance);
					
					leavesToDeductBalance = Number(leavesToDeductBalance) - Number(leftLeavePeriod);
					nlapiLogExecution('DEBUG','Left Leaves',leftLeavePeriod);
					leaveBalance = Number(0);
					carryForward = Number(0);
					
					nlapiLogExecution('Debug', 'Record Details', 'leavesToDeductBalance : '+leavesToDeductBalance+' leavesToDeductCarry : '+leavesToDeductCarry+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward);
					leaveBalance = Number(leaveBalance);
					leaveBalance = parseFloat(Number(leaveBalance).toFixed(2));
					getLeaveBalanceLine(leaveType, emp, leaveBalance, carryForward, totalLeaves);
					var submit = nlapiSubmitField(nlapiGetRecordType(), recId, ['custrecord_stlms_la_deductedfromleavebal', 'custrecord_stlms_la_deductedfromcarryfwd', 'custrecord_stlms_la_approvalstatus'], [leavesToDeductBalance, leavesToDeductCarry, status]);
					deductFromLoa(leftLeavePeriod,emp,status,recId);
					nlapiLogExecution('Debug', 'End Script');
					nlapiSetRedirectURL('RECORD', nlapiGetRecordType(), submit, false);
					// deduct left leave Period from Leave of Absence
					
				}
				if(carryForward == 0) {
					
					leftLeavePeriod = Number(leavePeriod) - Number(leaveBalance);
					leavesUsed = Number(leavePeriod) - Number(leftLeavePeriod);
					leaveBalance = Number(0);
					carryForward = Number(0);
					leavesToDeductBalance = Number(leavesUsed);
					leavesToDeductCarry = Number(0);
//					leavesToDeductBalance = Number(leavePeriod);
//					leavesToDeductCarry = Number(0);
//					
//					leaveBalance = Number(leaveBalance) - Number(leavePeriod);
//					carryForward = Number(0);

//					nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromleavebal', leavePeriod);
//					nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromcarryfwd', 0);
					
					nlapiLogExecution('Debug', 'Record Details', 'leavesToDeductBalance : '+leavesToDeductBalance+' leavesToDeductCarry : '+leavesToDeductCarry+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward);
					leaveBalance = Number(leaveBalance);
					leaveBalance = parseFloat(Number(leaveBalance).toFixed(2));
					getLeaveBalanceLine(leaveType, emp, leaveBalance, carryForward, leavesUsed);
					var submit = nlapiSubmitField(nlapiGetRecordType(), recId, ['custrecord_stlms_la_deductedfromleavebal', 'custrecord_stlms_la_deductedfromcarryfwd', 'custrecord_stlms_la_approvalstatus'], [leavesToDeductBalance, leavesToDeductCarry, status]);
					// deduct left leave Period from Leave of Absence
					deductFromLoa(leftLeavePeriod,emp,status,recId);
					
					nlapiLogExecution('Debug', 'Main End Script');
					nlapiSetRedirectURL('RECORD', nlapiGetRecordType(), submit, false);

				}
				
				
				
			}
			if(leavePeriod == totalLeaves) {
				nlapiLogExecution('DEBUG','Leave Period equal to total leaves');
				if(carryForward > 0) {
					leavesToDeductBalance = Number(leavePeriod) - Number(carryForward);
					leavesToDeductCarry = Number(leavePeriod) - Number(leavesToDeductBalance);
					carryForward = Number(0);
					if(leavesToDeductBalance > 0) {
						
						leavesToDeductBalance = Number(leavesToDeductBalance) - Number(leaveBalance);
						leavesToDeductBalance = Number(leaveBalance);
						leaveBalance = Number(0);
					}
					
				}
				else if(carryForward == 0) {
					
					leavesToDeductBalance = Number(leavePeriod) - Number(leaveBalance);
					leavesToDeductBalance = Number(leavePeriod) - leavesToDeductBalance
					leavesToDeductCarry  = Number(0);
					leaveBalance = 0;
					carryForward = 0;
				}
				
				
				nlapiLogExecution('Debug', 'Record Details', 'leavesToDeductBalance : '+leavesToDeductBalance+' leavesToDeductCarry : '+leavesToDeductCarry+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward);
				leaveBalance = Number(leaveBalance);
				leaveBalance = parseFloat(Number(leaveBalance).toFixed(2));
				getLeaveBalanceLine(leaveType, emp, leaveBalance, carryForward, leavePeriod);
				var submit = nlapiSubmitField(nlapiGetRecordType(), recId, ['custrecord_stlms_la_deductedfromleavebal', 'custrecord_stlms_la_deductedfromcarryfwd', 'custrecord_stlms_la_approvalstatus'], [leavesToDeductBalance, leavesToDeductCarry, status]);
				nlapiLogExecution('Debug', 'End Script');
				nlapiSetRedirectURL('RECORD', nlapiGetRecordType(), submit, false);
				
				
			}
			
		}
		//-----------------------if leave type is other than annual leave it will go into else part---------------------------------------------//
		else {

			leavePeriod = nlapiGetFieldValue('custrecord_stlms_la_leaveperiod');
			leaveBalance = nlapiGetFieldValue('custrecord_stlms_la_leavebalance');
			carryForward = nlapiGetFieldValue('custrecord_stlms_la_carriedforwardbalanc');

			nlapiLogExecution('Debug', 'Record Details', 'leavePeriod : '+leavePeriod+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward);

			//Annual Leave = 8, Casual Leave = 3, Maternity Leave = 5, Paternity Leave = 6, 
			//Privilige Leave = 2, Sabbatical/Study Leave = 4, Unpaid leave of absence = 7.
//			if(leaveType != 1) {

			if(carryForward > 0) {

				if(carryForward < leavePeriod) {

					leavesToDeductBalance = Number(leavePeriod) - Number(carryForward);
					leavesToDeductCarry = Number(leavePeriod) - Number(leavesToDeductBalance);

					leaveBalance = Number(leaveBalance) - Number(leavesToDeductBalance);
					carryForward = Number(0);

				}

				else if(carryForward >= leavePeriod) {

					leavesToDeductBalance = Number(0);
					leavesToDeductCarry = Number(leavePeriod);

					carryForward = Number(carryForward) - Number(leavePeriod);
					leaveBalance = leaveBalance;

				}

			}

			else if(carryForward == 0) {
				leavesToDeductCarry = Number(0);

				leaveBalance = Number(leaveBalance) - Number(leavePeriod);
				carryForward = Number(0);

//				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromleavebal', leavePeriod);
//				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromcarryfwd', 0);

			}

			nlapiLogExecution('Debug', 'Record Details', 'leavesToDeductBalance : '+leavesToDeductBalance+' leavesToDeductCarry : '+leavesToDeductCarry+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward);
			leaveBalance = Number(leaveBalance);
			leaveBalance = parseFloat(Number(leaveBalance).toFixed(2));
			getLeaveBalanceLine(leaveType, emp, leaveBalance, carryForward, leavePeriod);
			var submit = nlapiSubmitField(nlapiGetRecordType(), recId, ['custrecord_stlms_la_deductedfromleavebal', 'custrecord_stlms_la_deductedfromcarryfwd', 'custrecord_stlms_la_approvalstatus'], [leavesToDeductBalance, leavesToDeductCarry, status]);
			nlapiLogExecution('Debug', 'End Script');
			nlapiSetRedirectURL('RECORD', nlapiGetRecordType(), submit, false);

		}
	}
}

function getLeaveBalanceLine(leaveType, emp, leaveBalance, carryForward, leavePeriod) {

	nlapiLogExecution('Debug', 'Inside Get Leave Balance Line Function', 'leaveType : '+leaveType+' emp : '+emp+' leaveBalance : '+leaveBalance+' carryForward : '+carryForward+' leavePeriod : '+leavePeriod);
	var filter = new Array();
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveType));
	filter.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', emp));

	var column = new Array();
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leaveutilized'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_parent'));

	var search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filter, column);
	var submitLine;

	if(search) {
		nlapiLogExecution('Debug', 'Inside Search Line');
		var leavetypes='';

		var filterEmp = new Array();
		filterEmp.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filterEmp.push(new nlobjSearchFilter('internalid', null, 'is', emp));

		var columnEmp = new Array();
		columnEmp.push(new nlobjSearchColumn('custentity_stlms_leavetypes'));

		var searchEmp = nlapiSearchRecord('employee', null, filterEmp, columnEmp);

		if (searchEmp)
		{
			leavetypes = searchEmp[0].getValue('custentity_stlms_leavetypes');
			nlapiLogExecution('Debug', 'Leaves on Employee ', leavetypes);
			leavetypes = leavetypes.split(',');
			nlapiLogExecution('Debug', 'custentity_stlms_leavetypes', leavetypes);
			nlapiLogExecution('Debug', 'custentity_stlms_leavetypes', leavetypes.indexOf(leaveType));
		}

		if(inArray(leaveType,leavetypes)) {

			nlapiLogExecution('Debug', 'Inside index of matched', leavetypes);

			var asonDate = new Date();
			asonDate = nlapiDateToString(asonDate);//nlapiStringToDate(todaysDate);

			var leavesUtilized = search[0].getValue('custrecord_stlms_lbline_leaveutilized');
			nlapiLogExecution('DEBUG','Leaves Utilized',leavesUtilized);
			leavesUtilized = Number(leavesUtilized) + Number(leavePeriod);

			submitLine = nlapiSubmitField('customrecord_stlms_leavebalancemasterlin', search[0].getId(), ['custrecord_stlms_lbline_leavebalance', 'custrecord_stlms_lbline_carriedforward', 'custrecord_stlms_lbline_leaveutilized'],[leaveBalance, carryForward, leavesUtilized]);

			//Submit Leave Balance Master record
			nlapiSubmitField('customrecord_stlms_leavebalancemaster', search[0].getValue('custrecord_stlms_lbline_parent'), 'custrecord_stlms_lbm_asondate', asonDate);
		}
		else {

			nlapiLogExecution('Debug', 'Inside index of not matched', leavetypes);

			var asonDate = new Date();
			asonDate = nlapiDateToString(asonDate);

			var leavesUtilized = search[0].getValue('custrecord_stlms_lbline_leaveutilized');
			leavesUtilized = Number(leavesUtilized) + Number(leavePeriod);

			submitLine = nlapiSubmitField('customrecord_stlms_leavebalancemasterlin', search[0].getId(), ['custrecord_stlms_lbline_leavebalance', 'custrecord_stlms_lbline_carriedforward', 'custrecord_stlms_lbline_leaveutilized'],[0, 0, leavesUtilized]);

			//Submit Leave Balance Master record
			nlapiSubmitField('customrecord_stlms_leavebalancemaster', search[0].getValue('custrecord_stlms_lbline_parent'), 'custrecord_stlms_lbm_asondate', asonDate);

		}

	}

	else {

		var filter = new Array();
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filter.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', null, 'is', emp));

		var search = nlapiSearchRecord('customrecord_stlms_leavebalancemaster', null, filter, null);

		var recId;

		if(search) {
			recId = search[0].getId();
		}

		var createRecord = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');
		createRecord.setFieldValue('custrecord_stlms_lbline_leavetype', leaveType);
		createRecord.setFieldValue('custrecord_stlms_lbline_leavebalance', 0);
		createRecord.setFieldValue('custrecord_stlms_lbline_carriedforward', 0);
		createRecord.setFieldValue('custrecord_stlms_lbline_leaveutilized', leavePeriod);
		if(recId) {
			createRecord.setFieldValue('custrecord_stlms_lbline_parent', recId);
		}
		submitLine = nlapiSubmitRecord(createRecord);

	}

	nlapiLogExecution('Debug', 'Submited Record', submitLine);
	return submitLine;
}


//---------Function to deduct leaves if Annual Leave becomes Zero--- Author:Sachin Savale--------------------------------------------------------------------------//

function deductFromLoa(leftLeavePeriod,emp,status,recId) {
	
	nlapiLogExecution('DEBUG','Inside deduct from LOA');
	var totalLeavesLoa = 0;
	var leaveType = 1;     // leave of Absence
	var carryForwardLoa = 0;
	var leaveBalanceLoa = 0;
	var filter = new Array();
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', 'custrecord_stlms_lbline_parent', 'is', emp));
	// leaveOfAbsence = 7 i.e leave od Absence Id
	filter.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leaveType));

	var column = new Array();
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leaveutilized'));
	// will search for leave type = leave of absence
	var search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin', null, filter, column); 
	
	 leaveBalanceLoa = search[0].getValue('custrecord_stlms_lbline_leavebalance');
	 carryForwardLoa = search[0].getValue('custrecord_stlms_lbline_carriedforward');
	 nlapiLogExecution('DEBUG','LOA leave balance and carry forward',leaveBalanceLoa+'##'+carryForwardLoa);
	 
	totalLeavesLoa = Number(leaveBalanceLoa) + Number(carryForwardLoa);
	
	if(carryForwardLoa > 0) {

		if(carryForwardLoa < leftLeavePeriod) {

			leavesToDeductBalance = Number(leftLeavePeriod) - Number(carryForwardLoa);
			leavesToDeductCarry = Number(leftLeavePeriod) - Number(leavesToDeductBalance);

			leaveBalanceLoa = Number(leaveBalanceLoa) - Number(leavesToDeductBalance);
			carryForwardLoa = Number(0);

		}

		else if(carryForwardLoa >= leftLeavePeriod) {

			leavesToDeductBalance = Number(0);
			leavesToDeductCarry = Number(leftLeavePeriod);

			carryForwardLoa = Number(carryForwardLoa) - Number(leftLeavePeriod);
			leaveBalanceLoa = leaveBalanceLoa;

		}

	}

	else if(carryForwardLoa == 0) {

		leavesToDeductBalance = Number(leftLeavePeriod);
		leavesToDeductCarry = Number(0);

		leaveBalanceLoa = Number(leaveBalanceLoa) - Number(leftLeavePeriod);
		carryForwardLoa = Number(0);

//		nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromleavebal', leavePeriod);
//		nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custrecord_stlms_la_deductedfromcarryfwd', 0);
		

	}
	
	nlapiLogExecution('Debug', 'Record Details inside deduct from loa function', 'leavesToDeductBalance : '+leavesToDeductBalance+' leavesToDeductCarry : '+leavesToDeductCarry+' leaveBalance LOA : '+leaveBalanceLoa+' carryForward LOA: '+carryForwardLoa);
	leaveBalanceLoa = Number(leaveBalanceLoa);
	leaveBalanceLoa = parseFloat(Number(leaveBalanceLoa).toFixed(2));
	getLeaveBalanceLine(leaveType, emp, leaveBalanceLoa, carryForwardLoa, leftLeavePeriod);
	var submit = nlapiSubmitField(nlapiGetRecordType(), recId, ['custrecord_stlms_la_deducted_loaleavebal', 'custrecord_stlms_la_deducted_loacarryfwd', 'custrecord_stlms_la_approvalstatus'], [leavesToDeductBalance, leavesToDeductCarry, status]);
	nlapiLogExecution('Debug', 'End Script');

	
	
} // end of deductFromLoa() function



//-----------------function to find value in array ----------------------------------------------------------------------------------//


function arrayCompare(a1, a2) {
    if (a1.length != a2.length) return false;
    var length = a2.length;
    for (var i = 0; i < length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(typeof haystack[i] == 'object') {
            if(arrayCompare(haystack[i], needle)) return true;
        } else {
            if(haystack[i] == needle) return true;
        }
    }
    return false;
}

//-------------------------------------------------------------------------------------------------------------------------------------------//