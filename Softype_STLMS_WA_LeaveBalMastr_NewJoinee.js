/*************************************************************************
 * Copyright (c) 1998-2012 Softype, Inc.                                 
 * Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 * All Rights Reserved.                                                    
 *                                                                        
 * This software is the confidential and proprietary information of          
 * Softype, Inc. ("Confidential Information"). You shall not               
 * disclose such Confidential Information and shall use it only in          
 * accordance with the terms of the license agreement you entered into    
 * with Softype.
 *
 * @author:  SHYAM SHARMA
 * @date:    23 Mar 2016
 * @version: Revised version
 * 
 *************************************************************************/
var employeeName = '';
var currContext = nlapiGetContext();
var todayDate = nlapiDateToString(new Date());
var statusComplete = 2;//Complete
var calendStMonth='',calendEnMonth='',isCalendrYearChanged='F';
var calendrNewStartYear='';
var currContext = nlapiGetContext('');
var leavTypeArr = [];
var srchLeaveTypeSetup = '';
var prevBalance = 0;

function CalcLeaveBalNewJoinee() 
{
	//try	{
	var recId  =  nlapiGetRecordId();
	var recType = nlapiGetRecordType();
	var empRecObj = nlapiLoadRecord(recType, recId);
	employeeName = empRecObj.getFieldValue('entityid');
	var employeeType = empRecObj.getFieldValue('employeetype');
	var empLeaveType = empRecObj.getFieldValues('custentity_stlms_leavetypes');
	var jobTitle = empRecObj.getFieldValue('custentity_stlms_jobtitle');
//	nlapiLogExecution('DEBUG', 'inside job title', jobTitle);

	var servicePeriod = empRecObj.getFieldValue('custentity_stlms_serviceperiod');
	if(empLeaveType)
	{
		for(var pp=0;pp<empLeaveType.length;pp++)
		{
			leavTypeArr.push(empLeaveType[pp]);
		}
	}
	nlapiLogExecution('DEBUG', '43  leavTypeArr = ',leavTypeArr+'@@'+'employeeType = '+employeeType);
	/** Get accrual factor, carried forward and leave type from leave type setup record.**/
	var leaveTypeSetups = [];//getLeaveTypeSetup(leavTypeArr,employeeType,jobTitle,servicePeriod);

	var isEligibleForRunAccrual = empRecObj.getFieldValue('custentity_stlms_eligibilityforleaveacc');
	var isLeaveAccrualNewJoinee = empRecObj.getFieldValue('custentity_stlms_leaveaccrualnewjoinee');
	var isResumeOneTimeLeaveAccrl = empRecObj.getFieldValue('custentity_stlms_resumeleaveaccrual');

	var oldFromDate = empRecObj.getFieldValue('custentity_old_from_date');
	var fromDate = empRecObj.getFieldValue('custentity_stlms_leaveaccrualdatescript');
	var toBeAddedAnnually;
	var annualCompleteLeave = 0;

	if(isEligibleForRunAccrual=='T' && isLeaveAccrualNewJoinee=='F')
	{
		/** New Joinee part stated **/

		var ToDate  = getRunPeriodicLeaveAccrDate(employeeType);
		if(fromDate=='' || ToDate=='')
		{
			var scriptId = currContext.getScriptId();
			var errorMsg = 'Date field is empty for employee '+employeeName;

			var errorCatcherObj = nlapiCreateRecord('customrecord_stlms_errorcatcher');
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_employeename',recId);
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

			var errorCatcherSubmitId = nlapiSubmitRecord(errorCatcherObj,true,true);
			nlapiLogExecution('DEBUG', '74 errorCatcherSubmitId = ',errorCatcherSubmitId);
			return;
		}

		var calendrSTMonthNew='',calendrENMonthNew='';
		var leaveAccrualStDate='',latestRunAccrualDate='',LeaveBalMastrToDate='';

		var setupCalederSearch = getLeaveCalenderSearch(employeeType);
		if(setupCalederSearch)
		{
			calendrSTMonthNew = setupCalederSearch[0].getValue('custrecord_stlms_su_calendarstartdate');
			calendrENMonthNew = setupCalederSearch[0].getValue('custrecord_stlms_su_calendarenddate');
		}

		var leaveAccrualStDate = fromDate;
		var latestRunAccrualDate = ToDate;
		var leavBalMastrFromDate = fromDate;

		var newYearStartDate = calendrSTMonthNew;
		var prevYearLastDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));

		nlapiLogExecution('DEBUG', '148 calendrSTMonthNew = ',calendrSTMonthNew+' calendrENMonthNew = '+calendrENMonthNew+' leaveAccrualStDate '+leaveAccrualStDate);
		//nlapiLogExecution('DEBUG', '149 leaveAccrualStDate = ',leaveAccrualStDate+'@@'+'latestRunAccrualDate = '+latestRunAccrualDate+'@@'+'prevYearLastDate = '+prevYearLastDate);

		//Update Leave Balance Master Line record, if record exist.
		var sFilt = [];
		var sColm = [];

		sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		sFilt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', null, 'is', recId));

		var searchResults = nlapiSearchRecord('customrecord_stlms_leavebalancemaster', null, sFilt, sColm);
		if(searchResults)
		{
			nlapiLogExecution('DEBUG', 'Setting from date', searchResults[0].getId());
			var leaveBalMastrObj = nlapiLoadRecord('customrecord_stlms_leavebalancemaster', searchResults[0].getId());
//			leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
			leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_todate',ToDate);
			leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_asondate',todayDate);
//			leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_employeename',recId);

//			var leaveBalMastrSubmitId = nlapiSubmitRecord(leaveBalMastrObj,true,true);
//			if(leaveBalMastrSubmitId)
//			{
			var leaveBalMastrLineSubmitId;
			var lineCount = leaveBalMastrObj.getLineItemCount('recmachcustrecord_stlms_lbline_parent');
			nlapiLogExecution('DEBUG', 'lineCount', lineCount);
			for(var lt = 1; lt <= lineCount; lt ++) {

				var empLeaveType = leaveBalMastrObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype', lt);//leavTypeArr[lt];
				nlapiLogExecution('DEBUG', 'empLeaveType', empLeaveType);

				var leaveBalanceIfAvail = leaveBalMastrObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance', lt);//leavTypeArr[lt];
				nlapiLogExecution('DEBUG', 'leaveBalanceIfAvail', leaveBalanceIfAvail);

				if(jobTitle){

					nlapiLogExecution('DEBUG', 'inside job title', jobTitle);

					var sFilta = new Array();
					sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

					var sColma = new Array();
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

					leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
					if(leaveTypeSetups)
					{
						nlapiLogExecution('DEBUG', 'inside search all found');
						var leaveBal = 0;
						for(var ii=0;ii<leaveTypeSetups.length;ii++)
						{

							var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
							var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');
							nlapiLogExecution('DEBUG', 'Values Compared', 'min '+minexprnc+' max '+maxexprnc+' servicePeriod '+servicePeriod);

							if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

								nlapiLogExecution('DEBUG', 'inside all found');
								var carrFwd = 0;
								var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
								var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
								var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
								var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
								toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
								annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
//								var leaveBalMastrLineObj = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');

								nlapiLogExecution('DEBUG', '173 leaveType = ',leaveType+'@@'+'leaveType = '+leaveType+'@@'+'accrualFacotr = '+accrualFacotr);

								/** Normal Use case when calender year does not change **/
								if(nlapiStringToDate(leaveAccrualStDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveAccrualStDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
								{
									fromDate = leavBalMastrFromDate;
//									leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
									if(toBeAddedAnnually == 'T')
										leaveBal = Number(annualCompleteLeave);
									else
										leaveBal = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,latestRunAccrualDate);
									prevBalance = 0;
									nlapiLogExecution('DEBUG', '182 cas1 fromDate = ',fromDate+'@@'+'leaveBal = '+leaveBal);
								}
								/** Leave Calculation from prev year to next year**/
								else if(nlapiStringToDate(leaveAccrualStDate)<nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
								{
									fromDate = calendrSTMonthNew; 
									leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
									var calcLeaveBalPrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,prevYearLastDate);
									var calcLeaveBalNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,latestRunAccrualDate);
									prevBalance = Number(calcLeaveBalPrev);

									nlapiLogExecution('DEBUG', '193 cas2 fromDate = ',fromDate+'@@'+'BalPrev = '+calcLeaveBalPrev+'@@'+'BalNext = '+calcLeaveBalNext);

									if(isCarryForward=='T')
									{
										var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
										var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
										nlapiLogExecution('DEBUG', '199 cas2CFT totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);

										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave) + Number(minCarryForwd);
										else
											leaveBal = Number(calcLeaveBalNext) + Number(minCarryForwd);
									}	
									else
									{
										var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
										var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
										nlapiLogExecution('DEBUG', '207 cas2CFF totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);										

										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave);
										else
											leaveBal = Number(calcLeaveBalNext);

										carrFwd  = parseFloat(minCarryForwd).toFixed(2);
									}
								}

								leaveBal = Number(leaveBal) + Number(leaveBalanceIfAvail);
								leaveBal = parseFloat(leaveBal).toFixed(2);
								prevBalance = parseFloat(prevBalance).toFixed(2);

//								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent', 'custrecord_stlms_lbline_leavetype',lt,leaveType);	
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent', 'custrecord_stlms_lbline_leavebalance',lt,leaveBal);
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent', 'custrecord_stlms_lbline_carriedforward',lt,carrFwd);
//								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent', 'custrecord_stlms_lbline_parent',lt,leaveBalMastrSubmitId);
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent', 'custrecord_stmm_lbline_leavebalprevious',lt,prevBalance);
//								leaveBalMastrLineSubmitId = nlapiSubmitRecord(leaveBalMastrLineObj,true,true);
							}
						}
					}
					else
					{
						nlapiLogExecution('DEBUG', 'inside service period found before search');
						var sFilta = new Array();
						sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

						var sColma = new Array();
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

						leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
						if(leaveTypeSetups)
						{
							nlapiLogExecution('DEBUG', 'inside search service period found');
							var leaveBal = 0;
							for(var ii=0;ii<leaveTypeSetups.length;ii++)
							{

								var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
								var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

								if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {
									nlapiLogExecution('DEBUG', 'inside service period found');
									var carrFwd = 0;
									var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
									var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
									var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
									var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
									toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
									annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

//									var leaveBalMastrLineObj = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');

									nlapiLogExecution('DEBUG', '266 leaveType = ',leaveType+'@@'+'leaveType = '+leaveType+'@@'+'accrualFacotr = '+accrualFacotr);

									/** Normal Use case when calender year does not change **/
									if(nlapiStringToDate(leaveAccrualStDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveAccrualStDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
									{
										fromDate = leavBalMastrFromDate;
//										leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave);
										else
											leaveBal = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,latestRunAccrualDate);
										prevBalance = Number(0);
										nlapiLogExecution('DEBUG', '275 cas1 fromDate = ',fromDate+'@@'+'leaveBal = '+leaveBal);
									}
									/** Leave Calculation from prev year to next year**/
									else if(nlapiStringToDate(leaveAccrualStDate)<nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
									{
										fromDate = calendrSTMonthNew; 
										leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
										var calcLeaveBalPrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,prevYearLastDate);
										var calcLeaveBalNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,latestRunAccrualDate);
										prevBalance = Number(calcLeaveBalPrev);
										nlapiLogExecution('DEBUG', '285 cas2 fromDate = ',fromDate+'@@'+'BalPrev = '+calcLeaveBalPrev+'@@'+'BalNext = '+calcLeaveBalNext);

										if(isCarryForward=='T')
										{
											var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
											var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
											nlapiLogExecution('DEBUG', '291 cas2CFT totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);
											if(toBeAddedAnnually == 'T')
												leaveBal = Number(annualCompleteLeave) + Number(minCarryForwd);
											else
												leaveBal = Number(calcLeaveBalNext) + Number(minCarryForwd);
										}	
										else
										{
											var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
											var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
											nlapiLogExecution('DEBUG', '299 cas2CFF totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);										

											if(toBeAddedAnnually == 'T')
												leaveBal = Number(annualCompleteLeave);
											else
												leaveBal = Number(calcLeaveBalNext);
											carrFwd  = parseFloat(minCarryForwd).toFixed(2);
										}
									}	

									leaveBal = Number(leaveBal) + Number(leaveBalanceIfAvail);
									leaveBal = parseFloat(leaveBal).toFixed(2);
									prevBalance = parseFloat(prevBalance).toFixed(2);

//									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',lt,leaveType);	
									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',lt,leaveBal);
									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',lt,carrFwd);
//									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_parent',lt,leaveBalMastrSubmitId);
									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',lt,prevBalance);
//									leaveBalMastrLineSubmitId = nlapiSubmitRecord(leaveBalMastrLineObj,true,true);
								}
							}
						}

						else {
							nlapiLogExecution('DEBUG', 'inside job title found before search');

							var sFilta = new Array();
							sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', empLeaveType));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

							var sColma = new Array();
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

							leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
							if(leaveTypeSetups)
							{
								nlapiLogExecution('DEBUG', 'inside search job title found');
								var leaveBal = 0;
								for(var ii=0;ii<leaveTypeSetups.length;ii++)
								{

									var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
									var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

//									if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {
									nlapiLogExecution('DEBUG', 'inside job title found');
									var carrFwd = 0;
									var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
									var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
									var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
									var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
									toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
									annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

//									var leaveBalMastrLineObj = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');

									nlapiLogExecution('DEBUG', '359 leaveType = ',leaveType+'@@'+'leaveType = '+leaveType+'@@'+'accrualFacotr = '+accrualFacotr);

									/** Normal Use case when calender year does not change **/
									if(nlapiStringToDate(leaveAccrualStDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveAccrualStDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
									{
										fromDate = leavBalMastrFromDate;
//										leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave);
										else
											leaveBal = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,latestRunAccrualDate);
										prevBalance = Number(0);
										nlapiLogExecution('DEBUG', '368 cas1 fromDate = ',fromDate+'@@'+'leaveBal = '+leaveBal);
									}
									/** Leave Calculation from prev year to next year**/
									else if(nlapiStringToDate(leaveAccrualStDate)<nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
									{
										fromDate = calendrSTMonthNew; 
										leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
										var calcLeaveBalPrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,prevYearLastDate);
										var calcLeaveBalNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,latestRunAccrualDate);
										prevBalance = Number(calcLeaveBalPrev);
										nlapiLogExecution('DEBUG', '378 cas2 fromDate = ',fromDate+'@@'+'BalPrev = '+calcLeaveBalPrev+'@@'+'BalNext = '+calcLeaveBalNext);

										if(isCarryForward=='T')
										{
											var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
											var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
											nlapiLogExecution('DEBUG', '384 cas2CFT totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);
											if(toBeAddedAnnually == 'T')
												leaveBal = Number(annualCompleteLeave) + Number(minCarryForwd);
											else
												leaveBal = Number(calcLeaveBalNext) + Number(minCarryForwd);
										}	
										else
										{
											var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
											var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
											nlapiLogExecution('DEBUG', '392 cas2CFF totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);										

											if(toBeAddedAnnually == 'T')
												leaveBal = Number(annualCompleteLeave);
											else
												leaveBal = Number(calcLeaveBalNext);

											carrFwd  = parseFloat(minCarryForwd).toFixed(2);
										}
									}	

									leaveBal = Number(leaveBal) + Number(leaveBalanceIfAvail);
									leaveBal = parseFloat(leaveBal).toFixed(2);
									prevBalance = parseFloat(prevBalance).toFixed(2);

//									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',lt,leaveType);	
									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',lt,leaveBal);
									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',lt,carrFwd);
//									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_parent',lt,leaveBalMastrSubmitId);
									leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',lt,prevBalance);
//									leaveBalMastrLineSubmitId = nlapiSubmitRecord(leaveBalMastrLineObj,true,true);
//									}
								}
							}

							else {
								nlapiLogExecution('DEBUG', 'inside all not found before search');

								var sFilta = new Array();
								sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

								var sColma = new Array();
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

								leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
								if(leaveTypeSetups)
								{
									nlapiLogExecution('DEBUG', 'inside search all not found');
									var leaveBal = 0;
									for(var ii=0;ii<leaveTypeSetups.length;ii++)
									{

										var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
										var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

//										if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {
										nlapiLogExecution('DEBUG', 'inside all not found');
										var carrFwd = 0;
										var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
										var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
										var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
										var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
										toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
										annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

//										var leaveBalMastrLineObj = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');

										nlapiLogExecution('DEBUG', '452 leaveType = ',leaveType+'@@'+'leaveType = '+leaveType+'@@'+'accrualFacotr = '+accrualFacotr);

										/** Normal Use case when calender year does not change **/
										if(nlapiStringToDate(leaveAccrualStDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveAccrualStDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
										{
											fromDate = leavBalMastrFromDate;
//											leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
											if(toBeAddedAnnually == 'T')
												leaveBal = Number(annualCompleteLeave);
											else
												leaveBal = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,latestRunAccrualDate);
											prevBalance = Number(0);
											nlapiLogExecution('DEBUG', '461 cas1 fromDate = ',fromDate+'@@'+'leaveBal = '+leaveBal);
										}
										/** Leave Calculation from prev year to next year**/
										else if(nlapiStringToDate(leaveAccrualStDate)<nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
										{
											fromDate = calendrSTMonthNew; 
											leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
											var calcLeaveBalPrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,prevYearLastDate);
											var calcLeaveBalNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,latestRunAccrualDate);
											prevBalance = Number(calcLeaveBalPrev);
											nlapiLogExecution('DEBUG', '471 cas2 fromDate = ',fromDate+'@@'+'BalPrev = '+calcLeaveBalPrev+'@@'+'BalNext = '+calcLeaveBalNext);

											if(isCarryForward=='T')
											{
												var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
												var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
												nlapiLogExecution('DEBUG', '477 cas2CFT totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);
												if(toBeAddedAnnually == 'T')
													leaveBal = Number(annualCompleteLeave) + Number(minCarryForwd);
												else
													leaveBal = Number(calcLeaveBalNext) + Number(minCarryForwd);
											}	
											else
											{
												var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
												var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
												nlapiLogExecution('DEBUG', '485 cas2CFF totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);										
												if(toBeAddedAnnually == 'T')
													leaveBal = Number(annualCompleteLeave);
												else
													leaveBal = Number(calcLeaveBalNext);

												carrFwd  = parseFloat(minCarryForwd).toFixed(2);
											}
										}	

										leaveBal = Number(leaveBal) + Number(leaveBalanceIfAvail);
										leaveBal = parseFloat(leaveBal).toFixed(2);
										prevBalance = parseFloat(prevBalance).toFixed(2);

//										leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',lt,leaveType);	
										leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',lt,leaveBal);
										leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',lt,carrFwd);
//										leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_parent',lt,leaveBalMastrSubmitId);
										leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',lt,prevBalance);
//										leaveBalMastrLineSubmitId = nlapiSubmitRecord(leaveBalMastrLineObj,true,true);
//										}
									}
								}
							}
						}
					}
				}
				else {

					nlapiLogExecution('DEBUG', 'inside service period found before search else');
					var sFilta = new Array();
					sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
					sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

					var sColma = new Array();
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

					leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
					if(leaveTypeSetups)
					{
						nlapiLogExecution('DEBUG', 'inside search service period found else');
						var leaveBal = 0;
						for(var ii=0;ii<leaveTypeSetups.length;ii++)
						{

							var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
							var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

							if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {
								nlapiLogExecution('DEBUG', 'inside service period found');
								var carrFwd = 0;
								var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
								var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
								var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
								var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
								toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
								annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

//								var leaveBalMastrLineObj = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');

								nlapiLogExecution('DEBUG', '548 leaveType = ',leaveType+'@@'+'leaveType = '+leaveType+'@@'+'accrualFacotr = '+accrualFacotr);

								/** Normal Use case when calender year does not change **/
								if(nlapiStringToDate(leaveAccrualStDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveAccrualStDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
								{
									fromDate = leavBalMastrFromDate;
//									leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
									if(toBeAddedAnnually == 'T')
										leaveBal = Number(annualCompleteLeave);
									else
										leaveBal = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,latestRunAccrualDate);
									prevBalance = Number(0);
									nlapiLogExecution('DEBUG', '557 cas1 fromDate = ',fromDate+'@@'+'leaveBal = '+leaveBal);
								}
								/** Leave Calculation from prev year to next year**/
								else if(nlapiStringToDate(leaveAccrualStDate)<nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
								{
									fromDate = calendrSTMonthNew; 
									leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
									var calcLeaveBalPrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,prevYearLastDate);
									var calcLeaveBalNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,latestRunAccrualDate);
									prevBalance = Number(calcLeaveBalPrev);
									nlapiLogExecution('DEBUG', '567 cas2 fromDate = ',fromDate+'@@'+'BalPrev = '+calcLeaveBalPrev+'@@'+'BalNext = '+calcLeaveBalNext);

									if(isCarryForward=='T')
									{
										var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
										var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
										nlapiLogExecution('DEBUG', '573 cas2CFT totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);

										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave) + Number(minCarryForwd);
										else
											leaveBal = Number(calcLeaveBalNext) + Number(minCarryForwd);
									}	
									else
									{
										var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
										var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
										nlapiLogExecution('DEBUG', '581 cas2CFF totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);										

										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave);
										else
											leaveBal = Number(calcLeaveBalNext);

										carrFwd  = parseFloat(minCarryForwd).toFixed(2);
									}
								}	

								leaveBal = Number(leaveBal) + Number(leaveBalanceIfAvail);
								leaveBal = parseFloat(leaveBal).toFixed(2);
								prevBalance = parseFloat(prevBalance).toFixed(2);

//								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',lt,leaveType);	
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',lt,leaveBal);
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',lt,carrFwd);
//								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_parent',lt,leaveBalMastrSubmitId);
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',lt,prevBalance);
//								leaveBalMastrLineSubmitId = nlapiSubmitRecord(leaveBalMastrLineObj,true,true);
							}
						}
					}
					else {
						nlapiLogExecution('DEBUG', 'inside all not found before search else');

						var sFilta = new Array();
						sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

						var sColma = new Array();
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

						leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
						if(leaveTypeSetups)
						{
							nlapiLogExecution('DEBUG', 'inside search all not found else');
							var leaveBal = 0;
							for(var ii=0;ii<leaveTypeSetups.length;ii++)
							{

								var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
								var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

//								if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {
								nlapiLogExecution('DEBUG', 'inside all not found');
								var carrFwd = 0;
								var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
								var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
								var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
								var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
								toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
								annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
								nlapiLogExecution('DEBUG', 'carryForwdMax from setup', carryForwdMax);

//								var leaveBalMastrLineObj = nlapiCreateRecord('customrecord_stlms_leavebalancemasterlin');

								nlapiLogExecution('DEBUG', '640 leaveType = ',leaveType+'@@'+'leaveType = '+leaveType+'@@'+'accrualFacotr = '+accrualFacotr);

								/** Normal Use case when calender year does not change **/
								if(nlapiStringToDate(leaveAccrualStDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveAccrualStDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
								{
									fromDate = leavBalMastrFromDate;
//									leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);

									if(toBeAddedAnnually == 'T')
										leaveBal = Number(annualCompleteLeave);
									else
										leaveBal = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,latestRunAccrualDate);
									prevBalance = Number(0);
									nlapiLogExecution('DEBUG', '649 cas1 fromDate = ',fromDate+'@@'+'leaveBal = '+leaveBal);
								}
								/** Leave Calculation from prev year to next year**/
								else if(nlapiStringToDate(leaveAccrualStDate)<nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(latestRunAccrualDate)<=nlapiStringToDate(calendrENMonthNew))
								{
									fromDate = calendrSTMonthNew; 
									leaveBalMastrObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
									var calcLeaveBalPrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStDate,prevYearLastDate);
									var calcLeaveBalNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,latestRunAccrualDate);
									prevBalance = Number(calcLeaveBalPrev);
									nlapiLogExecution('DEBUG', '659 cas2 fromDate = ',fromDate+'@@'+'BalPrev = '+calcLeaveBalPrev+'@@'+'BalNext = '+calcLeaveBalNext);

									if(isCarryForward=='T')
									{
										var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
										var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
										nlapiLogExecution('DEBUG', '665 cas2CFT totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);

										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave) + Number(minCarryForwd);
										else
											leaveBal = Number(calcLeaveBalNext) + Number(minCarryForwd);
									}
									else
									{
										var totalLeavePrev = Number(calcLeaveBalPrev);// + Number(leaveBalMastrLeaveBal);
										var minCarryForwd=Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);
										nlapiLogExecution('DEBUG', '673 cas2CFF totalLeavePrev = ',totalLeavePrev+'@@'+'minCarryForwd = '+minCarryForwd+'@@'+'BalNext = '+calcLeaveBalNext);										

										if(toBeAddedAnnually == 'T')
											leaveBal = Number(annualCompleteLeave);
										else
											leaveBal = Number(calcLeaveBalNext);

										carrFwd  = parseFloat(minCarryForwd).toFixed(2);
									}
								}	

								leaveBal = Number(leaveBal) + Number(leaveBalanceIfAvail);
								leaveBal = parseFloat(leaveBal).toFixed(2);
								prevBalance = parseFloat(prevBalance).toFixed(2);

//								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',lt,leaveType);	
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',lt,leaveBal);
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',lt,carrFwd);
//								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_parent',lt,leaveBalMastrSubmitId);
								leaveBalMastrObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',lt,prevBalance);
//								leaveBalMastrLineSubmitId = nlapiSubmitRecord(leaveBalMastrLineObj,true,true);
//								}
							}
						}
					}
				}
			}

//			nlapiSubmitField('customrecord_stlms_leavebalancemaster',leaveBalMastrSubmitId,'custrecord_stlms_lbm_fromdate',fromDate);
			var submitRecord = nlapiSubmitRecord(leaveBalMastrObj);
			nlapiLogExecution('DEBUG', '698 submitRecord = ',submitRecord);

//			}
			nlapiSubmitField('employee',recId,['custentity_stlms_leaveaccrualnewjoinee','custentity_old_from_date'],['T',fromDate]);
			if(submitRecord) {

				nlapiSetRedirectURL('RECORD', 'customrecord_stlms_leavebalancemaster', submitRecord, false);
			}
		}
		else
			nlapiLogExecution('DEBUG', '708 LeaveBalance Master does not exist = ','LeaveBalance Master does not exist');

	}
	if(isEligibleForRunAccrual=='T' && isResumeOneTimeLeaveAccrl=='T' && isLeaveAccrualNewJoinee=='T')
	{
		nlapiLogExecution('DEBUG', '713 Sabbaatical Part started = ','Sabbaatical Part started');
		/** Sabbatical part starts **/
		//prevBalance

		var calendrMonths='';
		var calendStartMonth='',calendEndMonth;
		var calendrSTMonth='',fromSTMonth='',toENMonth='';

		var ToDate  = getRunPeriodicLeaveAccrDate(employeeType);
		var fromDate1 = empRecObj.getFieldValue('custentity_stlms_leaveaccrualdatescript');

		/** Get leave calender start date and leave calender enf date from leave management setup record.**/
		var setupCalederSearch = getLeaveCalenderSearch(employeeType);
		if(setupCalederSearch)
		{
			calendStartMonth = setupCalederSearch[0].getValue('custrecord_stlms_su_calendarstartdate');
			calendEndMonth = setupCalederSearch[0].getValue('custrecord_stlms_su_calendarenddate');
		}

		if(!calendStartMonth || calendStartMonth=='' || !calendEndMonth || calendEndMonth=='')
		{
			var scriptId = currContext.getScriptId();
			var errorMsg = 'Date field is empty in leave management setup';

			var errorCatcherObj = nlapiCreateRecord('customrecord_stlms_errorcatcher');
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_employeename',recId);
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
			errorCatcherObj.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

			var errorCatcherSubmitId = nlapiSubmitRecord(errorCatcherObj,true,true);
			nlapiLogExecution('DEBUG', '68 errorCatcherSubmitId = ',errorCatcherSubmitId);
			return;
		}

		var isCalendrMonthInBtwn = 'F';
		calendrSTMonth = calendStartMonth;
		fromSTMonth = fromDate1;
		toENMonth = ToDate;

		var calendrSTMonthNew = calendrSTMonth;
		var calendrENMonthNew = calendEndMonth;
		var fromSTMonthNew = fromSTMonth;
		var toENMonthNew = toENMonth;

		var sFilt = new Array();//[];
		var sColm = new Array();//[];

		sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		sFilt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename', null, 'is', recId));
		sColm.push(new nlobjSearchColumn('custrecord_stlms_lbm_fromdate'));
		sColm.push(new nlobjSearchColumn('custrecord_stlms_lbm_todate'));

		var leaveBalMasterSearch = nlapiSearchRecord('customrecord_stlms_leavebalancemaster', null, sFilt, sColm);

		if(leaveBalMasterSearch) {

			for(var i=0;i<leaveBalMasterSearch.length;i++)
			{
				var balMastrId = leaveBalMasterSearch[i].getId();
				var leaveBalMasterObj = nlapiLoadRecord('customrecord_stlms_leavebalancemaster', balMastrId);
				var leaveBalToDate = leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_todate');
				var sameYearFromDate = leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate');
				nlapiLogExecution('DEBUG', 'Leave Balance Master Record', balMastrId+' leaveBalToDate '+leaveBalToDate);
				nlapiLogExecution('DEBUG', 'From Date', leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate'));
				if(fromDate=='' || ToDate=='')
				{
					var scriptId = currContext.getScriptId();
					var errorMsg = 'Date field is empty for employee '+employeeName;

					var errorCatcherObj = nlapiCreateRecord('customrecord_stlms_errorcatcher');
					errorCatcherObj.setFieldValue('custrecord_stlms_ecr_employeename',recId);
					errorCatcherObj.setFieldValue('custrecord_stlms_ecr_error',errorMsg);
					errorCatcherObj.setFieldValue('custrecord_stlms_ecr_scriptid',scriptId);

					var errorCatcherSubmitId = nlapiSubmitRecord(errorCatcherObj,true,true);
					nlapiLogExecution('DEBUG', '68 errorCatcherSubmitId = ',errorCatcherSubmitId);
					return;
				}

				var daysBetween = 0;
				var daysBetween1 = 0;
				var daysBetween2 = 0;
				var noOfMonths = 0;

				var month = 0;
				var fromDate1 = fromSTMonth;
				fromDate1 =	fromDate1.split('/');
				var stDay = fromDate1[0];
				var stMonth = fromDate1[1];
				var stYear = fromDate1[2];
				var stRetrnDay = getDay(stMonth,stYear);

				var ToDate1 = ToDate;
				ToDate1 = ToDate1.split('/');
				var toDay = ToDate1[0];
				var toMonth = ToDate1[1];
				var toYear = ToDate1[2];
				var toRetrnDay = getDay(toMonth,toYear);

				nlapiLogExecution('DEBUG', '266 stDay = ',stDay+'@@'+'toDay = '+toDay);
				/** Calculating no of days and full months between leave accrual start date and latest run accrual ToDate**/
				if(Number(stDay)==1 && (Number(toDay)==30 || Number(toDay)==31 || Number(toDay)==28))
				{
					if(toYear>stYear)
						noOfMonths = Number(12) - Number(stMonth) + Number(toMonth) + Number(1);
					else
						noOfMonths = Number(toMonth) - Number(stMonth) + Number(1);

					nlapiLogExecution('DEBUG', '274 noOfMonths = ',noOfMonths+'@@'+'daysBetween = '+daysBetween);
				}	
				else if(stDay>1 && (toDay==30 || toDay==31)) 
				{
					if(toYear>stYear)
						noOfMonths = Number(12) - Number(stMonth) + Number(toMonth) + Number(1);
					else
						noOfMonths = Number(toMonth) - Number(stMonth);

					daysBetween = Number(stRetrnDay) - Number(stDay) + Number(1);

					nlapiLogExecution('DEBUG', '285 noOfMonths = ',noOfMonths+'@@'+'daysBetween = '+daysBetween);
				}	
				else if(stDay>1 && toDay<30) 
				{
					if(toYear>stYear)
						noOfMonths = Number(12) - Number(stMonth) + Number(toMonth) + Number(1);
					else
						noOfMonths = Number(toMonth) - Number(stMonth);
					daysBetween1 = Number(stRetrnDay) - Number(stDay) + Number(1);
					daysBetween2 = Number(toRetrnDay) - Number(toDay) + Number(1);

				}

				nlapiLogExecution('DEBUG', '266 noOfMonths = ',noOfMonths+'@@'+'daysBetween = '+daysBetween);

				var leaveBalMastrLineCount = leaveBalMasterObj.getLineItemCount('recmachcustrecord_stlms_lbline_parent');
				var normalCaseFromDate = '';
				var normalCaseFound = 'F';

				for(var lt = 0; lt < leavTypeArr.length; lt ++) {

					var empLeaveType = leavTypeArr[lt];
					nlapiLogExecution('DEBUG', 'empLeaveType', empLeaveType);

					if(jobTitle){

						nlapiLogExecution('DEBUG', 'inside job title sabbatical', jobTitle);

						var sFilta = new Array();
						sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

						var sColma = new Array();
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

						leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);

						if(leaveTypeSetups)
						{
							for(var ii=0;ii<leaveTypeSetups.length;ii++)
							{

								var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
								var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

								if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

									nlapiLogExecution('DEBUG', 'inside service period found sabbatical');
									var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
									var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
									var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
									var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
									toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
									annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

									var leaveBalance1 = noOfMonths*accrualFacotr;
									var leaveBalance2=0,leaveBalance3=0,leaveBalance4=0;
									if(daysBetween>0)
									{
										leaveBalance2 = (Number(daysBetween)/Number(stRetrnDay))*accrualFacotr;
									}

									if(daysBetween1>0 && daysBetween2>0)
									{
										leaveBalance3 = (Number(daysBetween1)/Number(stRetrnDay))*Number(accrualFacotr);
										leaveBalance4 = (Number(daysBetween2)/Number(toRetrnDay))*Number(accrualFacotr);
									}

									var calculatedLeave = Number(leaveBalance1) + Number(leaveBalance2)+ Number(leaveBalance3)+ Number(leaveBalance4);

									var oldLeaveBal = 0;
									var carriedForwdFieldVal = 0;
									for(var iii=1;iii<=leaveBalMastrLineCount;iii++)
									{
										var leaveBalMastrLineRecId = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','id',iii);
										var leaveBalMastrLeaveBal = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii);
										var leaveBalMastrLeavType = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',iii);
										var leaveBalCarriedForwd = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii);
										var leaveBalPrevYear = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii);

										/** Compare setup leave types with leave balance line master leave types to calculate leave balance only for matched leave types**/
										if(leaveType==leaveBalMastrLeavType)
										{
											nlapiLogExecution('DEBUG', '923 calendrSTMonthNew = ',calendrSTMonthNew+'@@'+'calendrENMonthNew = '+calendrENMonthNew);
											nlapiLogExecution('DEBUG', '924 leaveAccrualStart = ',fromSTMonthNew+'@@'+'latestRunAccrual = '+toENMonthNew+'@@'+'leaveBalToDate = '+leaveBalToDate);

											/** Sabbatical leave in same year **/
											if(nlapiStringToDate(leaveBalToDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveBalToDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(fromSTMonthNew)<=nlapiStringToDate(calendrENMonthNew))
											{
												prevBalance = Number(leaveBalPrevYear);
												nlapiLogExecution('DEBUG', '931 case1 leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'calculatedLeave = '+calculatedLeave);
												normalCaseFound='T';
												normalCaseFromDate = sameYearFromDate;//leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate');//fromSTMonth;

												if(toBeAddedAnnually == 'T')
													leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal);
												else
													leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal)+Number(calculatedLeave);

												oldLeaveBal = Number(leaveBalCarriedForwd);
											}
											/** Sabbatical leave in previous and in next year **/
											else if(nlapiStringToDate(fromSTMonthNew)<nlapiStringToDate(calendrSTMonthNew))
											{
												prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);

												var leaveAccrualStart = fromSTMonth;
												var newYearStartDate = calendrSTMonthNew;
												var prevYearLastDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));
												var nextYearLastDate = ToDate;
												var calcLeavePrev, calcLeaveNext;

												if(toBeAddedAnnually == 'T') {

													/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of the new calender year upto latest run accrual.**/
													calcLeavePrev = Number(prevBalance);//getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
													calcLeaveNext = Number(accrualFacotr);//getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);

													prevBalance = Number(prevBalance);

												}

												else {

													/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of the new calender year upto latest run accrual.**/
													calcLeavePrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
													calcLeaveNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);

													prevBalance = Number(prevBalance) + Number(calcLeavePrev);
												}
												/** From Date is new calender year start date.**/
												fromDate = calendStartMonth;

												/** Perform carry forward logic **/
												if(isCarryForward=='T')
												{
													nlapiLogExecution('DEBUG', '916 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext+'@@'+'carryForwdMax = '+carryForwdMax);

													var totalLeavePrev = Number(0);

													if(toBeAddedAnnually == 'T') 
														totalLeavePrev = Number(calcLeavePrev);
													else 
														totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

													nlapiLogExecution('DEBUG', '972 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
													var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

													leaveBalMastrLeaveBal = Number(calcLeaveNext) + Number(minCarryForwd);
													oldLeaveBal = Number(leaveBalCarriedForwd);

												}	
												else
												{
													var totalLeavePrev = Number(0);

													if(toBeAddedAnnually == 'T') 
														totalLeavePrev = Number(calcLeavePrev);
													else 
														totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

													nlapiLogExecution('DEBUG', '992 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
													var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

													leaveBalMastrLeaveBal = Number(calcLeaveNext);
													oldLeaveBal = Number(minCarryForwd);

												}
											}
											/** Sabbatical Leave in Next Year only **/
											else if(nlapiStringToDate(fromSTMonthNew)>=nlapiStringToDate(calendrSTMonthNew))
											{

												prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);

												/** From Date is new calender year start date.**/
												fromDate = calendStartMonth;

												if(isCarryForward=='T')
												{
													nlapiLogExecution('DEBUG', 'case3 1013 CFT leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'carryForwdMax= '+carryForwdMax);

													var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG','case3 1017 CFT calculatedLeave= ',calculatedLeave+'@@'+'minCarryForwd= '+minCarryForwd);

													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(accrualFacotr) + Number(minCarryForwd);
													else
														leaveBalMastrLeaveBal = Number(calculatedLeave) + Number(minCarryForwd);

													oldLeaveBal = Number(leaveBalCarriedForwd);
												}
												else
												{
													//Commented by new update by rutika on 12-May
													//var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													oldLeaveBal = Number(minCarryForwd);// + Number(carryForwdMax);

													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(accrualFacotr);
													else
														leaveBalMastrLeaveBal = Number(calculatedLeave);

													nlapiLogExecution('DEBUG', 'case3 986 CFF oldLeaveBal = ',oldLeaveBal+'@@'+'leaveBalMastrLeaveBal= '+leaveBalMastrLeaveBal);

												}
											}

											oldLeaveBal = parseFloat(oldLeaveBal).toFixed(2);
											leaveBalMastrLeaveBal = parseFloat(leaveBalMastrLeaveBal).toFixed(2);
											prevBalance = parseFloat(prevBalance).toFixed(2);

											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii,leaveBalMastrLeaveBal);
											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii,oldLeaveBal);
											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii,prevBalance);
										}
									}
								}
							}	
						}
						else {

							nlapiLogExecution('DEBUG', 'inside service period found before search sabbatical');
							var sFilta = new Array();
							sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

							var sColma = new Array();
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

							leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);

							if(leaveTypeSetups) {

								for(var ii=0;ii<leaveTypeSetups.length;ii++)
								{
									var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
									var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

									if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {

										nlapiLogExecution('DEBUG', 'inside service period found sabbatical');
										var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
										var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
										var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
										var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
										toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
										annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

										var leaveBalance1 = noOfMonths*accrualFacotr;
										var leaveBalance2=0,leaveBalance3=0,leaveBalance4=0;
										if(daysBetween>0)
										{
											leaveBalance2 = (Number(daysBetween)/Number(stRetrnDay))*accrualFacotr;
										}

										if(daysBetween1>0 && daysBetween2>0)
										{
											leaveBalance3 = (Number(daysBetween1)/Number(stRetrnDay))*Number(accrualFacotr);
											leaveBalance4 = (Number(daysBetween2)/Number(toRetrnDay))*Number(accrualFacotr);
										}

										var calculatedLeave = Number(leaveBalance1) + Number(leaveBalance2)+ Number(leaveBalance3)+ Number(leaveBalance4);

										var oldLeaveBal = 0;
										var carriedForwdFieldVal = 0;
										for(var iii=1;iii<=leaveBalMastrLineCount;iii++)
										{
											var leaveBalMastrLineRecId = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','id',iii);
											var leaveBalMastrLeaveBal = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii);
											var leaveBalMastrLeavType = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',iii);
											var leaveBalCarriedForwd = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii);
											var leaveBalPrevYear = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii);

											/** Compare setup leave types with leave balance line master leave types to calculate leave balance only for matched leave types**/
											if(leaveType==leaveBalMastrLeavType)
											{
												nlapiLogExecution('DEBUG', '1064 calendrSTMonthNew = ',calendrSTMonthNew+'@@'+'calendrENMonthNew = '+calendrENMonthNew);
												nlapiLogExecution('DEBUG', '1065 leaveAccrualStart = ',fromSTMonthNew+'@@'+'latestRunAccrual = '+toENMonthNew+'@@'+'leaveBalToDate = '+leaveBalToDate);
//												var normalCaseFromDate = '';
//												var normalCaseFound = 'F';
												/** Sabbatical leave in same year **/
												if(nlapiStringToDate(leaveBalToDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveBalToDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(fromSTMonthNew)<=nlapiStringToDate(calendrENMonthNew))
												{

													prevBalance = Number(leaveBalPrevYear);
													nlapiLogExecution('DEBUG', '1071 case1 leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'calculatedLeave = '+calculatedLeave);
													normalCaseFound='T';
													normalCaseFromDate = sameYearFromDate;//leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate');//fromSTMonth;
													nlapiLogExecution('DEBUG', 'From Date', normalCaseFromDate+' '+normalCaseFound);

													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal);
													else
														leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal)+Number(calculatedLeave);

													oldLeaveBal = Number(leaveBalCarriedForwd);
												}
												/** Sabbatical leave in previous and in next year **/
												else if(nlapiStringToDate(fromSTMonthNew)<nlapiStringToDate(calendrSTMonthNew))
												{

													prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);
													var leaveAccrualStart = fromSTMonth;
													var newYearStartDate = calendrSTMonthNew;
													var prevYearLastDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));
													var nextYearLastDate = ToDate;
													var calcLeavePrev, calcLeaveNext;

													if(toBeAddedAnnually == 'T') {

														/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of the new calender year upto latest run accrual.**/
														calcLeavePrev = Number(prevBalance);//getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
														calcLeaveNext = Number(accrualFacotr);//getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);

														prevBalance = Number(prevBalance);

													}

													else {
														/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of 
									the new calender year upto latest run accrual.**/
														calcLeavePrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
														calcLeaveNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);
														prevBalance = Number(prevBalance) + Numer(calcLeavePrev);
													}
													/** From Date is new calender year start date.**/
													fromDate = calendStartMonth;

													/** Perform carry forward logic **/
													if(isCarryForward=='T')
													{
														nlapiLogExecution('DEBUG', '1096 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext+'@@'+'carryForwdMax = '+carryForwdMax);

														var totalLeavePrev = Number(0);

														if(toBeAddedAnnually == 'T') 
															totalLeavePrev = Number(calcLeavePrev);
														else
															totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

														nlapiLogExecution('DEBUG', '1109 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
														var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

														nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

														leaveBalMastrLeaveBal = Number(calcLeaveNext) + Number(minCarryForwd);
														oldLeaveBal = Number(leaveBalCarriedForwd);

													}	
													else
													{
														
														var totalLeavePrev = Number(0);

														if(toBeAddedAnnually == 'T') 
															totalLeavePrev = Number(calcLeavePrev);
														else
															totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

														nlapiLogExecution('DEBUG', '1129 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
														var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

														nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

														leaveBalMastrLeaveBal = Number(calcLeaveNext);
														oldLeaveBal = Number(minCarryForwd);

													}
												}
												/** Sabbatical Leave in Next Year only **/
												else if(nlapiStringToDate(fromSTMonthNew)>=nlapiStringToDate(calendrSTMonthNew))
												{
													/** From Date is new calender year start date.**/
													fromDate = calendStartMonth;
													prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);

													if(isCarryForward=='T')
													{
														nlapiLogExecution('DEBUG', 'case3 1147 CFT leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'carryForwdMax= '+carryForwdMax);

														var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

														nlapiLogExecution('DEBUG','case3 1151 CFT calculatedLeave= ',calculatedLeave+'@@'+'minCarryForwd= '+minCarryForwd);

														if(toBeAddedAnnually == 'T')
															leaveBalMastrLeaveBal = Number(accrualFacotr) + Number(minCarryForwd);
														else
															leaveBalMastrLeaveBal = Number(calculatedLeave) + Number(minCarryForwd);

														oldLeaveBal = Number(leaveBalCarriedForwd);
													}	
													else
													{
														//Commented by new update by rutika on 12-May
														//var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

														var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

														oldLeaveBal = Number(minCarryForwd);// + Number(carryForwdMax);
														if(toBeAddedAnnually == 'T')
															leaveBalMastrLeaveBal = Number(accrualFacotr);
														else
															leaveBalMastrLeaveBal = Number(calculatedLeave);

														nlapiLogExecution('DEBUG', 'case3 1166 CFF oldLeaveBal = ',oldLeaveBal+'@@'+'leaveBalMastrLeaveBal= '+leaveBalMastrLeaveBal);

													}
												}

												oldLeaveBal = parseFloat(oldLeaveBal).toFixed(2);
												leaveBalMastrLeaveBal = parseFloat(leaveBalMastrLeaveBal).toFixed(2);
												prevBalance = parseFloat(prevBalance).toFixed(2);

												leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii,leaveBalMastrLeaveBal);
												leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii,oldLeaveBal);
												leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii,prevBalance);
											}
										}
									}
								}	
							}
							else {

								nlapiLogExecution('DEBUG', 'inside job title found before search sabbatical');

								var sFilta = new Array();
								sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'is', empLeaveType));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
								sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

								var sColma = new Array();
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
								sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

								leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);

								if(leaveTypeSetups) {

									for(var ii=0;ii<leaveTypeSetups.length;ii++)
									{
										var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
										var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
										var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
										var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
										toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
										annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

										var leaveBalance1 = noOfMonths*accrualFacotr;
										var leaveBalance2=0,leaveBalance3=0,leaveBalance4=0;
										if(daysBetween>0)
										{
											leaveBalance2 = (Number(daysBetween)/Number(stRetrnDay))*accrualFacotr;
										}

										if(daysBetween1>0 && daysBetween2>0)
										{
											leaveBalance3 = (Number(daysBetween1)/Number(stRetrnDay))*Number(accrualFacotr);
											leaveBalance4 = (Number(daysBetween2)/Number(toRetrnDay))*Number(accrualFacotr);
										}

										var calculatedLeave = Number(leaveBalance1) + Number(leaveBalance2)+ Number(leaveBalance3)+ Number(leaveBalance4);

										var oldLeaveBal = 0;
										var carriedForwdFieldVal = 0;
										for(var iii=1;iii<=leaveBalMastrLineCount;iii++)
										{
											var leaveBalMastrLineRecId = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','id',iii);
											var leaveBalMastrLeaveBal = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii);
											var leaveBalMastrLeavType = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',iii);
											var leaveBalCarriedForwd = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii);
											var leaveBalPrevYear = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii);

											/** Compare setup leave types with leave balance line master leave types to calculate leave balance only for matched leave types**/
											if(leaveType==leaveBalMastrLeavType)
											{
												nlapiLogExecution('DEBUG', '1239 calendrSTMonthNew = ',calendrSTMonthNew+'@@'+'calendrENMonthNew = '+calendrENMonthNew);
												nlapiLogExecution('DEBUG', '1240 leaveAccrualStart = ',fromSTMonthNew+'@@'+'latestRunAccrual = '+toENMonthNew+'@@'+'leaveBalToDate = '+leaveBalToDate);
//												var normalCaseFromDate = '';
//												var normalCaseFound = 'F';
												/** Sabbatical leave in same year **/
												if(nlapiStringToDate(leaveBalToDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveBalToDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(fromSTMonthNew)<=nlapiStringToDate(calendrENMonthNew))
												{

													prevBalance = Number(leaveBalPrevYear);
													nlapiLogExecution('DEBUG', '1246 case1 leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'calculatedLeave = '+calculatedLeave);
													normalCaseFound='T';
													normalCaseFromDate = sameYearFromDate;//leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate');//fromSTMonth;
													nlapiLogExecution('DEBUG', 'From Date', normalCaseFromDate+' '+normalCaseFound);

													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal);
													else
														leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal)+Number(calculatedLeave);

													oldLeaveBal = Number(leaveBalCarriedForwd);
												}
												/** Sabbatical leave in previous and in next year **/
												else if(nlapiStringToDate(fromSTMonthNew)<nlapiStringToDate(calendrSTMonthNew))
												{

													prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);
													var leaveAccrualStart = fromSTMonth;
													var newYearStartDate = calendrSTMonthNew;
													var prevYearLastDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));
													var nextYearLastDate = ToDate;

													var calcLeavePrev, calcLeaveNext;

													if(toBeAddedAnnually == 'T') {

														/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of the new calender year upto latest run accrual.**/
														calcLeavePrev = Number(prevBalance);//getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
														calcLeaveNext = Number(accrualFacotr);//getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);

														prevBalance = Number(prevBalance);

													}

													else {
													/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of 
										the new calender year upto latest run accrual.**/
													calcLeavePrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
													calcLeaveNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);
													prevBalance = Number(prevBalance) + Number(calcLeavePrev);
													}
													
													/** From Date is new calender year start date.**/
													fromDate = calendStartMonth;

													/** Perform carry forward logic **/
													if(isCarryForward=='T')
													{
														nlapiLogExecution('DEBUG', '1271 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext+'@@'+'carryForwdMax = '+carryForwdMax);
														
														var totalLeavePrev = Number(0);

														if(toBeAddedAnnually == 'T') 
															totalLeavePrev = Number(calcLeavePrev);
														else
															totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

														nlapiLogExecution('DEBUG', '1284 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
														var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

														nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

														leaveBalMastrLeaveBal = Number(calcLeaveNext) + Number(minCarryForwd);
														oldLeaveBal = Number(leaveBalCarriedForwd);

													}	
													else
													{
														
														var totalLeavePrev = Number(0);

														if(toBeAddedAnnually == 'T') 
															totalLeavePrev = Number(calcLeavePrev);
														else
															totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

														nlapiLogExecution('DEBUG', '1304 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
														var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

														nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

														leaveBalMastrLeaveBal = Number(calcLeaveNext);
														oldLeaveBal = Number(minCarryForwd);

													}
												}
												/** Sabbatical Leave in Next Year only **/
												else if(nlapiStringToDate(fromSTMonthNew)>=nlapiStringToDate(calendrSTMonthNew))
												{

													prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);
													/** From Date is new calender year start date.**/
													fromDate = calendStartMonth;

													if(isCarryForward=='T')
													{
														nlapiLogExecution('DEBUG', 'case3 1322 CFT leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'carryForwdMax= '+carryForwdMax);

														var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

														nlapiLogExecution('DEBUG','case3 1326 CFT calculatedLeave= ',calculatedLeave+'@@'+'minCarryForwd= '+minCarryForwd);

														if(toBeAddedAnnually == 'T')
															leaveBalMastrLeaveBal = Number(accrualFacotr) + Number(minCarryForwd);
														else
															leaveBalMastrLeaveBal = Number(calculatedLeave) + Number(minCarryForwd);

														oldLeaveBal = Number(leaveBalCarriedForwd);
													}	
													else
													{
														//Commented by new update by rutika on 12-May
														//var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

														var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

														oldLeaveBal = Number(minCarryForwd);// + Number(carryForwdMax);
														if(toBeAddedAnnually == 'T')
															leaveBalMastrLeaveBal = Number(accrualFacotr);
														else
															leaveBalMastrLeaveBal = Number(calculatedLeave);

														nlapiLogExecution('DEBUG', 'case3 1341 CFF oldLeaveBal = ',oldLeaveBal+'@@'+'leaveBalMastrLeaveBal= '+leaveBalMastrLeaveBal);

													}
												}

												oldLeaveBal = parseFloat(oldLeaveBal).toFixed(2);
												leaveBalMastrLeaveBal = parseFloat(leaveBalMastrLeaveBal).toFixed(2);
												prevBalance = parseFloat(prevBalance).toFixed(2);

												leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii,leaveBalMastrLeaveBal);
												leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii,oldLeaveBal);
												leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii,prevBalance);
											}
										}
									}	
								}
								else {

									nlapiLogExecution('DEBUG', 'inside all not found before search sabbatical');

									var sFilta = new Array();
									sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
									sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
									sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
									sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
									sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
									sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

									var sColma = new Array();
									sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
									sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
									sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
									sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
									sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
									sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
									sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

									leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
									if(leaveTypeSetups) {

										for(var ii=0;ii<leaveTypeSetups.length;ii++)
										{
											var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
											var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
											var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
											var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
											toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
											annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

											var leaveBalance1 = noOfMonths*accrualFacotr;
											var leaveBalance2=0,leaveBalance3=0,leaveBalance4=0;
											if(daysBetween>0)
											{
												leaveBalance2 = (Number(daysBetween)/Number(stRetrnDay))*accrualFacotr;
											}

											if(daysBetween1>0 && daysBetween2>0)
											{
												leaveBalance3 = (Number(daysBetween1)/Number(stRetrnDay))*Number(accrualFacotr);
												leaveBalance4 = (Number(daysBetween2)/Number(toRetrnDay))*Number(accrualFacotr);
											}

											var calculatedLeave = Number(leaveBalance1) + Number(leaveBalance2)+ Number(leaveBalance3)+ Number(leaveBalance4);

											var oldLeaveBal = 0;
											var carriedForwdFieldVal = 0;
											for(var iii=1;iii<=leaveBalMastrLineCount;iii++)
											{
												var leaveBalMastrLineRecId = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','id',iii);
												var leaveBalMastrLeaveBal = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii);
												var leaveBalMastrLeavType = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',iii);
												var leaveBalCarriedForwd = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii);
												var leaveBalPrevYear = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii);

												/** Compare setup leave types with leave balance line master leave types to calculate leave balance only for matched leave types**/
												if(leaveType==leaveBalMastrLeavType)
												{
													nlapiLogExecution('DEBUG', '1412 calendrSTMonthNew = ',calendrSTMonthNew+'@@'+'calendrENMonthNew = '+calendrENMonthNew);
													nlapiLogExecution('DEBUG', '1413 leaveAccrualStart = ',fromSTMonthNew+'@@'+'latestRunAccrual = '+toENMonthNew+'@@'+'leaveBalToDate = '+leaveBalToDate);
//													var normalCaseFromDate = '';
//													var normalCaseFound = 'F';
													/** Sabbatical leave in same year **/
													if(nlapiStringToDate(leaveBalToDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveBalToDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(fromSTMonthNew)<=nlapiStringToDate(calendrENMonthNew))
													{

														prevBalance = Number(leaveBalPrevYear);
														nlapiLogExecution('DEBUG', '1419 case1 leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'calculatedLeave = '+calculatedLeave);
														normalCaseFound='T';
														normalCaseFromDate = sameYearFromDate;//leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate');//fromSTMonth;
														nlapiLogExecution('DEBUG', 'From Date', normalCaseFromDate+' '+normalCaseFound);

														if(toBeAddedAnnually == 'T')
															leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal);
														else
															leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal)+Number(calculatedLeave);

														oldLeaveBal = Number(leaveBalCarriedForwd);
													}
													/** Sabbatical leave in previous and in next year **/
													else if(nlapiStringToDate(fromSTMonthNew)<nlapiStringToDate(calendrSTMonthNew))
													{

														prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);

														var leaveAccrualStart = fromSTMonth;
														var newYearStartDate = calendrSTMonthNew;
														var prevYearLastDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));
														var nextYearLastDate = ToDate;

														var calcLeavePrev, calcLeaveNext;

														if(toBeAddedAnnually == 'T') {

															/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of the new calender year upto latest run accrual.**/
															calcLeavePrev = Number(prevBalance);//getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
															calcLeaveNext = Number(accrualFacotr);//getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);

															prevBalance = Number(prevBalance);

														}

														else {
														/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of 
											the new calender year upto latest run accrual.**/
														calcLeavePrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
														calcLeaveNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);
														prevBalance = Number(prevBalance) + Number(calcLeavePrev);
														}
														/** From Date is new calender year start date.**/
														fromDate = calendStartMonth;

														/** Perform carry forward logic **/
														if(isCarryForward=='T')
														{
															nlapiLogExecution('DEBUG', '1444 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext+'@@'+'carryForwdMax = '+carryForwdMax);

															var totalLeavePrev = Number(0);

															if(toBeAddedAnnually == 'T') 
																totalLeavePrev = Number(calcLeavePrev);
															else
																totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

															nlapiLogExecution('DEBUG', '1457 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
															var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

															nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

															leaveBalMastrLeaveBal = Number(calcLeaveNext) + Number(minCarryForwd);
															oldLeaveBal = Number(leaveBalCarriedForwd);

														}	
														else
														{
															
															var totalLeavePrev = Number(0);

															if(toBeAddedAnnually == 'T') 
																totalLeavePrev = Number(calcLeavePrev);
															else
																totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

															nlapiLogExecution('DEBUG', '1477 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
															var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

															nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

															leaveBalMastrLeaveBal = Number(calcLeaveNext);
															oldLeaveBal = Number(minCarryForwd);

														}
													}
													/** Sabbatical Leave in Next Year only **/
													else if(nlapiStringToDate(fromSTMonthNew)>=nlapiStringToDate(calendrSTMonthNew))
													{

														prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);
														/** From Date is new calender year start date.**/
														fromDate = calendStartMonth;

														if(isCarryForward=='T')
														{
															nlapiLogExecution('DEBUG', 'case3 1495 CFT leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'carryForwdMax= '+carryForwdMax);

															var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

															nlapiLogExecution('DEBUG','case3 1499 CFT calculatedLeave= ',calculatedLeave+'@@'+'minCarryForwd= '+minCarryForwd);

															if(toBeAddedAnnually == 'T')
																leaveBalMastrLeaveBal = Number(accrualFacotr) + Number(minCarryForwd);
															else
																leaveBalMastrLeaveBal = Number(calculatedLeave) + Number(minCarryForwd);

															oldLeaveBal = Number(leaveBalCarriedForwd);
														}	
														else
														{
															//Commented by new update by rutika on 12-May
															//var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

															var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

															oldLeaveBal = Number(minCarryForwd);// + Number(carryForwdMax);
															if(toBeAddedAnnually == 'T')
																leaveBalMastrLeaveBal = Number(accrualFacotr);
															else
																leaveBalMastrLeaveBal = Number(calculatedLeave);

															nlapiLogExecution('DEBUG', 'case3 1514 CFF oldLeaveBal = ',oldLeaveBal+'@@'+'leaveBalMastrLeaveBal= '+leaveBalMastrLeaveBal);

														}
													}

													oldLeaveBal = parseFloat(oldLeaveBal).toFixed(2);
													leaveBalMastrLeaveBal = parseFloat(leaveBalMastrLeaveBal).toFixed(2);
													prevBalance = parseFloat(prevBalance).toFixed(2);

													leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii,leaveBalMastrLeaveBal);
													leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii,oldLeaveBal);
													leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii,prevBalance);
												}
											}
										}	
									}
								}
							}
						}
					}
					else {

						nlapiLogExecution('DEBUG', 'inside service period found before search else sabbatical');
						var sFilta = new Array();
						sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
						sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

						var sColma = new Array();
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

						leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
						if(leaveTypeSetups)
						{
							nlapiLogExecution('DEBUG', 'inside search service period found else');
							var leaveBal = 0;
							for(var ii=0;ii<leaveTypeSetups.length;ii++)
							{

								var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
								var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');

								if(Number(minexprnc) <= Number(servicePeriod) && Number(maxexprnc) >= Number(servicePeriod)) {
									var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
									var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
									var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
									var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
									toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
									annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

									var leaveBalance1 = noOfMonths*accrualFacotr;
									var leaveBalance2=0,leaveBalance3=0,leaveBalance4=0;
									if(daysBetween>0)
									{
										leaveBalance2 = (Number(daysBetween)/Number(stRetrnDay))*accrualFacotr;
									}

									if(daysBetween1>0 && daysBetween2>0)
									{
										leaveBalance3 = (Number(daysBetween1)/Number(stRetrnDay))*Number(accrualFacotr);
										leaveBalance4 = (Number(daysBetween2)/Number(toRetrnDay))*Number(accrualFacotr);
									}

									var calculatedLeave = Number(leaveBalance1) + Number(leaveBalance2)+ Number(leaveBalance3)+ Number(leaveBalance4);

									var oldLeaveBal = 0;
									var carriedForwdFieldVal = 0;
									for(var iii=1;iii<=leaveBalMastrLineCount;iii++)
									{
										var leaveBalMastrLineRecId = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','id',iii);
										var leaveBalMastrLeaveBal = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii);
										var leaveBalMastrLeavType = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',iii);
										var leaveBalCarriedForwd = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii);
										var leaveBalPrevYear = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii);

										/** Compare setup leave types with leave balance line master leave types to calculate leave balance only for matched leave types**/
										if(leaveType==leaveBalMastrLeavType)
										{
											nlapiLogExecution('DEBUG', '1595 calendrSTMonthNew = ',calendrSTMonthNew+'@@'+'calendrENMonthNew = '+calendrENMonthNew);
											nlapiLogExecution('DEBUG', '1596 leaveAccrualStart = ',fromSTMonthNew+'@@'+'latestRunAccrual = '+toENMonthNew+'@@'+'leaveBalToDate = '+leaveBalToDate);
//											var normalCaseFromDate = '';
//											var normalCaseFound = 'F';
											/** Sabbatical leave in same year **/
											if(nlapiStringToDate(leaveBalToDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveBalToDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(fromSTMonthNew)<=nlapiStringToDate(calendrENMonthNew))
											{

												prevBalance = Number(leaveBalPrevYear);
												nlapiLogExecution('DEBUG', '1602 case1 leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'calculatedLeave = '+calculatedLeave);
												normalCaseFound='T';
												normalCaseFromDate = sameYearFromDate;//leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate');//fromSTMonth;
												nlapiLogExecution('DEBUG', 'From Date', normalCaseFromDate+' '+normalCaseFound);

												if(toBeAddedAnnually == 'T')
													leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal);
												else
													leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal)+Number(calculatedLeave);

												oldLeaveBal = Number(leaveBalCarriedForwd);
											}
											/** Sabbatical leave in previous and in next year **/
											else if(nlapiStringToDate(fromSTMonthNew)<nlapiStringToDate(calendrSTMonthNew))
											{

												prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);

												var leaveAccrualStart = fromSTMonth;
												var newYearStartDate = calendrSTMonthNew;
												var prevYearLastDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));
												var nextYearLastDate = ToDate;

												var calcLeavePrev, calcLeaveNext;

												if(toBeAddedAnnually == 'T') {

													/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of the new calender year upto latest run accrual.**/
													calcLeavePrev = Number(prevBalance);//getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
													calcLeaveNext = Number(accrualFacotr);//getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);

													prevBalance = Number(prevBalance);

												}

												else {
												/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of 
									the new calender year upto latest run accrual.**/
												calcLeavePrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
												calcLeaveNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);
												prevBalance = Number(prevBalance) + Number(calcLeavePrev);
												}
												/** From Date is new calender year start date.**/
												fromDate = calendStartMonth;

												/** Perform carry forward logic **/
												if(isCarryForward=='T')
												{
													nlapiLogExecution('DEBUG', '1627 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext+'@@'+'carryForwdMax = '+carryForwdMax);

													var totalLeavePrev = Number(0);

													if(toBeAddedAnnually == 'T') 
														totalLeavePrev = Number(calcLeavePrev);
													else
														totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

													nlapiLogExecution('DEBUG', '1640 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
													var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

													leaveBalMastrLeaveBal = Number(calcLeaveNext) + Number(minCarryForwd);
													oldLeaveBal = Number(leaveBalCarriedForwd);

												}	
												else
												{
													
													var totalLeavePrev = Number(0);

													if(toBeAddedAnnually == 'T') 
														totalLeavePrev = Number(calcLeavePrev);
													else
														totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

													nlapiLogExecution('DEBUG', '1660 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
													var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

													leaveBalMastrLeaveBal = Number(calcLeaveNext);
													oldLeaveBal = Number(minCarryForwd);

												}
											}
											/** Sabbatical Leave in Next Year only **/
											else if(nlapiStringToDate(fromSTMonthNew)>=nlapiStringToDate(calendrSTMonthNew))
											{

												prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);

												/** From Date is new calender year start date.**/
												fromDate = calendStartMonth;

												if(isCarryForward=='T')
												{
													nlapiLogExecution('DEBUG', 'case3 1678 CFT leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'carryForwdMax= '+carryForwdMax);

													var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG','case3 1682 CFT calculatedLeave= ',calculatedLeave+'@@'+'minCarryForwd= '+minCarryForwd);

													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(accrualFacotr) + Number(minCarryForwd);
													else
														leaveBalMastrLeaveBal = Number(calculatedLeave) + Number(minCarryForwd);

													oldLeaveBal = Number(leaveBalCarriedForwd);
												}	
												else
												{
													//Commented by new update by rutika on 12-May
													//var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													oldLeaveBal = Number(minCarryForwd);// + Number(carryForwdMax);
													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(accrualFacotr);
													else
														leaveBalMastrLeaveBal = Number(calculatedLeave);

													nlapiLogExecution('DEBUG', 'case3 1697 CFF oldLeaveBal = ',oldLeaveBal+'@@'+'leaveBalMastrLeaveBal= '+leaveBalMastrLeaveBal);

												}
											}

											oldLeaveBal = parseFloat(oldLeaveBal).toFixed(2);
											leaveBalMastrLeaveBal = parseFloat(leaveBalMastrLeaveBal).toFixed(2);
											prevBalance = parseFloat(prevBalance).toFixed(2);

											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii,leaveBalMastrLeaveBal);
											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii,oldLeaveBal);
											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii,prevBalance);
										}
									}
								}
							}
						}
						else {
							nlapiLogExecution('DEBUG', 'inside all not found before search else sabbatical');

							var sFilta = new Array();
							sFilta.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
							sFilta.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

							var sColma = new Array();
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmin'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_expmax'));
							sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

							leaveTypeSetups = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilta, sColma);
							if(leaveTypeSetups)
							{
								nlapiLogExecution('DEBUG', 'inside search all not found else sabbatical');
								var leaveBal = 0;
								for(var ii=0;ii<leaveTypeSetups.length;ii++)
								{

									var minexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmin');
									var maxexprnc = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_expmax');
									var accrualFacotr = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');
									var leaveType = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavetype');
									var isCarryForward = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_carryforward');
									var carryForwdMax = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_leavescarriedfo');
									toBeAddedAnnually = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_appliedannually');
									annualCompleteLeave = leaveTypeSetups[ii].getValue('custrecord_stlms_ltsetup_accrualfactor');

									var leaveBalance1 = noOfMonths*accrualFacotr;
									var leaveBalance2=0,leaveBalance3=0,leaveBalance4=0;
									if(daysBetween>0)
									{
										leaveBalance2 = (Number(daysBetween)/Number(stRetrnDay))*accrualFacotr;
									}

									if(daysBetween1>0 && daysBetween2>0)
									{
										leaveBalance3 = (Number(daysBetween1)/Number(stRetrnDay))*Number(accrualFacotr);
										leaveBalance4 = (Number(daysBetween2)/Number(toRetrnDay))*Number(accrualFacotr);
									}

									var calculatedLeave = Number(leaveBalance1) + Number(leaveBalance2)+ Number(leaveBalance3)+ Number(leaveBalance4);

									var oldLeaveBal = 0;
									var carriedForwdFieldVal = 0;
									for(var iii=1;iii<=leaveBalMastrLineCount;iii++)
									{
										var leaveBalMastrLineRecId = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','id',iii);
										var leaveBalMastrLeaveBal = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii);
										var leaveBalMastrLeavType = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavetype',iii);
										var leaveBalCarriedForwd = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii);
										var leaveBalPrevYear = leaveBalMasterObj.getLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii);

										/** Compare setup leave types with leave balance line master leave types to calculate leave balance only for matched leave types**/
										if(leaveType==leaveBalMastrLeavType)
										{
											nlapiLogExecution('DEBUG', '1773 calendrSTMonthNew = ',calendrSTMonthNew+'@@'+'calendrENMonthNew = '+calendrENMonthNew);
											nlapiLogExecution('DEBUG', '1774 leaveAccrualStart = ',fromSTMonthNew+'@@'+'latestRunAccrual = '+toENMonthNew+'@@'+'leaveBalToDate = '+leaveBalToDate);

											/** Sabbatical leave in same year **/
											if(nlapiStringToDate(leaveBalToDate)>=nlapiStringToDate(calendrSTMonthNew) && nlapiStringToDate(leaveBalToDate)<=nlapiStringToDate(calendrENMonthNew) && nlapiStringToDate(fromSTMonthNew)<=nlapiStringToDate(calendrENMonthNew))
											{

												prevBalance = Number(leaveBalPrevYear);
												nlapiLogExecution('DEBUG', '1780 case1 leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'calculatedLeave = '+calculatedLeave);
												normalCaseFound='T';
												normalCaseFromDate = sameYearFromDate;//leaveBalMasterSearch[i].getValue('custrecord_stlms_lbm_fromdate');//fromSTMonth;
												nlapiLogExecution('DEBUG', 'From Date', normalCaseFromDate+' '+normalCaseFound);

												if(toBeAddedAnnually == 'T')
													leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal);
												else
													leaveBalMastrLeaveBal = Number(leaveBalMastrLeaveBal)+Number(calculatedLeave);

												oldLeaveBal = Number(leaveBalCarriedForwd);
											}
											/** Sabbatical leave in previous and in next year **/
											else if(nlapiStringToDate(fromSTMonthNew)<nlapiStringToDate(calendrSTMonthNew))
											{

												prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);

												var leaveAccrualStart = fromSTMonth;
												var newYearStartDate = calendrSTMonthNew;
												var prevYearLastDate = nlapiDateToString(nlapiAddDays(new Date(newYearStartDate),-1));
												var nextYearLastDate = ToDate;

												var calcLeavePrev, calcLeaveNext;

												if(toBeAddedAnnually == 'T') {

													/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of the new calender year upto latest run accrual.**/
													calcLeavePrev = Number(prevBalance);//getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
													calcLeaveNext = Number(accrualFacotr);//getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);

													prevBalance = Number(prevBalance);

												}

												else {
												/**Double calculation part. First calculate upto end of prev calender year and then calculate from start of 
									the new calender year upto latest run accrual.**/
												calcLeavePrev = getCalculatedLeaves(accrualFacotr,leaveAccrualStart,prevYearLastDate);
												calcLeaveNext = getCalculatedLeaves(accrualFacotr,calendrSTMonthNew,nextYearLastDate);
												prevBalance = Number(prevBalance) + Number(calcLeavePrev);
												}

												/** From Date is new calender year start date.**/
												fromDate = calendStartMonth;

												/** Perform carry forward logic **/
												if(isCarryForward=='T')
												{
													nlapiLogExecution('DEBUG', '1805 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext+'@@'+'carryForwdMax = '+carryForwdMax);

													var totalLeavePrev = Number(0);

													if(toBeAddedAnnually == 'T') 
														totalLeavePrev = Number(calcLeavePrev);
													else
														totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

													nlapiLogExecution('DEBUG', '1818 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
													var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

													leaveBalMastrLeaveBal = Number(calcLeaveNext) + Number(minCarryForwd);
													oldLeaveBal = Number(leaveBalCarriedForwd);

												}	
												else
												{
													
													var totalLeavePrev = Number(0);

													if(toBeAddedAnnually == 'T') 
														totalLeavePrev = Number(calcLeavePrev);
													else
														totalLeavePrev = Number(calcLeavePrev) + Number(leaveBalMastrLeaveBal);

													nlapiLogExecution('DEBUG', '1838 calcLeavePrev = ',calcLeavePrev+'@@'+'calcLeaveNext = '+calcLeaveNext);
													var minCarryForwd = Number(totalLeavePrev) < Number(carryForwdMax) ? Number(totalLeavePrev) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG', 'minCarryForwd = ',minCarryForwd);

													leaveBalMastrLeaveBal = Number(calcLeaveNext);
													oldLeaveBal = Number(minCarryForwd);

												}
											}
											/** Sabbatical Leave in Next Year only **/
											else if(nlapiStringToDate(fromSTMonthNew)>=nlapiStringToDate(calendrSTMonthNew))
											{

												prevBalance = Number(leaveBalMastrLeaveBal) + Number(leaveBalCarriedForwd);
												/** From Date is new calender year start date.**/
												fromDate = calendStartMonth;

												if(isCarryForward=='T')
												{
													nlapiLogExecution('DEBUG', 'case3 1856 CFT leaveBalMastrLeaveBal = ',leaveBalMastrLeaveBal+'@@'+'carryForwdMax= '+carryForwdMax);

													var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													nlapiLogExecution('DEBUG','case3 1860 CFT calculatedLeave= ',calculatedLeave+'@@'+'minCarryForwd= '+minCarryForwd);

													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(accrualFacotr) + Number(minCarryForwd);
													else
														leaveBalMastrLeaveBal = Number(calculatedLeave) + Number(minCarryForwd);

													oldLeaveBal = Number(leaveBalCarriedForwd);
												}	
												else
												{
													//Commented by new update by rutika on 12-May
													//var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													var minCarryForwd = Number(leaveBalMastrLeaveBal) < Number(carryForwdMax) ? Number(leaveBalMastrLeaveBal) : Number(carryForwdMax);

													oldLeaveBal = Number(minCarryForwd);// + Number(carryForwdMax);
													if(toBeAddedAnnually == 'T')
														leaveBalMastrLeaveBal = Number(accrualFacotr);
													else
														leaveBalMastrLeaveBal = Number(calculatedLeave);

													nlapiLogExecution('DEBUG', 'case3 1875 CFF oldLeaveBal = ',oldLeaveBal+'@@'+'leaveBalMastrLeaveBal= '+leaveBalMastrLeaveBal);

												}
											}

											oldLeaveBal = parseFloat(oldLeaveBal).toFixed(2);
											leaveBalMastrLeaveBal = parseFloat(leaveBalMastrLeaveBal).toFixed(2);
											prevBalance = parseFloat(prevBalance).toFixed(2);

											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_leavebalance',iii,leaveBalMastrLeaveBal);
											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stlms_lbline_carriedforward',iii,oldLeaveBal);
											leaveBalMasterObj.setLineItemValue('recmachcustrecord_stlms_lbline_parent','custrecord_stmm_lbline_leavebalprevious',iii,prevBalance);
										}
									}
								}
							}
						}
					}
				}
			}

			if(normalCaseFound=='T') {
				nlapiLogExecution('DEBUG', 'Inside normal case found From Date', normalCaseFromDate+' '+normalCaseFound);
//				leaveBalMasterObj.setFieldValue('custrecord_stlms_lbm_fromdate',normalCaseFromDate);
				leaveBalMasterObj.setFieldValue('custrecord_stlms_lbm_todate',ToDate);
				leaveBalMasterObj.setFieldValue('custrecord_stlms_lbm_asondate',todayDate);

				nlapiSubmitField('employee',recId,'custentity_stlms_resumeleaveaccrual','F');
				var leaveBalMastrSubmitId = nlapiSubmitRecord(leaveBalMasterObj,true,true);
				nlapiLogExecution('DEBUG', '1904 leaveBalMastrSubmitId = ',leaveBalMastrSubmitId);
				if(leaveBalMastrSubmitId) {

					nlapiSetRedirectURL('RECORD', 'customrecord_stlms_leavebalancemaster', leaveBalMastrSubmitId, false);
				}
			}
			else if(normalCaseFound=='F'){

				leaveBalMasterObj.setFieldValue('custrecord_stlms_lbm_fromdate',fromDate);
				leaveBalMasterObj.setFieldValue('custrecord_stlms_lbm_todate',ToDate);
				leaveBalMasterObj.setFieldValue('custrecord_stlms_lbm_asondate',todayDate);

				nlapiSubmitField('employee',recId,'custentity_stlms_resumeleaveaccrual','F');
				var leaveBalMastrSubmitId = nlapiSubmitRecord(leaveBalMasterObj,true,true);
				nlapiLogExecution('DEBUG', '1904 leaveBalMastrSubmitId = ',leaveBalMastrSubmitId);
				if(leaveBalMastrSubmitId) {

					nlapiSetRedirectURL('RECORD', 'customrecord_stlms_leavebalancemaster', leaveBalMastrSubmitId, false);
				}
			}
		}
	}

	/*}
	catch(error)
	{
		if (error.getDetails != undefined)
		{
			nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
			throw error;
		}
		else
		{
			nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}*/
}

function getCalculatedLeaves(accrualFacotr,fromSTMonth,ToDate)
{
	var daysBetween = 0;
	var daysBetween1 = 0;
	var daysBetween2 = 0;
	var noOfMonths = 0;

	//nlapiLogExecution('DEBUG', '494 accrualFacotr = ',accrualFacotr+'@@'+'fromSTMonth = '+fromSTMonth+'@@'+'ToDate = '+ToDate);

	var month = 0;
//	var fromSTMonth = fromSTMonth;
//	fromSTMonth = nlapiDateToString(fromSTMonth);
	var fromDate1 = fromSTMonth;
	fromDate1 =	fromDate1.split('/');
	var stDay = fromDate1[0];
	var stMonth = fromDate1[1];
	var stYear = fromDate1[2];
	var stRetrnDay = getDay(stMonth,stYear);

//	var ToDate = ToDate;
//	ToDate = nlapiDateToString(ToDate);
	var ToDate1 = ToDate;
	ToDate1 = ToDate1.split('/');
	var toDay = ToDate1[0];
	var toMonth = ToDate1[1];
	var toYear = ToDate1[2];
	var toRetrnDay = getDay(toMonth,toYear);

	//nlapiLogExecution('DEBUG', '278 stDay = ',stDay+'@@'+'toDay = '+toDay);
	if(Number(stDay)==1 && (Number(toDay)==30 || Number(toDay)==31 || Number(toDay)==28))
	{
		if(toYear>stYear)
			noOfMonths = Number(12) - Number(stMonth) + Number(toMonth) + Number(1);
		else
			noOfMonths = Number(toMonth) - Number(stMonth) + Number(1);

		nlapiLogExecution('DEBUG', '286 noOfMonths = ',noOfMonths+'@@'+'daysBetween = '+daysBetween);
	}	
	else if(stDay>1 && (toDay==30 || toDay==31)) 
	{
		if(toYear>stYear)
			noOfMonths = Number(12) - Number(stMonth) + Number(toMonth) + Number(1);
		else
			noOfMonths = Number(toMonth) - Number(stMonth);

		daysBetween = Number(stRetrnDay) - Number(stDay) + Number(1);

		nlapiLogExecution('DEBUG', '296 noOfMonths = ',noOfMonths+'@@'+'daysBetween = '+daysBetween);
	}	
	else if(stDay>1 && toDay<30) 
	{
		if(toYear>stYear)
			noOfMonths = Number(12) - Number(stMonth) + Number(toMonth) + Number(1);
		else
			noOfMonths = Number(toMonth) - Number(stMonth);
		daysBetween1 = Number(stRetrnDay) - Number(stDay) + Number(1);
		daysBetween2 = Number(toRetrnDay) - Number(toDay) + Number(1);

		nlapiLogExecution('DEBUG', '308 noOfMonths = ',noOfMonths+'@@'+'daysBetween = '+daysBetween);
	}

	var leaveBalance1 = noOfMonths*accrualFacotr;
	var leaveBalance2=0,leaveBalance3=0,leaveBalance4=0;
	if(daysBetween>0)
	{
		leaveBalance2 = (Number(daysBetween)/Number(stRetrnDay))*accrualFacotr;
	}

	if(daysBetween1>0 && daysBetween2>0)
	{
		leaveBalance3 = (Number(daysBetween1)/Number(stRetrnDay))*Number(accrualFacotr);
		leaveBalance4 = (Number(daysBetween2)/Number(toRetrnDay))*Number(accrualFacotr);
	}

	var calculatedLeave = Number(leaveBalance1) + Number(leaveBalance2)+ Number(leaveBalance3)+ Number(leaveBalance4);

	return  calculatedLeave;
}

function monthDiff(d1, d2) 
{
	nlapiLogExecution('DEBUG', '173 d1 = ',d1+'##'+'d2 = '+d2);
	var months = '';

	/*
	if(d1)
	{
		d1 = d1.split('/');
		d1 = new Date(d1[2],d1[1]-1,d1[0]);
	}
	if(d2)
	{
		d2 = d2.split('/');
		d2 = new Date(d2[2],d2[1]-1,d2[0]);
	}	

	var d1FullYear = d1.getFullYear();
	var d2FullYear = d2.getFullYear();
	var d1Months = d1.getMonth();
	var d2Months = d2.getMonth();

	months =  (d2FullYear-d1FullYear)*12;
	months += Math.abs(d1Months-d2Months);

	return months <= 0 ? 0 : months;
	 */
	//var d1 = new Date('3/1/2016');
	//var d2 = new Date('4/30/2016');


	if(d1)
	{
		d1 = d1.split('/');
		d1 = new Date(d1[1]+'/'+d1[0]+'/'+d1[2]);
	}
	if(d2)
	{
		d2 = d2.split('/');
		d2 = new Date(d2[1]+'/'+d2[0]+'/'+d2[2]);
	}	


	var one_day = 1000*60*60*24;
	var date1_ms = d1.getTime();
	var date2_ms = d2.getTime();

	var difference_ms = date2_ms - date1_ms;

	var a = Math.round(difference_ms/one_day);
	return a = Number(a);


}

Date.prototype.DaysBetween = function(){
	var intMilDay = 24 * 60 * 60 * 1000;
	var intMilDif = arguments[0] - this;
	var intDays = Math.floor(intMilDif/intMilDay);
	return intDays;
};



function getLeaveTypeSetup(empLeaveType,employeeType,jobTitle,servicePeriod)
{
	var sFilt = [];
	var sColm = [];
	var srchLeaveTypeSetup = '';

	if(empLeaveType && employeeType)
	{

		if(jobTitle){

			sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
			sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

			srchLeaveTypeSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilt, sColm);

			if(srchLeaveTypeSetup){
				return srchLeaveTypeSetup;

			}

			else {

				sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', jobTitle));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
				sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

				srchLeaveTypeSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilt, sColm);

				if(srchLeaveTypeSetup){
					return srchLeaveTypeSetup;

				}

				else {

					sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
					sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
					sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
					sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
					sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

					sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
					sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
					sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
					sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
					sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

					srchLeaveTypeSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilt, sColm);

					if(srchLeaveTypeSetup){
						return srchLeaveTypeSetup;

					}

					else {

						sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
						sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
						sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
						sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
						sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

						sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
						sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
						sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
						sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
						sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

						srchLeaveTypeSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilt, sColm);

						if(srchLeaveTypeSetup){
							return srchLeaveTypeSetup;

						}
					}

				}

			}

		}

		else {

			sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'is', '@NONE@'));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'greaterthanorequalto', Number(0)));
			sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'greaterthan', Number(0)));

			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
			sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
			sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

			srchLeaveTypeSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilt, sColm);

			if(srchLeaveTypeSetup){
				return srchLeaveTypeSetup;

			}
			else {

				sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype','custrecord_stlms_ltsetup_parent','is', employeeType));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_leavetype', null, 'anyof', empLeaveType));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_jobtitle', null, 'anyof', '@NONE@'));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmin', null, 'isempty', null));
				sFilt.push(new nlobjSearchFilter('custrecord_stlms_ltsetup_expmax', null, 'isempty', null));

				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_accrualfactor'));
				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavetype'));
				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_carryforward'));
				sColm.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_leavescarriedfo'));
				sColma.push(new nlobjSearchColumn('custrecord_stlms_ltsetup_appliedannually'));

				srchLeaveTypeSetup = nlapiSearchRecord('customrecord_stlms_leavetypesetup', null, sFilt, sColm);

				if(srchLeaveTypeSetup){
					return srchLeaveTypeSetup;

				}

			}

		}

	}

}

function getRunPeriodicLeaveAccrDate(employeeType)
{
	var today = nlapiDateToString(new Date());
	today = Date.parse(today);
	var returnEndDate = '';

	var sFilt = [];
	var sColm = [];

	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_rpla_status', null, 'is', statusComplete));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_rpla_employeetype', null, 'is', employeeType));

	sColm.push(new nlobjSearchColumn('custrecord_stlms_rpla_fromdate',null,'max'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_rpla_todate',null,'max'));

	var runRediodicResults = nlapiSearchRecord('customrecord_stlms_runaccrual', null, sFilt, sColm); 
	if(runRediodicResults)
	{
		//nlapiLogExecution('DEBUG', '462 runRediodicResults.length = ',runRediodicResults.length);
		for(var ii=0;ii<runRediodicResults.length;ii++)
		{
			var stDate = runRediodicResults[ii].getValue('custrecord_stlms_rpla_fromdate',null,'max');
			//stDate = Date.parse(stDate);
			var enDate = runRediodicResults[ii].getValue('custrecord_stlms_rpla_todate',null,'max');
			returnEndDate = enDate
			//nlapiLogExecution('DEBUG', '469 returnEndDate = ',returnEndDate);

			//enDate = Date.parse(enDate);

			//if(stDate<today && today<enDate)
			//return returnEndDate;
		}
	}
	else
		nlapiLogExecution('DEBUG', '478 No Max = ','No Max');

	return returnEndDate;
}

function getDay(leaveCalndrStMonth,toYears)
{

	var toYear = parseInt(toYears);
	var results = 0;
	switch (parseInt(leaveCalndrStMonth)) {
	case 1:
		results = 31;
		break;
	case 2:
		if((toYears%4==0) || (toYears%100==0) || (toYears%400==0))
		{
			results = 29;
			break;
		}
		else
		{
			results = 28;
			break;
		}	
	case 3:
		results = 31;
		break;
	case 4:
		results = 30;
		break;
	case 5:
		results = 31;
		break;
	case 6:
		results = 30;
		break;
	case 7:
		results = 31;
		break;
	case 8:
		results = 31;
		break;
	case 9:
		results = 30;
		break;
	case 10:
		results = 31;
		break;
	case 11:
		results = 30;
		break;	
	case 12:
		results = 31;
		break;	
	}

	return results;
}

function getLeaveCalenderSearch(employeeType)
{
	var calendrStMonths = '';
	var currDate = nlapiDateToString(new Date());
	var currYear = currDate.split('/')[2];

	var sFilt = [];
	var sColm = [];

	sFilt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	sFilt.push(new nlobjSearchFilter('custrecord_stlms_su_employeetype', null, 'is', employeeType));

	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_leavecalendarstart'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_leavecalendarend'));

	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_calendarstartdate'));
	sColm.push(new nlobjSearchColumn('custrecord_stlms_su_calendarenddate'));

	var setupCalenderMonths = nlapiSearchRecord('customrecord_stlms_setups', null, sFilt, sColm); 
	if(setupCalenderMonths)
	{
		var startMonth = setupCalenderMonths[0].getValue('custrecord_stlms_su_leavecalendarstart');
		calendrStMonths = startMonth+'/'+'1'+'/'+currYear;
		//calendrStMonths = '1'+'/'+startMonth+'/'+currYear;
	}

	return setupCalenderMonths;
}