/* ************************************************************************************  
 ** Copyright (coffee) 1998-2012 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of          
 ** Softype, Inc.("Confidential Information"). You shall not disclose              
 ** such Confidential Information and shall use it only in accordance         
 ** with the terms of the license agreement you entered into with Softype.
 ** @version: Revised version               
 **                       
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Mar 2016     Nihal Mulani
 *
	Description : This Script Add leaves which is deducted after approve leave application.
 ************************************************************************************ */
function Cancel_Leave_Application() 
{
	var Rec_Id= nlapiGetRecordId();
	var Rec_Type= nlapiGetRecordType();

	/*	var old_Data=nlapiGetOldRecord(Rec_Type,Rec_Id);
		var old_leavebalance=old_Data.getFieldValue('custrecord_stlms_la_leavebalance');
		nlapiLogExecution('Debug', "old_leavebalance",old_leavebalance);
	 */
	var leavetype = nlapiGetFieldValue('custrecord_stlms_la_deductfrom');
	var employeename = nlapiGetFieldValue('custrecord_stlms_la_employeename');

	var deductedfromleavebal = nlapiGetFieldValue('custrecord_stlms_la_deductedfromleavebal');
	var deductedfromcarryfwd = nlapiGetFieldValue('custrecord_stlms_la_deductedfromcarryfwd');
	var deductedfromleavebal_loa = nlapiGetFieldValue('custrecord_stlms_la_deducted_loaleavebal');
	var deductedfromcarryfwd_loa = nlapiGetFieldValue('custrecord_stlms_la_deducted_loacarryfwd');
	
	var leaveperiod = nlapiGetFieldValue('custrecord_stlms_la_leaveperiod');

/****************************Search On Employee Record **********************************************/	
	
	var CLA_Filter = new Array();
	var CLA_col = new Array();
	var leavetypes='';
	var annualLeave = 8;        // Annual Leave ID
	var leaveOfAbsenceId = 1;     // Leave Of Absence ID
	CLA_Filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F' ));
	CLA_Filter.push(new nlobjSearchFilter('internalid', null, 'is', employeename ));
	CLA_col.push(new nlobjSearchColumn('custentity_stlms_leavetypes'));
	var Search = nlapiSearchRecord('employee',null,CLA_Filter,CLA_col);
	if (Search)
	{
			leavetypes =Search[0].getValue('custentity_stlms_leavetypes');
			leavetypes=leavetypes.split(',');
	}
	
/****************************Search On Leave Balance Master Record *************************************/
	
	if(leavetypes.indexOf(leavetype)!=-1) // if leavetype is present on Entity then Process.
	{
		CLA_Search = searchEmployeeLeaveBalance(leavetype,employeename);
		nlapiLogExecution('DEBUG','Inside Index Matched Record');
		nlapiLogExecution('DEBUG','Search Object',JSON.stringify(CLA_Search));
//		var CLA_Flt = new Array();
//		var leavebalancemaster_id='';
//		CLA_Flt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F' ));
//		CLA_Flt.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leavetype ));
//		CLA_Flt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename','custrecord_stlms_lbline_parent','is', employeename ));
//		var column = new Array();
//		column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
//		column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
//		column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leaveutilized'));
//		var CLA_Search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin',null,CLA_Flt,column);
		if(CLA_Search)
		{
			
//			var leavebalance= CLA_Search[0].getValue('custrecord_stlms_lbline_leavebalance');
//			var carriedforward= CLA_Search[0].getValue('custrecord_stlms_lbline_carriedforward');
//			var leaveutilized= CLA_Search[0].getValue('custrecord_stlms_lbline_leaveutilized');
//		//	leaveperiod = - Number(leaveperiod);
//			var utilized  = (Number(leaveutilized) - Number(leaveperiod));
//			nlapiLogExecution('DEBUG', 'leaveutilized', leaveutilized);
//			nlapiLogExecution('DEBUG', 'leaveperiod', leaveperiod);
//			nlapiLogExecution('DEBUG', 'utilized', utilized);
//			new_leavebal=(Number(leavebalance) + Number(deductedfromleavebal));
//			new_carryfwd=(Number(carriedforward) + Number(deductedfromcarryfwd));
//			nlapiSubmitField('customrecord_stlms_leavebalancemasterlin', CLA_Search[0].getId(), ['custrecord_stlms_lbline_leavebalance','custrecord_stlms_lbline_carriedforward','custrecord_stlms_lbline_leaveutilized'], [new_leavebal,new_carryfwd,utilized]);
			
			
			// if the leave type is annual leave it will recover all leave balance from leave types---->Annual leave and Leave and Absence
			if(leavetype == annualLeave) {
				
				// In leaveperiod you will get total leaves deducted from annual leave
				leaveperiod = Number(deductedfromleavebal) + Number(deductedfromcarryfwd);
				nlapiLogExecution('DEBUG','Leave Period of Annual Leave',leaveperiod);
				recoverLeaveBalance(leavetype,CLA_Search,leaveperiod,deductedfromleavebal,deductedfromcarryfwd);
				
				// CLA_Search_loa will search for leave of absence record of a particular employee.
				if(inArray(leaveOfAbsenceId,leavetypes)) {
					//--- if leave of absence id is present in leaveTypes then it will deduct leave balance from this type-----------------------//
					var CLA_Search_loa = searchEmployeeLeaveBalance(leaveOfAbsenceId,employeename);
					nlapiLogExecution('DEBUG','Search Object LOA',JSON.stringify(CLA_Search_loa));
					if(CLA_Search_loa) {
						// In leaveperiod you will get total leaves deducted from leave of absence
						leaveperiod = Number(deductedfromleavebal_loa) + Number(deductedfromcarryfwd_loa);
						nlapiLogExecution('DEBUG','Leave Period of Leave of Absence',leaveperiod);
						recoverLeaveBalance(leaveOfAbsenceId,CLA_Search_loa,leaveperiod,deductedfromleavebal_loa,deductedfromcarryfwd_loa);
					}
				}
				else{
						leaveperiod = Number(deductedfromleavebal_loa) + Number(deductedfromcarryfwd_loa);
						leaveTypeNotInEmployeeRecord(leaveOfAbsenceId,employeename,leaveperiod);
				}
			}
			else {
				
				recoverLeaveBalance(leavetype,CLA_Search,leaveperiod,deductedfromleavebal,deductedfromcarryfwd);
			}
		}
		else // if leavetype is not present on Entity then Process.
		{
			leaveTypeNotInEmployeeRecord(leavetype,employeename,leaveperiod);
		}
	}
}

//--------------------Function to search Employee Leave Balance ----------------------------------------------------------------//
function searchEmployeeLeaveBalance(leavetype,employeename) {
	
	var CLA_Flt = new Array();
	var leavebalancemaster_id='';
	CLA_Flt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F' ));
	CLA_Flt.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leavetype ));
	CLA_Flt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename','custrecord_stlms_lbline_parent','is', employeename ));
	var column = new Array();
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leavebalance'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_carriedforward'));
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leaveutilized'));
	var CLA_Search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin',null,CLA_Flt,column);
	return CLA_Search;
	
	
} // end of searchEmployeeLeaveBalance

//-----------------------Function to update employee record leave balance after cancellation------------------------------------// 

 function recoverLeaveBalance(leaveType,CLA_Search,leaveperiod,deductedfromleavebal,deductedfromcarryfwd) {
	 
		var leavebalance= CLA_Search[0].getValue('custrecord_stlms_lbline_leavebalance');
		var carriedforward= CLA_Search[0].getValue('custrecord_stlms_lbline_carriedforward');
		var leaveutilized= CLA_Search[0].getValue('custrecord_stlms_lbline_leaveutilized');
	//	leaveperiod = - Number(leaveperiod);
		var utilized  = (Number(leaveutilized) - Number(leaveperiod));
		nlapiLogExecution('DEBUG', 'leaveutilized', leaveutilized);
		nlapiLogExecution('DEBUG', 'leaveperiod', leaveperiod);
		nlapiLogExecution('DEBUG', 'utilized', utilized);
		new_leavebal=(Number(leavebalance) + Number(deductedfromleavebal));
		new_carryfwd=(Number(carriedforward) + Number(deductedfromcarryfwd));
		nlapiSubmitField('customrecord_stlms_leavebalancemasterlin', CLA_Search[0].getId(), ['custrecord_stlms_lbline_leavebalance','custrecord_stlms_lbline_carriedforward','custrecord_stlms_lbline_leaveutilized'], [new_leavebal,new_carryfwd,utilized]);
		
}
 
 
//---------If leave Type is not in Employee Record then it will set Leave Balance to Zero 
function leaveTypeNotInEmployeeRecord(leavetype,employeename,leaveperiod) {
	
	var CLA_Flt = new Array();
	var leavebalancemaster_id='';
	CLA_Flt.push(new nlobjSearchFilter('isinactive', null, 'is', 'F' ));
	CLA_Flt.push(new nlobjSearchFilter('custrecord_stlms_lbline_leavetype', null, 'is', leavetype ));
	CLA_Flt.push(new nlobjSearchFilter('custrecord_stlms_lbm_employeename','custrecord_stlms_lbline_parent','is', employeename ));
	var column = new Array();
	column.push(new nlobjSearchColumn('custrecord_stlms_lbline_leaveutilized'));
	var CLA_Search = nlapiSearchRecord('customrecord_stlms_leavebalancemasterlin',null,CLA_Flt,column);
	if(CLA_Search)
	{
		var leaveutilized= CLA_Search[0].getValue('custrecord_stlms_lbline_leaveutilized');
		var utilized  = (Number(leaveutilized)-Number(leaveperiod));
		nlapiSubmitField('customrecord_stlms_leavebalancemasterlin', CLA_Search[0].getId(), ['custrecord_stlms_lbline_leavebalance','custrecord_stlms_lbline_carriedforward','custrecord_stlms_lbline_leaveutilized'], [0,0,utilized]);

	}
}
 
 
//-----------------function to find value in array ----------------------------------------------------------------------------------//


 function arrayCompare(a1, a2) {
     if (a1.length != a2.length) return false;
     var length = a2.length;
     for (var i = 0; i < length; i++) {
         if (a1[i] !== a2[i]) return false;
     }
     return true;
 }

 function inArray(needle, haystack) {
     var length = haystack.length;
     for(var i = 0; i < length; i++) {
         if(typeof haystack[i] == 'object') {
             if(arrayCompare(haystack[i], needle)) return true;
         } else {
             if(haystack[i] == needle) return true;
         }
     }
     return false;
 }

 //-------------------------------------------------------------------------------------------------------------------------------------------//